<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->enum('business_type', ['Simple', 'Top'])->default('Simple');
            $table->string('name', 250);
            $table->string('category', 250);
            $table->string('phone', 100);
            $table->string('email', 100);
            $table->string('bio')->nullable(true);
            $table->string('web1')->nullable(true);
            $table->string('web2')->nullable(true);
            $table->string('video')->nullable(true);
            $table->string('sdate')->nullable(true);
            $table->string('edate')->nullable(true);
            $table->string('showdate')->nullable(true);
            $table->string('opening_hours')->nullable(true);
            $table->string('closing_hours')->nullable(true);
            $table->string('street', 250)->nullable(true);
            $table->string('house_no', 250)->nullable(true);
            $table->string('post_code', 20)->nullable(true);
            $table->string('city', 250)->nullable(true);
            $table->string('country', 250)->nullable(true);
            $table->string('path_cover_image', 450)->nullable(true);
            $table->string('path_logo_image', 450)->nullable(true);
            $table->enum('is_deleted', ['true', 'false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('businesses');
    }
}
