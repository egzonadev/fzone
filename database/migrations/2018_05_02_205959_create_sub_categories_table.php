<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('name', 100);
            $table->timestamps();
        });

        DB::table('sub_categories')->insert([
            ['category_id' => 1 , 'name' => 'Hiking'],
            ['category_id' => 1 , 'name' => 'Cinema'],
            ['category_id' => 1 , 'name' => 'Walking'],
            ['category_id' => 1 , 'name' => 'Travel']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_categories');
    }
}
