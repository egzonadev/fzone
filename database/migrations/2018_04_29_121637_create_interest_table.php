<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest', function (Blueprint $table) {
            $table->increments('id');
            $table->string('interest')->nullable(false);
            $table->string('image');
            $table->timestamps();
        });
        DB::table('interest')->insert([
            ['interest' => 'Essen & Trinken', 'image' => 'essen.jpg'],
            ['interest' => 'Familie & Kinder', 'image' => 'familie.jpg'],
            ['interest' => 'Firmenanlasse & Ausfluge', 'image' => 'ausfluge.jpg'],
            ['interest' => 'Fun and Adventure', 'image' => 'fun.jpg'],
            ['interest' => 'Kultur', 'image' => 'kultur.jpg'],
            ['interest' => 'Romantik', 'image' => 'romantik.jpg'],
            ['interest' => 'Sport', 'image' => 'sport.JPG'],
            ['interest' => 'Wellness & Beauty', 'image' => 'beauty.jpg'],

        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest');
    }
}
