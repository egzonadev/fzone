<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable(false);
            $table->string('image');
            $table->timestamps();
        });
        DB::table('categories')->insert([
            ['name' => 'Essen & Trinken', 'image' => 'essen.jpg'],
            ['name' => 'Familie & Kinder', 'image' => 'familie.jpg'],
            ['name' => 'Firmenanlasse & Ausfluge', 'image' => 'ausfluge.jpg'],
            ['name' => 'Fun and Adventure', 'image' => 'fun.jpg'],
            ['name' => 'Kultur', 'image' => 'kultur.jpg'],
            ['name' => 'Romantik', 'image' => 'romantik.jpg'],
            ['name' => 'Sport', 'image' => 'sport.JPG'],
            ['name' => 'Wellness & Beauty', 'image' => 'beauty.jpg'],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
