<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('business_id')->unsigned();
            //$table->foreign('business_id')->references('id')->on('businesses'); 
            $table->string('title', 250);
            $table->string('description');
            $table->dateTime('start_date_time');
            $table->dateTime('end_date_time');
            $table->string('location', 450);
            $table->string('city', 250);
            $table->boolean('use_account_email');
            $table->boolean('use_account_phone');
            $table->string('extra_email')->nullable();
            $table->string('extra_phone')->nullable();
            $table->timestamps();
        }); 
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('activities');
    }
}
