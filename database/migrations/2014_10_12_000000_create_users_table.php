<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
             $table->increments('id');
             $table->enum('usertype',['Individual','Business','SuperAdmin', 'Premium'])->default('Individual');
             $table->string('name')->nullable(false);
             $table->string('description')->nullable(true);
             $table->string('username')->nullable(false);
             $table->string('email')->unique();
             $table->string('password')->nullable(false);
             $table->string('image')->nullable(false)->default('user.jpg');
             $table->string('gender')->nullable(false);
             $table->date('age')->nullable(false);
             $table->string('phone')->nullable(false);
             $table->string('street')->nullable(false);
             $table->string('pcode')->nullable(false);

            $table->string('jobtitle')->nullable(true);
            $table->string('contact_name')->nullable(true);
            $table->string('contact_phone')->nullable(true);
            $table->string('contact_email')->nullable(true);
             $table->enum('is_upgraded',['true','false'])->default('false');
             $table->enum('has_requested_upgrade',['true','false'])->default('false');
             $table->rememberToken();
             $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('users');
    }
}
