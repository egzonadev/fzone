<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityInvitedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_invited_users', function (Blueprint $table) {
            $table->increments('id'); 

            $table->integer('activity_id')->unsigned(); // activity
            $table->integer('invited_user_id')->unsigned(); // user that is being invited

            $table->foreign('activity_id')->references('id')->on('activities'); 
            $table->foreign('invited_user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_invited_users');
    }
}
