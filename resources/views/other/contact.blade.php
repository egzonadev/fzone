@extends('layout.master')
@section('main_content')
    <div class="row">
        <div class="col-lg-12" style="background-color: #ea394c; height: 20%">
            <h1 class="text-center" style="color: white; font-weight: bold">CONTACT</h1>
        </div>
        <div class="container">

            <div class="row" style="margin-top: 15%; padding-bottom: 5%">
                <div class="col-lg-4 col-lg-offset-1">

                    <div id="map" style="width:400px;height:400px;background:yellow"></div>
                </div>
                <div class="col-lg-4 col-lg-offset-1">
                    <h3><strong>Get In Touch</strong></h3>
                    <p><small>Contact us now and let your journey begin</small></p>
                    @include('includes.messages')
                    <form method="post" action="{{action('BusinessesController@contact_company', 1)}}" class="form-horizontal">
                        {{csrf_field()}}
                        <div style="margin-top: 5%">
                            <input type="text" name="name" value="{{Auth::check() ? Auth::user()->name : ''}}"
                                   maxlength="255" placeholder="Your name" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%">
                            <input type="email" name="email" value="{{Auth::check() ? Auth::user()->email : ''}}"
                                   maxlength="255" placeholder="Email" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%">
                            <input type="text" name="subject" value="{{Auth::check() ? Auth::user()->subject : ''}}"
                                   maxlength="255" placeholder="Subject" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%;">
                            <textarea  rows="4" name="message"  maxlength="450" placeholder="Message" class="form-control" required autofocus></textarea>
                        </div>
                        <div style="margin-top: 5%">
                            <button class="btn btn-danger" type="submit">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script>
        function myMap() {
            var mapOptions = {
                center: new google.maps.LatLng(51.5, -0.12),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.street
            }
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiEWBVUo0RaQ4qZJtn-isVV-DdbWD51y8&callback=myMap"></script>
@stop