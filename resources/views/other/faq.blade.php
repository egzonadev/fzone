@extends('layout.master')
@section('main_content')
    <div class="row">
        <div class="col-lg-12" style="background-color: #ea394c; height: 20%">
            <h1 class="text-center" style="color: white; font-weight: bold">Faqs</h1>
        </div>
        <div class="container">
            <div class="row" style="margin-top: 15%; padding-bottom: 5%">
                <div class="col-lg-10 col-lg-offset-1">
                    <div >
                        <ul class="nav navbar-top-links navbar-center" style="padding: 4px; font-weight: bold; background-color: #dbdbdb; ">
                            <li {{{ (Request::is('faqs') ? 'class=active' : '') }}}>
                                <a class="dropdown-toggle count-info" href="/faqs">
                                     <span class="user-mini-navbar">Shopping</span>
                                </a>
                            </li>
                            <li class="col-lg-offset-4" {{{ (Request::is('user/activities') ? 'class=active' : '') }}}>
                                <a class="dropdown-toggle count-info" href="#">
                                    <span class="user-mini-navbar">Payment</span>
                                </a>
                            </li>
                            <li class="pull-right" {{{ (Request::is('user/friends') ? 'class=active' : '') }}}>
                                <a class="dropdown-toggle count-info" href="#">
                                     <span class="user-mini-navbar">Orders & Returns</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="col-lg-6">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="dd" id="nestable2">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="1">
                                            <div class="dd-handle">
                                                <span class="label label-info"></span> What if my purchase does not arrive within the hour
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="2">
                                                    <div class="dd-handle">
                                                        <span class="label label-info"></span> We will make sure it will arrive on time!
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>

                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label label-warning"></span> Do you ship internationally?
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="label label-warning"></span>
                                                        Shipping internationally is easier than you might think. Our online process
                                                        leads you step by step to create your shipment, and generates your customs documents as you go.
                                                        And because you can reuse saved addresses and product details from previous shipments, it gets
                                                        faster and easier the more you ship.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label label-warning"></span> What payment methods are accepted?
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="label label-warning"></span>
                                                        Major credit cards and pre-paid credit cards (Visa, MasterCard, Amex, Discover, JCB)
                                                        Many debit cards that can be processed as credit.
                                                        PayPal for select countries.
                                                        Alipay for China only.
                                                        Postepay for Italy only.
                                                        Sofort Überweisung for Germany only.
                                                        iDEAL for the Netherlands only.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="dd" id="nestable3">
                                    <ol class="dd-list">
                                        <li class="dd-item" data-id="1">
                                            <div class="dd-handle">
                                                <span class="label label-info"></span> How long it will take to get my package?
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="2">
                                                    <div class="dd-handle">
                                                        <span class="label label-info"></span> Usually five working days!
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>

                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label label-warning"></span> What is included in FZZ package
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="label label-warning"></span>
                                                        Free and Premium accounts
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                        <li class="dd-item" data-id="5">
                                            <div class="dd-handle">
                                                <span class="label label-warning"></span> Will you be compatible with facebook.
                                            </div>
                                            <ol class="dd-list">
                                                <li class="dd-item" data-id="6">
                                                    <div class="dd-handle">
                                                        <span class="label label-warning"></span>
                                                        We might in the future.
                                                    </div>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Nestable List -->
    <script src="/assets/js/jquery.nestable.js"></script>
    <script>
        $(document).ready(function(){

            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target),
                    output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };
            // activate Nestable for list 1
            $('#nestable').nestable({
                group: 1
            }).on('change', updateOutput);

            // activate Nestable for list 2
            $('#nestable2').nestable({
                group: 1
            }).on('change', updateOutput);

            // activate Nestable for list 3
            $('#nestable3').nestable({
                group: 1
            }).on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#nestable').data('output', $('#nestable-output')));
            updateOutput($('#nestable2').data('output', $('#nestable2-output')));
            updateOutput($('#nestable3').data('output', $('#nestable2-output')));

            $('#nestable-menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });
        });
    </script>

@stop