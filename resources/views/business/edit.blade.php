
@extends('layout.master')

@section('main_content')
    <?php
    use App\BusinessFeature;
    $business_feature = new BusinessFeature();
    $business_features =  $business_feature->get_business_features($business->id);
    $features = "";
    foreach($business_features as $feature_image){
        $features .= "'/storage/business/features/". $feature_image->path_feature_image ."' , ";
    }
    ?> 
    @include('includes.messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-title">
                <div align="center">
                    <h2> <b> Edit Business</b></h2>
                </div>
            </div>
            <div class="col-lg-6 col-lg-offset-3">
                <div  class="ibox float-e-margins">
                    <div class="ibox-content">
                        <form method="post" action="{{action('BusinessesController@update', $business->id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Business Name : </label>
                                <input type="text" name="name" value="{{$business->name}}" class="form-control" placeholder="Enter Business Name">
                            </div>
                            <div class="form-group">
                                <label for="type">Business Category: </label>
                                    <select name="type" class="form-control">
                                        <option style="display: none" value="{{$business->category}}">{{$business->category}}</option>
                                        <option value="Essen & Trinken">Essen & Trinken</option>
                                        <option value="Familie & Kinder">Familie & Kinder</option>
                                        <option value="Firmenanlasse & Ausfluge">Firmenanlasse & Ausfluge</option>
                                        <option value="Fun and Adventure">Fun and Adventure</option>
                                        <option value="Kultur">Kultur</option>
                                        <option value="Romantik">Romantik</option>
                                        <option value="Sport">Sport</option>
                                        <option value="Wellness & Beauty">Wellness & Beauty</option>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone number: </label>
                                <input type="text" name="phone" value="{{$business->phone}}" class="form-control" placeholder="Enter a valid phone number">
                            </div>
                            <div class="form-group">
                                <label for="email">Email adress : </label>
                                <input type="text" name="email" value="{{$business->email}}" class="form-control" placeholder="Enter a valid contact email adress">
                            </div>
                            <div class="form-group">
                                <label for="website">Website : </label>
                                <input type="text" name="website" value="{{$business->website}}" class="form-control" placeholder="Enter a url website">
                            </div>
                            <div class="form-group">
                                <label for="opening_hours">Opening Hours : </label>
                                <input type="text" name="opening_hours" value="{{$business->opening_hours}}" class="form-control" placeholder="Enter Opening Hours e.g 11:00 am - 17:00 pm">
                            </div>
                            <div class="form-group">
                                    <label for="opening_hours">Closing Hours : </label>
                                    <input type="text" name="opening_hours" value="{{$business->closing_hours}}" class="form-control" placeholder="Enter Closing Hours e.g 11:00 am - 17:00 pm">
                                </div>
                            <div class="form-group">
                                <label for="street">Street : </label>
                                <input type="text" name="street" value="{{$business->street}}" class="form-control" placeholder="Enter Street">
                            </div>
                            <div class="form-group">
                                <label for="house_no">House Number : </label>
                                <input type="text" name="house_no" value="{{$business->house_no}}" class="form-control" placeholder="Enter House No">
                            </div>
                            <div class="form-group">
                                <label for="post_code">Post Code : </label>
                                <input type="text" name="post_code" value="{{$business->post_code}}" class="form-control" placeholder="Enter Post Code">
                            </div>
                            <div class="form-group">
                                <label for="city">City : </label>
                                <input type="text" name="city" value="{{$business->city}}" class="form-control" placeholder="Enter City">
                            </div>
                            <div class="form-group">
                                <label for="country">Country : </label>
                                <input type="text" name="country" value="{{$business->country}}" class="form-control" placeholder="Enter Country">
                            </div>
                            <div class="form-group">
                                    <label for="input-logo_image">Logo Image : </label>
                                    <input type="file" name="input-logo_image" id="input-logo_image" class="file">
                                </div>
                            <div class="form-group">
                                <label for="input-cover_image">Cover Image : </label>
                                <input type="file" name="input-cover_image" id="input-cover_image" class="file">
                            </div>
                            <div class="form-group">
                                <label for="input-logo_image">Feature Image : </label>
                                <input type="file" name="input-feature_images[]" id="input-feature_images" class="file" multiple>
                            </div>
                            <input name="_method" type="hidden" value="PUT">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width:100%;">Update Business</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>

    <script>
        var path_cover_image = '/storage/business/cover/{{$business->path_cover_image}}';
        var path_logo_image =  '/storage/business/logo/{{$business->path_logo_image}}';
        $("#input-cover_image").fileinput({
            'showUpload': false,
            allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
            initialPreviewAsData: true,
            initialPreview: [path_cover_image],
            initialPreviewConfig: [{caption: "{{$business->path_cover_image}}", filename: "{{$business->path_cover_image}}", downloadUrl: path_cover_image, size: 930321, width: "120px", key: 1}],
            overwriteInitial: true,
        });
        $("#input-logo_image").fileinput({
            'showUpload': false,
            allowedFileExtensions: ["jpeg", "jpg", "png", "gif"],
            initialPreviewAsData: true,
            initialPreview: [path_logo_image],
            initialPreviewConfig: [{caption: "{{$business->path_logo_image}}", filename: "{{$business->path_logo_image}}", downloadUrl: path_logo_image, size: 930321, width: "120px", key: 1}],
            overwriteInitial: true,
        });
        $("#input-feature_images").fileinput({
            showUpload: false,
            maxFileCount: 3,
            initialPreviewAsData: true,
            initialPreview: [ <?php echo $features?>]
        });
    </script>
@endsection