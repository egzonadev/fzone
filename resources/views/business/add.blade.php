@extends('layout.master')
@section('main_content')
    @include('includes.messages')
    <div class="row ">
        <div class="col-lg-12">
            <div align="center">
                <h2> <b> Add Business</b></h2>
            </div>
            <div class="col-lg-10 col-lg-offset-1">
                <div  class="ibox float-e-margins">
                    <div class="ibox-content">
                        <form method="post" action="{{action('BusinessesController@storePremium', $btype)}}" class="form-horizontal" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group"><label class="col-lg-2 control-label" for="name">Business Name</label>
                                <div class="col-lg-10"><input type="text" name="name" class="form-control" placeholder="Enter Business Name..."></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="type">Business Category</label>
                                <div class="col-sm-10">
                                    <select name="type" class="form-control">
                                        <option value="Essen & Trinken">Essen & Trinken</option>
                                        <option value="Familie & Kinder">Familie & Kinder</option>
                                        <option value="Firmenanlasse & Ausfluge">Firmenanlasse & Ausfluge</option>
                                        <option value="Fun and Adventure">Fun and Adventure</option>
                                        <option value="Kultur">Kultur</option>
                                        <option value="Romantik">Romantik</option>
                                        <option value="Sport">Sport</option>
                                        <option value="Wellness & Beauty">Wellness & Beauty</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="phone">Phone number</label>
                                <div class="col-sm-10"><input type="text" name="phone" class="form-control" placeholder="Enter a valid phone number">
                                </div>
                            </div>
                            {{--added columns--}}
                            @if($btype == 'top')
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="phone">Start Date</label>
                                <div class="col-sm-10">
                                    <input type="date" name="sdate" class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="phone">End Date</label>
                                <div class="col-sm-10">
                                    <input type="date" name="edate" class="form-control">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="phone">Start Show</label>
                                <div class="col-sm-10">
                                    <input type="date" name="showdate" class="form-control">
                                </div>
                            </div>
                            @endif
                            {{----}}
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="phone">Bio</label>
                                <div class="col-sm-10">
                                    <textarea name="bio" maxlength="1400" class="form-control">Enter description...</textarea>
                                    <small>Maximum: 1400</small>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-lg-2 control-label" for="email">Email address</label>

                                <div class="col-lg-10"><input type="text" name="email" class="form-control" placeholder="Enter a valid contact email adress"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="website">Website 1</label>
                                <div class="col-sm-10">
                                    <input type="text" name="web1" class="form-control" placeholder="Enter a url website" ></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="website">Website 2</label>
                                <div class="col-sm-10">
                                    <input type="text" name="web2" class="form-control" placeholder="Enter a url website" ></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="website">Video URL</label>
                                <div class="col-sm-10">
                                    <input type="text" name="video" class="form-control" placeholder="Enter a url video" ></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="opening_hours">Opening Hours</label>
                                <div class="col-sm-10">
                                    <input type="time" name="opening_hours" class="form-control" placeholder="Enter Opening Hours e.g 11:00 am - 17:00 pm"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="closing_hours">Closing Hours</label>
                                <div class="col-sm-10"><input type="time" name="closing_hours" class="form-control" placeholder="Enter Closing Hours e.g 11:00 am - 17:00 pm"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="street">Street</label>
                                <div class="col-sm-10"><input type="text" name="street" class="form-control" placeholder="Enter Street"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="house_no">House Number</label>
                                <div class="col-sm-10"><input type="text" name="house_no" class="form-control" placeholder="Enter House No"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="post_code">Post Code</label>
                                <div class="col-sm-10"><input type="text" name="post_code" class="form-control" placeholder="Enter Post Code"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="city">City</label>
                                <div class="col-sm-10"><input type="text" name="city" class="form-control" placeholder="Enter City"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label" for="country">Country</label>
                                <div class="col-sm-10"><input type="text" name="country" class="form-control" placeholder="Enter Country"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            {{----}}
                            <div class="form-group">
                                <label for="input-logo_image">Logo Image : </label>
                                <input type="file" name="input-logo_image" id="input-logo_image" class="file">
                            </div>
                            <div class="form-group">
                                <label for="input-cover_image">Cover Image : </label>
                                <input type="file" name="input-cover_image" id="input-cover_image" class="file">
                            </div>
                            <div class="form-group">
                                <label for="input-logo_image">Feature Image : </label>
                                <input type="file" name="input-feature_images[]" id="input-feature_images" class="file" multiple>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="width:100%;">Add Business</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $("#input-cover_image").fileinput({
            'showUpload': false
        });
        $("#input-logo_image").fileinput({
            'showUpload': false
        });
        $("#input-feature_images").fileinput({
            showUpload: false,
            maxFileCount: 3
        });
    </script>
@endsection