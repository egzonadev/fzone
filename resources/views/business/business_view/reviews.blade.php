@extends('layout.business_layout')
@section('sub-nav')

    <div class="row">
        <div class="col-lg-12" style="margin-top: -8px">
            <hr>
            <div class="row" style="margin-top: 5%"></div>
            <div class="col-lg-6  col-lg-offset-1">
            @foreach($reviews as $review)
                <div class="social-feed-box" style="border-left: none; border-top: none; border-right: none">
                    <div class="pull-right social-action">
                        <p style="color: red">
                         
                            @for ($i = 0; $i < $review->rating; $i++) 
                            <i class="fa fa-star"></i>  
                            @endfor 
                            
                            @for ($i = $review->rating; $i < 5; $i++)
                            {{-- <i class="fa fa-star-half-o"></i>  --}}
                            <i class="fa fa-star-o"></i> 
                            @endfor  
                        </p>
                    </div>
                    <div class="social-avatar">
                        <a href="#" class="pull-left">
                            <img class="img-rounded" alt="image" src="/assets/img/a1.jpg">
                        </a>
                        <div class="media-body">
                            <a href="#">
                                {{$review->name}}
                            </a>
                            <small class="text-muted fa fa-clock-o" style="color: #cc5965"> {{ date("l h:i:a", strtotime( $review->created_at )) }} - {{  date("d-m-Y", strtotime( $review->created_at )) }}
                            </small>
                        </div>
                    </div>
                    <div class="social-body">
                        <p>
                           {{$review->message}}
                        </p>

                        <div class="btn-group">
                            <button class="btn btn-white btn-xs"><i class="fa fa-comment-o"></i> Reply</button>
                        </div>
                    </div>
                </div>
            @endforeach

            {{$reviews->links()}}
            </div>
              
            
            <div class="col-lg-4 col-lg-offset-1">
                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" style="height: 40px; font-size: 14px"><i
                                    class="fa fa-clock-o"> Opening hours</i></button>
                        <button type="button" class="btn btn-default"
                                style="height: 40px; border-left: none; font-size: 14px"><i>{{$business->opening_hours }} am</i></button>
                               <button type="button" class="btn btn-default"
                                       style="height: 40px; border-left: none; width: auto"><i>-</i></button>
                        <button type="button" class="btn btn-default"
                                style="height: 40px; border-left: none; font-size: 14px"><i>{{$business->closing_hours }} pm</i></button>
                    </span>
                </div>
                <div class="ibox-content">
                    <h3>Add a review
                        <small class="pull-right">
                            <p style="color: red">

                                @for ($i = 0; $i < $avg_reviews; $i++) 
                                <i class="fa fa-star"></i>  
                                @endfor 
                                
                                @for ($i = $avg_reviews; $i < 5; $i++)
                                {{-- <i class="fa fa-star-half-o"></i>  --}}
                                <i class="fa fa-star-o"></i> 
                                @endfor
                                ({{$count_reviews}} Rating)
                            </p>
                        </small>
                    </h3>
                        <div>
                            <div>
                                <p style="color: grey; cursor:pointer;" id="5_review" onclick="AddReview(5)">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </p>
                            </div>
                            <div class="progress progress-small">
                                <div style="width: 100%;" class="progress-bar progress-bar-gray"></div>
                            </div>

                            <div>
                                <p style="color: grey; cursor:pointer;" id="4_review" onclick="AddReview(4)">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </p>
                            </div>
                            <div class="progress progress-small">
                                <div style="width: 90%;" class="progress-bar progress-bar-gray"></div>
                            </div>

                            <div>
                                <p style="color: grey; cursor:pointer;" id="3_review" onclick="AddReview(3)">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </p>
                            </div>
                            <div class="progress progress-small">
                                <div style="width: 70%;" class="progress-bar progress-bar-gray"></div>
                            </div>

                            <div>
                                <p style="color: grey; cursor:pointer;" id="2_review" onclick="AddReview(2)">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </p>
                            </div>
                            <div class="progress progress-small">
                                <div style="width: 50%;" class="progress-bar progress-bar-gray"></div>
                            </div>

                            <div>
                                <p style="color: grey; cursor:pointer;" id="1_review" onclick="AddReview(1)">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </p>
                            </div>
                            <div class="progress progress-small">
                                <div style="width: 20%;" class="progress-bar progress-bar-gray"></div>
                            </div>
                        </div>
                        <b id="btnRatingValidation"></b>
                </div>
                <script>
                    function AddReview(rating){
                        $('p[id*=_review]').css('color','grey');
                        $('#' + rating + '_review').css('color','red'); 
                        $('#rating').val(rating);
                    }
                </script> 
                <div>
                    <div class="ibox float-e-margins">
                    </div>
                    <h3>Write a review</h3>
                    @include('includes.messages')
                    <form method="post" action="{{action('BusinessesController@add_review', $business->id)}}" class="form-horizontal">
                        {{csrf_field()}}
                        <div style="margin-top: 5%">
                        <input type="text" name="name" value="{{Auth::check() ? Auth::user()->name : ''}}" 
                                maxlength="255" placeholder="Your name" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%">
                            <input type="email" name="email" value="{{Auth::check() ? Auth::user()->email : ''}}" 
                                    maxlength="255" placeholder="Email" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%;">
                            <textarea  rows="4" name="message"  maxlength="450" placeholder="Message" class="form-control" required autofocus></textarea>
                        </div>
                        <input type="hidden" name="rating" id="rating" value="" /> 
                        <div style="margin-top: 5%">
                            <button class="btn btn-black" type="submit" id="btnSubmit" onclick="ValidateReview">Send</button>
                        </div>
                    </form>
                </div>
                <div class="social" style="margin-left: -10px; margin-top: 5px">

                    <ul>
                        <li><h3> Follow us</h3></li>
                        <br>
                        <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-envelope"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-whatsapp"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
@stop