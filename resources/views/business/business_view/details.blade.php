@extends('layout.business_layout')
@section('sub-nav')
    <link href="/assets/css/slick/slick.css" rel="stylesheet">
    <link href="/assets/css/slick/slick-theme.css" rel="stylesheet">
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>

    <div class="row">
        <div class="col-lg-12" style="margin-top: -8px"><hr>
            <div class="col-lg-6 col-lg-offset-1">
                @if($business->video != null)
                <a href="{{$business->video}}">
                    <div class="widget style1 red-bg">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="fa fa-youtube-play fa-5x"></i>
                            </div>
                            <div class="col-xs-8 text-right">
                                <span>To watch video</span>
                                <h2 class="font-bold">click here!</h2>
                            </div>
                        </div>
                    </div>
                </a>
                @endif
                <div class="ibox float-e-margins">
                    <div class="carousel slide" id="carousel1">
                        <div class="carousel-inner">
                            <div class="item active">
                                <img alt="image" class="img-responsive" src="/storage/business/cover/{{$business->path_cover_image}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick_demo_2">
                    @foreach( $business_features as $feature_image )
                        <div>
                            <div class="ibox-content">
                                <img style="height: 150px; width: 200px;" src="/storage/business/features/{{ $feature_image->path_feature_image}}" alt="" class="img-responsive">
                            </div>
                        </div>
                    @endforeach
                </div>
                <p>
                    {{$business->bio}}
                 </p>
                    <br> <br>
                 <h3>Follow us </h3>
             </div>
             <div class="col-lg-4 col-lg-offset-1">
                 <div class="client-avatar2">
                     <row>
                         <h3><strong>People who created event here</strong></h3><br>
                         <?php $attenders = App\Activity::whoCreatedEvent($business->id);
                         ?>
                         @foreach($attenders as $attender)
                             @if($attender['GetUser']->usertype == 'Individual')
                                 <img src="{{asset('/storage/profile/'.$attender['GetUser']->image)}}" />
                            @else
                                <img src="/storage/business/logo/{{$attender['getBusiness']->path_logo_image}}"/>
                            @endif
                        @endforeach
                        {{--<span class="m-l-xs"> + {{$count_attenders}} attenders</span>--}}
                    </row>
                </div>
                <div class="">
                    <h3><b>Contact Us</b></h3>
                    <p class="fa fa-map-marker cfbus">
                        <small class="cfbust">{{$business->street . ' ' . $business->house_no . ', ' . $business->post_code . ' ' . $business->city . ', ' . $business->country }}</small>
                    </p>
                    <p class="fa fa-phone cfbus">
                        <small class="cfbust"> {{$business->phone }}</small>
                    </p>
                    <p class="fa fa-envelope cfbus">
                        <small class="cfbust"> {{$business->email }}</small>
                    </p>
                    <p class="fa fa-globe cfbus">
                        <small class="cfbust"> {{$business->web1 }}</small>
                    </p>
                    <p class="fa fa-globe cfbus">
                        <small class="cfbust"> {{$business->web2 }}</small>
                    </p>
                </div>
                <div class="input-group">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" style="height: 40px; font-size: 14px"><i class="fa fa-clock-o"> Opening hours</i></button>
                        <button type="button" class="btn btn-default" style="height: 40px; border-left: none; font-size: 14px"><i>{{$business->opening_hours }} am</i></button>
                               <button type="button" class="btn btn-default" style="height: 40px; border-left: none; width: auto"><i>-</i></button>
                        <button type="button" class="btn btn-default" style="height: 40px; border-left: none; font-size: 14px"><i>{{$business->closing_hours }} pm</i></button>
                    </span>
                </div>
                <div class="ibox-content">
                    <p>
                        <a href="https://developers.google.com/maps/documentation/javascript/reference#MapOptions"></a>
                    </p>
                    <div class="google-map" id="map1"></div>
                </div>
                <div >
                    <div class="ibox float-e-margins">
                    </div>
                    <h2>Contact us</h2>
                    @include('includes.messages')
                    <form method="post" action="{{action('BusinessesController@contact_company', $business->id)}}" class="form-horizontal">
                        {{csrf_field()}}
                        <div style="margin-top: 5%">
                            <input type="text" name="name" value="{{Auth::check() ? Auth::user()->name : ''}}"
                                   maxlength="255" placeholder="Your name" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%">
                            <input type="email" name="email" value="{{Auth::check() ? Auth::user()->email : ''}}"
                                   maxlength="255" placeholder="Email" class="form-control" required autofocus {{Auth::check() ? ' disabled' : ''}}>
                        </div>
                        <div style="margin-top: 5%;">
                            <textarea  rows="4" name="message"  maxlength="450" placeholder="Message" class="form-control" required autofocus></textarea>
                        </div>
                        <div style="margin-top: 5%">
                            <button class="btn btn-black" type="submit">Send</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- slick carousel-->
    <script src="/assets/js/slick.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.slick_demo_1').slick({
                dots: true
            });
            $('.slick_demo_2').slick({
                infinite: true,
                slidesToShow: 2,
                slidesToScroll: 1,
                centerMode: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $('.slick_demo_3').slick({
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                adaptiveHeight: true
            });
        });

    </script>
    <script type="text/javascript">
        // When the window has finished loading google map
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Options for Google map
            // More info see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions1 = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400),
                // Style for Google Maps
                styles: [{
                    "featureType": "water",
                    "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
                }, {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
                }, {
                    "featureType": "road",
                    "elementType": "geometry.stroke",
                    "stylers": [{"color": "#808080"}, {"lightness": 54}]
                }, {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#ece2d9"}]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [{"color": "#ccdca1"}]
                }, {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#767676"}]
                }, {
                    "featureType": "road",
                    "elementType": "labels.text.stroke",
                    "stylers": [{"color": "#ffffff"}]
                }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
                }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                    "featureType": "poi.sports_complex",
                    "stylers": [{"visibility": "on"}]
                }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                    "featureType": "poi.business",
                    "stylers": [{"visibility": "simplified"}]
                }]
            };

            var mapOptions2 = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400),
                // Style for Google Maps
                styles: [{
                    "featureType": "all",
                    "elementType": "all",
                    "stylers": [{"invert_lightness": true}, {"saturation": 10}, {"lightness": 30}, {"gamma": 0.5}, {"hue": "#435158"}]
                }]
            };

            var mapOptions3 = {
                center: new google.maps.LatLng(36.964645, -122.01523),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                // Style for Google Maps
                styles: [{
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{"color": "#fffffa"}]
                }, {"featureType": "water", "stylers": [{"lightness": 50}]}, {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [{"visibility": "off"}]
                }, {"featureType": "transit", "stylers": [{"visibility": "off"}]}, {
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [{"lightness": 40}]
                }]
            };

            var mapOptions4 = {
                zoom: 11,
                center: new google.maps.LatLng(40.6700, -73.9400),
                // Style for Google Maps
                styles: [{"stylers": [{"hue": "#18a689"}, {"visibility": "on"}, {"invert_lightness": true}, {"saturation": 40}, {"lightness": 10}]}]
            };

            var fenway = new google.maps.LatLng(42.345573, -71.098326);
            var mapOptions5 = {
                zoom: 14,
                center: fenway,
                // Style for Google Maps
                styles: [{
                    featureType: "landscape",
                    stylers: [{saturation: -100}, {lightness: 65}, {visibility: "on"}]
                }, {
                    featureType: "poi",
                    stylers: [{saturation: -100}, {lightness: 51}, {visibility: "simplified"}]
                }, {
                    featureType: "road.highway",
                    stylers: [{saturation: -100}, {visibility: "simplified"}]
                }, {
                    featureType: "road.arterial",
                    stylers: [{saturation: -100}, {lightness: 30}, {visibility: "on"}]
                }, {
                    featureType: "road.local",
                    stylers: [{saturation: -100}, {lightness: 40}, {visibility: "on"}]
                }, {
                    featureType: "transit",
                    stylers: [{saturation: -100}, {visibility: "simplified"}]
                }, {
                    featureType: "administrative.province",
                    stylers: [{visibility: "off"}]/**/
                }, {
                    featureType: "administrative.locality",
                    stylers: [{visibility: "off"}]
                }, {
                    featureType: "administrative.neighborhood",
                    stylers: [{visibility: "on"}]/**/
                }, {
                    featureType: "water",
                    elementType: "labels",
                    stylers: [{visibility: "on"}, {lightness: -25}, {saturation: -100}]
                }, {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{hue: "#ffff00"}, {lightness: -25}, {saturation: -97}]
                }]
            };

            var panoramaOptions = {
                position: fenway,
                pov: {
                    heading: 10,
                    pitch: 10
                }
            };
            var panorama = new google.maps.StreetViewPanorama(document.getElementById('pano'), panoramaOptions);

            // Get all html elements for map
            var mapElement1 = document.getElementById('map1');
            var mapElement2 = document.getElementById('map2');
            var mapElement3 = document.getElementById('map3');
            var mapElement4 = document.getElementById('map4');

            // Create the Google Map using elements
            var map1 = new google.maps.Map(mapElement1, mapOptions1);
            var map2 = new google.maps.Map(mapElement2, mapOptions2);
            var map3 = new google.maps.Map(mapElement3, mapOptions3);
            var map4 = new google.maps.Map(mapElement4, mapOptions4);
        }
    </script>
    </body>
    </html>

@stop