
@extends('layout.master')
@section('page-content')
    <?php
    use App\BusinessFeature;
    $business_feature = new BusinessFeature();
    ?>
    <div class="container">
        <h2>All Businesses</h2> <a href="/business/create" class="btn btn-info">New Business</a>
        <br />
        <br />
        @foreach($user_businesses as $business)

            <div class="row">
                <div class="col-lg-2">
                    <img src="/storage/business/logo/{{$business->path_logo_image}}" style="width:100%;" />
                </div>
                <div class="col-lg-3">
                    <h2>{{$business->name}}</h2>
                    <a href="/business/{{$business->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                </div>
                <?php $business_features =  $business_feature->get_business_features($business->id); ?>
                @foreach( $business_features as $feature_image )
                    <div class="col-lg-2">
                        <img src="/storage/business/features/{{ $feature_image->path_feature_image}}" style="width:100%;" />
                    </div>
                @endforeach
            </div>
            <br />
        @endforeach
    </div>

@endsection