@extends('layout.business_master')
@section('business_dashboard')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox chat-view">
                {{-- <div class="ibox-title">
                     <small class="pull-right text-muted">Last message: Mon Jan 26 2015 - 18:39:23</small>
                     Chat room panel
                 </div>--}}
                {{--<div class="ibox-content">--}}
                <div class="row">
                    <div class="col-md-4" style="margin-right: -4%;">
                        <div class="chat-users ">
                            <div class="users-list">
                                @foreach($friends as $friend)
                                    <div class="chat-user">
                                        <img class="chat-avatar" src="/storage/profile/{{$friend->image}}" alt="">
                                        <div class="chat-user-name">
                                            {{-- <a id="user_{{$friend->follower_id}}" onclick="sel({{$friend->follower_id}})">{{$friend->name}}</a> --}}
                                            <a id="user_sel_{{$friend->follower_id}}" name="{{$friend->follower_id}}">{{$friend->name}}</a>
                                            <div id="user_new_{{$friend->follower_id}}"
                                                 style="display:none;width:20px;height:20px;border-radius:26%;background-color:#5285de;float:right;padding-left:5px;color:white;">
                                                <input type="hidden" id="hd_new_sms_us_{{$friend->follower_id}}" value="" />
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" style="display:none;" id="chat_div_main">
                        <div class="chat-discussion" id="chat_box">
                            {{-- <div class='chat-message left'>
                                <img class='message-avatar' src='/assets/img/a1.jpg'>
                                <div class='message'>
                                    <a class='message-author' href='#'> Michael Smith </a>
                                    <span class='message-date'> Mon Jan 26 2015 - 18:39:23 </span>
                                    <span class='message-content'>
                                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                    </span>
                                </div>
                            </div>
                            <div class="chat-message right">
                                <img class="message-avatar" src="/assets/img/a4.jpg" alt="">
                                <div class="message">
                                    <a class="message-author" href="#"> Karl Jordan </a>
                                    <span class="message-date">  Fri Jan 25 2015 - 11:12:36 </span>
                                    <span class="message-content">
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover.
                                        </span>
                                </div>
                            </div>   --}}
                        </div>
                        <div class="row">
                            <form role="form">
                                {{csrf_field()}}
                                <div class="col-lg-12" style="margin-top: 5px">
                                    <div class="input-group">
                                        <input type="text" id="message" name="message"
                                               class="form-control" style="height: 50px" autocomplete="off">
                                        <input type="hidden" id="hd_selu" name="hd_selu" value="">
                                        <span class="input-group-btn">
                                            <a class="btn btn-primary" style="height: 50px" id="btnSend">Send</a>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <script>
                            (function($) {
                                // sending messages
                                lim = 8;
                                data_length = 0;
                                $('a[id^=user_sel_]').click( function() {
                                    sel = $(this).attr('name');
                                    $('#chat_div_main').show(250);
                                    get_messages(sel, 8, 'normal' );
                                    $('#hd_selu').val(sel);
                                    $('#message').val('');
                                    $('#message').focus();
                                    //console.log('selected : ' + sel);
                                    save_msg_read( $('#hd_selu').val() );
                                });

                                function get_messages(sel, tk, from ){
                                    $.ajax({
                                        type: 'POST',
                                        url: "{{ URL::route('GetMessages') }}",
                                        dataType : 'json',
                                        data: {
                                            '_token': $('input[name=_token]').val(),
                                            'sel' : sel,
                                            'lim' : tk
                                        },
                                        success : function(data) {
                                            //console.log(data);
                                            left_right = "";
                                            msg_html = "";
                                            data_length = data.length;
                                            if( data.length > 0) {
                                                for (i = 0; i < data.length; i++) {
                                                    user_image = data[i].image;
                                                    user_name = data[i].name
                                                    send = data[i].sender_user_id;
                                                    receiver_user_id = data[i].receiver_user_id;
                                                    message = data[i].message;
                                                    created_at = data[i].created_at;

                                                    if(send == {{ Auth::id() }}){
                                                        left_right = "right";
                                                    }else{
                                                        left_right = "left";
                                                    }

                                                    msg_html += " <div class='chat-message " + left_right + "'>" +
                                                        "<img class='message-avatar' src='/storage/profile/" + user_image + "'>" +
                                                        "<div class='message'>" +
                                                        "    <a class='message-author' href='#'> " + user_name + " </a>" +
                                                        "    <span class='message-date'> " + created_at + " </span>" +
                                                        "    <span class='message-content'> " + message + " </span>" +
                                                        "</div>" +
                                                        "</div>";

                                                    $('#chat_box').html(msg_html);
                                                    if ( from != 'interval' ){
                                                        //document.getElementById('chat_box').scrollTop = 9999999;
                                                    }
                                                } // end of for loop

                                            } else {
                                                $('#chat_box').html( "" );
                                            }
                                        },
                                        error: function(e) {
                                            console.log(e.responseText);
                                        }
                                    });// end of ajax
                                }
                                // sending message
                                $('#btnSend').click(function(){
                                    ///message/send
                                    if($('#message').val() == ''){
                                        $('#message').css('border', '1px solid red');
                                    }else{
                                        send_msg();
                                        $('#chat_box').scrollTop($('#chat_box')[0].scrollHeight);
                                    }
                                });

                                $('#message').keypress(function(event){
                                    if( event.which == 13 ) {
                                        if($('#message').val() == ''){
                                            event.preventDefault();
                                            $('#message').css('border', '1px solid red');
                                        } else {
                                            send_msg();
                                            event.preventDefault();
                                            $('#chat_box').scrollTop($('#chat_box')[0].scrollHeight);
                                            save_msg_read( $('#hd_selu').val() );
                                        }
                                    } else {
                                        $('#message').css('border', '');

                                    }
                                });

                                function send_msg(){
                                    $.ajax({
                                        type: 'POST',
                                        url: "{{ URL::route('SendMessage') }}",
                                        data: {
                                            '_token': $('input[name=_token]').val(),
                                            'message': $('input[name=message]').val(),
                                            'sel' : $('#hd_selu').val()
                                        },
                                        success: function(data) {
                                            //console.log(data);
                                            get_messages( $('#hd_selu').val() , 8, 'normal');
                                            $('input[name=message]').val('');
                                            $('input[name=message]').focus();
                                        },
                                        error: function(e) {
                                            console.log(e.responseText);
                                        }
                                    });
                                } // end of send message function

                                function check_if_new_message(){
                                    $.ajax({
                                        type: 'POST',
                                        url: "{{ URL::route('CheckNewMsg') }}",
                                        dataType : 'json',
                                        data: {
                                            '_token': $('input[name=_token]').val()
                                        },
                                        success : function(data) {
                                            if( data.length > 0) {
                                                for (i = 0; i < data.length; i++) {
                                                    sender_user_id = data[i].sender_user_id;
                                                    count_new_sms = data[i].count_new_sms
                                                    if( ! $('#user_new_' + sender_user_id).is(':visible') ){
                                                        $('#user_new_' + sender_user_id).show();
                                                        $('#user_new_' + sender_user_id).html(count_new_sms);
                                                        $('#hd_new_sms_us_' + sender_user_id).val(count_new_sms);

                                                    } else if( $('#user_new_' + sender_user_id).is(':visible') &&
                                                        $('#hd_new_sms_us_' + sender_user_id).val() != count_new_sms
                                                    ) {
                                                        $('#user_new_' + sender_user_id).html(count_new_sms);
                                                        $('#hd_new_sms_us_' + sender_user_id).val(count_new_sms);
                                                    }

                                                } // end of for loop
                                            }
                                        },
                                        error: function(e) {
                                            console.log(e.responseText);
                                        }
                                    });
                                } // end of send message function

                                function save_msg_read(sel){
                                    $.ajax({
                                        type: 'POST',
                                        url: "{{ URL::route('SaveMsgRead') }}",
                                        dataType : 'json',
                                        data: {
                                            '_token': $('input[name=_token]').val(),
                                            'sel' : sel
                                        },
                                        success : function(data) {
                                            //console.log(data);
                                            $('#user_new_' + sel).hide();
                                            $('#user_new_' + sel).html('');
                                        },
                                        error: function(e) {
                                            console.log(e.responseText);
                                        }
                                    });
                                }

                                setInterval(function(){

                                    check_if_new_message();

                                    if( $('#hd_selu').val() != '' ){
                                        get_messages( $('#hd_selu').val(), lim , 'interval');
                                    } else {
                                        // make some red dot indicating the message
                                    }
                                }, 1000 );
                                /* $('#chat_box').scroll(function(){
                                    if ($('#chat_box').scrollTop() == 0){
                                        //clearInterval( interval );
                                         lim = lim + 8;
                                         get_messages( $('#hd_selu').val(), lim , 'interval');
                                     } else {
                                         get_messages( $('#hd_selu').val(), lim , 'normal');
                                     }
                                 });*/

                                $('#chat_box').scroll(function(){
                                    if ($('#chat_box').scrollTop() == 0){
                                        // clearInterval( interval );
                                        lim = lim + 8;
                                        get_messages( $('#hd_selu').val(), lim , 'interval');
                                    } else {
                                        get_messages( $('#hd_selu').val(), lim , 'normal');
                                    }
                                });

                            }(jQuery));
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop