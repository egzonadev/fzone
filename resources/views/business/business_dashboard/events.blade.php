@extends('layout.business_master')
@section('business_dashboard')

    @if (\Session::has('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p><i class="icon fa fa-check"></i>{{\Session::get('success')}}</p>
    </div>
    @endif
    @if(count($activities) > 0) 
    <div>
        <h3 style="padding-left: 2%">Upcoming</h3>
    </div>
    
    <div class="landing-page">
        <div id="foodSlide" class="carousel carousel-fade" data-ride="carousel">
       
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    
                    @foreach($activities as $activity)
                    <div class="col-lg-4 col-md-4 col-xs-4">
                        <div class="widget-head-color-box navy-bg p-lg text-center"
                        style="background-image: url('{{asset('/images/categories/'.$activity->category->first()->image)}}')">
                            <img  src="/storage/business/logo/{{$activity['getBusiness']->path_logo_image}}" class="img-small-profile" alt="profile">
                            @if( ! App\ActivityInterested::isFollowingActivity($activity->id))
                            <?php $heart_style = '' ?>
                            <form method="GET" action="{{action('ActivitiesController@OwnerInterested', $activity->id)}}" class="form-horizontal">
                            @else 
                            <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                            <form method="GET" action="{{action('ActivitiesController@OwnerNotInterested', $activity->id)}}" class="form-horizontal">
                            @endif
                                {{csrf_field()}}
                                <button style="{{$heart_style}}" id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="submit">
                                    <i class="fa fa-heart"></i>
                                </button>
                            </form> 
                        </div>

                        <div class="widget-text-box">
                            <a href="{{action('EventsController@index', $activity->id)}}">
                                <h3 href="" class="media-heading m-t-md"><b>{{$activity->title}}</b></h3>
                            </a>
                            <h5 class="media-heading m-t-md" style="text-align: justify">
                                    {{$activity->description}}<br><br>
                            </h5>

                            <div class="client-avatar">
                                <row>
                                    <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                                style="font-size: 15px"></i></small>
                                    <span class="m-l-xs">{{ date("d/m/Y", strtotime( $activity->start_date_time ) ) }}</span>
                                    <?php 
                                    $attenders= App\Activity::GetAllActivityAttenders($activity->id);
                                    $count_attenders = App\Activity::GetAttendersCount($activity->id);
                                    ?>
                                    @foreach($attenders as $attender)
                                    <img alt="image" src="/storage/profile/{{$attender->image}}">
                                    @endforeach 
                                    <span class="m-l-xs"> + {{$count_attenders}} attenders</span>
                                </row>
                            </div>
                            <br>
                            <div id="did-divider" class="divider"></div>
                            <div class="text-right" style="  padding-top: 10px;">
                                <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                        {{$activity->location}}, {{$activity->city}}</i></small>
                                @if( ! App\ActivityAttender::isAttendingActivity($activity->id))
                                <?php $attending = 'Attend' ?>
                                <form method="GET" action="{{action('ActivitiesController@OwnerAttend', $activity->id)}}" class="form-horizontal">
                                @else 
                                <?php $attending = 'Attending' ?>
                                <form method="GET" action="{{action('ActivitiesController@OwnerRemoveAttend', $activity->id)}}" class="form-horizontal">
                                @endif
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-danger btn-xs"><strong>{{$attending}}</strong>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div> 
                    @endforeach

                </div>  

            </div>
            
        </div>
        <div class="row text-center">
                {{$activities->links()}}
        </div>

        {{--categories--}}
        <div class="row"> 
            <div>
                <h3 style="padding-left: 2%">Categories</h3>
            <div class="col-lg-2">
                <div class="widget style1 navy-bg">
                    <div class="row vertical-align">
                        <div class="col-xs-9 text-right">
                            <h3 class="font-bold">Culture</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="widget style1 navy-bg">
                    <div class="row vertical-align">
                        <div class="col-xs-9 text-right">
                            <h3 class="font-bold">Fun</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="widget style1 navy-bg">
                    <div class="row vertical-align">
                        <div class="col-xs-9 text-right">
                            <h3 class="font-bold">Sport</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="widget style1 lazur-bg">
                    <div class="row vertical-align">
                        <div class="col-xs-9 text-right">
                            <h3 class="font-bold">Sport</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="widget style1 lazur-bg">
                    <div class="row vertical-align">
                        <div class="col-xs-9 text-right">
                            <h3 class="font-bold">Sport</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="widget style1 yellow-bg">
                    <div class="row vertical-align">
                        <div class="col-xs-9 text-right">
                            <h3 class="font-bold">Sport</h3>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

    </div>
  @else 
  <div>
        <h3 style="padding-left: 2%">No Upcoming Events</h3>
    </div>
  @endif
<script>
    $(function(){
        $('li[id^=business-sel-]').removeClass( 'active' );
        $('#business-sel-events').addClass( 'active' ); 
    });
</script> 
@stop 