@extends('layout.business_master')
@section('business_dashboard')
    @if (\Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-check"></i>{{\Session::get('success')}}</p>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="widget blue-bg p-lg text-center">
                <div class="m-b-md">
                    <h4 class="m-xs">Weekly Visits</h4>
                    <h2 class="font-bold no-margins" style="padding-top: 10px" id="weekly-visits"> 
                        {{App\BusinessVisits::GetWeeklyVisitsCount($business->id)}}
                    </h2>
                    {{--<small>We detect the error.</small>--}}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="widget black-bg p-lg text-center">
                <div class="m-b-md">
                    <h4 class="m-xs">Total Visits</h4>
                    <h2 class="font-bold no-margins" style="padding-top: 10px">
                        {{App\BusinessVisits::VisitsCount($business->id)}}
                    </h2>
                    {{--<small>We detect the error.</small>--}}
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4 col-xs-12">
            <div class="widget red-bg p-lg text-center">
                <div class="m-b-md">
                    <h4 class="m-xs">Total Reviews</h4>
                    <h2 class="font-bold no-margins" style="padding-top: 10px"> 
                        {{ App\BusinessReviews::CountReviews($business->id)}} 
                    </h2>
                    {{--<small>We detect the error.</small>--}}
                </div>
            </div>
        </div>
    </div>
     
    <div class="row">
        <div class="col-lg-8">
            <div id="line-chart"></div>
        </div>
        <script>
            Morris.Line({
                element: 'line-chart',
                data: [ 
                    @for ($i = date('Y'); $i > (date('Y')-6); $i--)
                    { y: '{{$i}}', a: {{App\Business::getVisitsGraphData( $business->id, $i )}},
                                   b: {{App\Business::getReviewsGraphData( $business->id, $i )}} },  
                    @endfor
                    ],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['Total Visits', 'Total Reviews'],
                hideHover: 'auto',
                resize: true,
                lineColors: ['#54cdb4','#1ab394'],
            });
        </script>
    </div>
    <script>
        $(function(){ 
            $('li[id^=business-sel-]').removeClass( 'active' );
            $('#business-sel-dashboard').addClass( 'active' ); 
        });
    </script>
@stop