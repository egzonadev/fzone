@extends('layout.business_master')
@section('business_dashboard')

<div class="wrapper wrapper-content  animated fadeInRight">

    <div class="row">
        <div class="col-lg-6">
            @if(count($interested) > 0)
            <h3>Interested</h3>
            @else
            <h3>No Interested Notifications</h3>
            @endif
            @foreach($interested as $interest)
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/storage/profile/{{$interest->image}}">
                </a>
                <div class="media-body ">
                <h6><b>{{$interest->name}}</b> is coming to your event</h6>
                <small style="color: #cc5965">{{$interest->city}}</small>
                </div>
            </div> 
            @endforeach
            {{$interested->links()}}
        </div>
        <div class="col-lg-6">
            <h3>Reviews</h3>
            @foreach($reviews as $review)
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/storage/profile/{{ App\BusinessReviews::UserImage($review->email)}}">
                </a>
                <div class="media-body ">
                    <h6><b>{{$review->name}}</b> wrote a review</h6>
                    <small style="color: #cc5965"> {{ App\BusinessReviews::UserLocation($review->email)}}</small>
                </div>
            </div>
            @endforeach
            {{$reviews->links()}}
        </div>
    </div>
    <div class="divider" style="padding: 5%"></div>
    <div  class="row">
        <div class="col-lg-6">
            <h3>New Friends</h3>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <a href="">See More</a>
        </div>
        <div class="col-lg-6">
            <h3>Likes</h3>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <div class="chat-element">
                <a href="#" class="pull-left">
                    <img alt="image" class="img-circle" src="/assets/img/a2.jpg">
                </a>
                <div class="media-body ">
                    <h6><b>Mike Smith</b> is coming to your event</h6>
                    <small style="color: #cc5965">Bern</small>
                </div>
            </div>
            <a href="">See More</a>
        </div>
    </div>
</div>
<script>
    $(function(){ 
        $('li[id^=business-sel-]').removeClass( 'active' );
        $('#business-sel-notification').addClass( 'active' ); 
    });
</script>
@stop