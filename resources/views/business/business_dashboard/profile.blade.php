@extends('layout.master')
@section('main_content')

    <div class="business-background-img" style=" background-image: url('/assets/img/www.jpg');">
    </div>
    <div class="container">
        <div class="row border-bottom white-bg page-heading">
            <div class="col-lg-2">
                <div id="businrss-profile-pic" class="col-lg-2 text-center">
                    <div class="text-center">
                        <img src="assets/img/aaaa.jpg" class="img-square img-md"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="b-r-n-r">
                    <h2 class="media-heading m-t-md"><b>Grand Hotel Zermatterhof</b></h2>
                    <h4>Pizza, Food, Drinks</h4>
                    <p style="color: red">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </p>
                    <h4 style="color: red">Open Now</h4>
                    {{--    <ol class="breadcrumb">
                            <li>
                                <a href="index-2.html">Home</a>
                            </li>
                            <li>
                                <a>Layouts</a>
                            </li>
                            <li class="active">
                                <strong>Outlook view</strong>
                            </li>
                        </ol>--}}
                </div>
            </div>
            <div class="col-lg-2" style="margin-top: 5px">
                <button class="btn btn-danger " type="button"><i class="fa fa-plus-circle"></i> UPGRADE TO PREMIUM
                </button>
            </div>
            <div class="col-lg-10 col-md-9 col-md-offset-3 col-xs-12 pull-right" >
                    <ul class="nav navbar-top-links" style="margin-bottom: -20px; margin-left: 5% ">
                        <li href="dashboard" class="active">
                            <a class="dropdown-toggle count-info" href="dashboard">
                                <img src="/assets/img/icon/icons8-user-64.png" alt="" id="user-icon-mini-navbar">
                                <span class="user-mini-navbar">Dashboard</span>
                            </a>
                        </li>
                        <li href="events">
                            <a class="dropdown-toggle count-info" href="events">
                                <img src="/assets/img/icon/activity.png" alt="" id="user-icon-mini-navbar">
                                <span class="user-mini-navbar">Events</span>
                            </a>
                        </li>
                        <li href="notification">
                            <a class="dropdown-toggle count-info" href="notification">
                                <img src="/assets/img/icon/icons-notificatio.png" alt="" id="user-icon-mini-navbar">
                                <span class="user-mini-navbar">Notification</span>
                            </a>
                        </li>
                        <li href="#">
                            <a class="dropdown-toggle count-info" href="#">
                                <img href="add-activity" src="/assets/img/icon/emails.png" alt=""
                                     id="user-icon-mini-navbar">
                                <span href="/add-activity" class="user-mini-navbar">Messages</span>
                            </a>
                        </li>
                    </ul>
            </div>
        </div>
        <div class="fh-breadcrumb">
            <div class="fh-column">
                <div class="full-height-scroll">
                    <ul class="list-group elements-list">
                        <li class="list-group-item active mmm">
                            <a data-toggle="tab" href="#tab-1">
                                <strong>Business 1</strong>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a data-toggle="tab" href="#tab-2">
                                <strong>Business 2</strong>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a data-toggle="tab" href="#tab-3">
                                <strong>Business 3</strong>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="full-height">
                <div class="full-height-scroll white-bg border-left">
                    <div class="element-detail-box">
                        <div class="tab-content">
                            <div id="tab-1" class="active tab-pane">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="widget blue-bg p-lg text-center">
                                            <div class="m-b-md">
                                                <h4 class="m-xs">Weekly Visits</h4>
                                                <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                    25
                                                </h2>
                                                {{--<small>We detect the error.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="widget black-bg p-lg text-center">
                                            <div class="m-b-md">
                                                <h4 class="m-xs">Total Visits</h4>
                                                <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                    145
                                                </h2>
                                                {{--<small>We detect the error.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="widget red-bg p-lg text-center">
                                            <div class="m-b-md">
                                                <h4 class="m-xs">Total Reviews</h4>
                                                <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                    67
                                                </h2>
                                                {{--<small>We detect the error.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div id="line-chart"></div>
                                    </div>
                                    <script>
                                        Morris.Line({
                                            element: 'line-chart',
                                            data: [{ y: '2006', a: 100, b: 90 },
                                                { y: '2007', a: 75, b: 65 },
                                                { y: '2008', a: 50, b: 40 },
                                                { y: '2009', a: 75, b: 65 },
                                                { y: '2010', a: 50, b: 40 },
                                                { y: '2011', a: 75, b: 65 },
                                                { y: '2012', a: 100, b: 90 } ],
                                            xkey: 'y',
                                            ykeys: ['a', 'b'],
                                            labels: ['Series A', 'Series B'],
                                            hideHover: 'auto',
                                            resize: true,
                                            lineColors: ['#54cdb4','#1ab394'],
                                        });
                                    </script>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="col-lg-4 col-sm-4 col-xs-12">
                                    <div class="widget blue-bg p-lg text-center">
                                        <div class="m-b-md">
                                            <h4 class="m-xs">Weekly Visits</h4>
                                            <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                25
                                            </h2>
                                            {{--<small>We detect the error.</small>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4 col-xs-12">
                                    <div class="widget black-bg p-lg text-center">
                                        <div class="m-b-md">
                                            <h4 class="m-xs">Total Visits</h4>
                                            <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                145
                                            </h2>
                                            {{--<small>We detect the error.</small>--}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-4 col-xs-12">
                                    <div class="widget red-bg p-lg text-center">
                                        <div class="m-b-md">
                                            <h4 class="m-xs">Total Reviews</h4>
                                            <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                67
                                            </h2>
                                            {{--<small>We detect the error.</small>--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Area Chart Example
                                                <small>With custom colors.</small>
                                            </h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <i class="fa fa-wrench"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-user">
                                                    <li><a href="#">Config option 1</a>
                                                    </li>
                                                    <li><a href="#">Config option 2</a>
                                                    </li>
                                                </ul>
                                                <a class="close-link">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content" style="position: relative">
                                            <div id="morris-area-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="widget blue-bg p-lg text-center">
                                            <div class="m-b-md">
                                                <h4 class="m-xs">Weekly Visits</h4>
                                                <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                    25
                                                </h2>
                                                {{--<small>We detect the error.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="widget black-bg p-lg text-center">
                                            <div class="m-b-md">
                                                <h4 class="m-xs">Total Visits</h4>
                                                <h2 class="font-bold no-margins" style="padding-top: 10px">
                                                    145
                                                </h2>
                                                {{--<small>We detect the error.</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-12">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Line Chart Example </h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                        <i class="fa fa-wrench"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-user">
                                                        <li><a href="#">Config option 1</a>
                                                        </li>
                                                        <li><a href="#">Config option 2</a>
                                                        </li>
                                                    </ul>
                                                    <a class="close-link">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content">
                                                <div id="morris-line-chart"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop