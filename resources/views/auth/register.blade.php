<!doctype html>
<html lang="en">
<head>
    @include('auth.includes.head')
</head>
<body>
<!--   Big container   -->
<div class="container">
    <div class="row">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br/>
        @elseif($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none">{{ $error }} <b>Please try again.</b></li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-sm-6 col-sm-offset-3">
            <div class="wizard-container">
                <div class="card wizard-card" data-color="red" id="wizardProfile">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="wizard-header">
                            <h3 class="wizard-title">
                                CREATE AN ACCOUNT
                            </h3>
                        </div>
                        <div class="wizard-navigation">
                            <ul>
                                <li><a href="#step1" data-toggle="tab">STEP 1</a></li>
                                <li><a href="#step2" data-toggle="tab">STEP 2</a></li>
                                <li><a href="#step3" data-toggle="tab">STEP 3</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane" id="step1">
                                <div class="row">
                                    <div class="col-sm-12">
                                    </div>
                                    <input type="hidden" name="usertype" value="Individual">
                                    {{--<div class="col-sm-10 col-sm-offset-1">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Account Type
                                                <small>(required)</small>
                                            </label>
                                            <select name="usertype" class="form-control">
                                                <option value="Individual"> Individual</option>
                                                <option value="Business"> Business</option>
                                            </select>
                                        </div>
                                    </div>--}}
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">face</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Full Name
                                                    <small>(required)</small>
                                                </label>
                                                <input name="name" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">face</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Username
                                                    <small>(required)</small>
                                                </label>
                                                <input name="username" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">email</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email
                                                    <small>(required)</small>
                                                </label>
                                                <input name="email" type="email" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock_outline</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Your Password</label>
                                                <input name="password" type="password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock_outline</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Confirm Password</label>
                                                <input name="password_confirmation" type="password"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step2">
                                <h4 class="info-text"> Select Your Interests </h4>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        @foreach($interests as $interest)
                                            <div class="col-sm-4">
                                                <div class="choice" data-toggle="wizard-checkbox"
                                                     id="category_{{$interest->id}}">
                                                    <input type="checkbox" name="category_{{$interest->id}}"
                                                           id="category">
                                                    <div class="icon-square">
                                                        <img src="{{asset('/images/categories/'.$interest->image)}}"
                                                             style="width: 106px; height:62px;">
                                                        <h6 style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-weight: bold">{{$interest->interest}}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <div class="icon-square" id="select_all">
                                                    <h6> Alle</h6>
                                                </div>
                                                <input type="hidden" value="false" id="checked_all">
                                            </div>
                                        </div>
                                        <script>
                                            $('#select_all').click(function () {
                                                console.log('clicked');
                                                if ($('#checked_all').val() == 'false') {
                                                    $('#checked_all').val('true');
                                                    $('div[id^=category_]').addClass('active');
                                                    $('input[name^=category_]').attr('checked', true);
                                                } else {
                                                    $('#checked_all').val('false');
                                                    $('div[id^=category_]').removeClass('active');
                                                    $('input[name^=category_]').attr('checked', false);
                                                }
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                            <input type="checkbox" name="agree1">
                                            Yes, I would lie to be informed about leisure tps, promotions and offers of
                                                    Freizeitzone
                                                    GmbH by e-mail. The data will be used exclusively for this purpose and will not
                                                    be disclosed to third parties.
                                                    My agreement can be revoked at any time. The general information on data
                                                    protection can be viewed
                                                    <a href="#">here</a>.
                                </div>
                            </div>
                            <div class="tab-pane" id="step3">
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <div class="picture-container">
                                            <div class="picture">
                                                <img src="/wizard/assets/img/default-avatar.png" class="picture-src"
                                                     id="wizardPicturePreview" title=""/>
                                                <input type="file" id="wizard-picture" name="image">
                                            </div>
                                            <h6>Profile Picture</h6>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Gender
                                                <small>(required)</small>
                                            </label>
                                            <select class="form-control" name="gender">
                                                <option disabled="" selected=""></option>
                                                <option value="male"> Male</option>
                                                <option value="female"> Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="form-group label-floating">
                                                <span>Birthday</span>
                                                <input name="age" type="date" class="form-control" required autofocus>
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Phone Number
                                                    <small>(required)</small>
                                                </label>
                                                <input name="phone" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Street Name
                                                    <small>(required)</small>
                                                </label>
                                                <input name="street" type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Plz/Ort
                                                    <small>(required)</small>
                                                </label>
                                                <input name="pcode" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <input type="checkbox" id="agreecheck" required autofocus>
                                            I totally agree to share my information, by the law of GPRD..
                                        </div>
                                        <script>
                                            function myFunction() {
                                                document.getElementById("agreecheck").required;
                                                document.getElementById("agreecheck1").required;
                                            }
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next'
                                       value='Next' onclick="javascript:myFunction()"/>
                                <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish'
                                       value='Finish'/>
                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd'
                                       name='previous' value='Previous'/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
