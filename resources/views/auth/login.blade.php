<!doctype html>
<html lang="en">
<head>
@include('auth.includes.head')
</head>
<body>
<!--   Big container   -->
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <!--      Wizard container        -->
            <div class="wizard-container">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="list-style: none">{{ $error }} <b>Please try again.</b></li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card wizard-card" data-color="red" id="wizardProfile">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="wizard-header">
                        </div>
                        <div class="wizard-navigation">
                            <ul>
                                <li><a href="#address" data-toggle="tab">Sign In</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane" id="address">
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">email</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email</label>
                                                <input name="email" type="email" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock_outline</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Password</label>
                                                <input name="password" type="password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
                                            <div class="col-sm-8">
                                                <input type='submit' class='btn btn-danger btn-wd'name='signin' value='Sign In' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="footer-checkbox">
                                            <div class="col-sm-4 col-sm-offset-2 col-xs-offset-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="optionsCheckboxes">
                                                    </label>Remember me
                                                </div>
                                            </div>
                                            <div class="col-sm-4 col-sm-offset-2 col-xs-offset-1">
                                                <a href="">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-offset-2 col-xs-offset-1">
                                        <a href="/signup" class='btn btn-default btn-wd col-lg-10 col-xs-10'name='Create'>Create an account</a>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
