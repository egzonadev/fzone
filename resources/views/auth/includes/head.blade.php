<link rel="apple-touch-icon" sizes="76x76" href="register/assets/img/apple-icon.png" />
<link rel="icon" type="image/png" href="register/assets/img/favicon.png" />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<!-- CSS Files -->
<link href="/wizard/assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="/wizard/assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<script src="/wizard/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="/wizard/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/wizard/assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<!--  Plugin for the Wizard -->
<script src="/wizard/assets/js/material-bootstrap-wizard.js"></script>
<script src="/wizard/assets/js/jquery.validate.min.js"></script>