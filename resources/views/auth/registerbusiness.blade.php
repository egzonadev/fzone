<!doctype html>
<html lang="en">
<head>
    @include('auth.includes.head')
</head>
<body>
<!--   Big container   -->
<div class="container">
    <div class="row">
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br/>
        @elseif($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li style="list-style: none">{{ $error }} <b>Please try again.</b></li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-sm-6 col-sm-offset-3">
            <div class="wizard-container">
                <div class="card wizard-card" data-color="red" id="wizardProfile">
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="wizard-header">
                            <h3 class="wizard-title">
                                CREATE AN ACCOUNT
                            </h3>
                        </div>
                        <div class="wizard-navigation">
                            <ul>
                                <li><a href="#step1" data-toggle="tab">STEP 1</a></li>
                                <li><a href="#step2" data-toggle="tab">STEP 2</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane" id="step1">
                                <div class="row">
                                    <div class="col-sm-12">
                                    </div>
                                    <input type="hidden" name="usertype" value="Business">
                                    {{--<div class="col-sm-10 col-sm-offset-1">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Account Type
                                                <small>(required)</small>
                                            </label>
                                            <select name="usertype" class="form-control">
                                                <option value="Individual"> Individual</option>
                                                <option value="Business"> Business</option>
                                            </select>
                                        </div>
                                    </div>--}}
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <h4>Company Details</h4>
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">location_city</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Company Name
                                                    <small>(required)</small>
                                                </label>
                                                <input name="name" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">place</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Street Name and Number
                                                    <small>(required)</small>
                                                </label>
                                                <input name="street" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="col-sm-6">
                                            <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">inbox</i>
													</span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">PLZ
                                                    </label>
                                                    <input name="plz" type="text" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">inbox</i>
													</span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">ORT
                                                    </label>
                                                    <input name="ort" type="text" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">phone</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Company Phone Number
                                                    <small>(required)</small>
                                                </label>
                                                <input name="phone" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">email</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email
                                                    <small>(required)</small>
                                                </label>
                                                <input name="email" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock_outline</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Your Password</label>
                                                <input name="password" type="password" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock_outline</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Confirm Password</label>
                                                <input name="password_confirmation" type="password"
                                                       class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step2">
                                <div class="row">
                                    <div class="col-sm-12">
                                    </div>
                                    <input type="hidden" name="usertype" value="Business">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <h4>Contact Person Details</h4>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Your job title
                                                <small>(required)</small>
                                            </label>
                                            <select name="jobtitle" class="form-control" required autofocus>
                                                <option value="Owner">Owner</option>
                                                <option value="CEO">CEO</option>
                                                <option value="Head of Marketing">Head of Marketing</option>
                                                <option value="Head of Administration">Head of Administration</option>
                                                <option value="other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">face</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Full Name
                                                    <small>(required)</small>
                                                </label>
                                                <input name="contact_name" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">phone</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Contact Phone Number
                                                    <small>(required)</small>
                                                </label>
                                                <input name="contact_phone" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">email</i>
													</span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email
                                                    <small>(required)</small>
                                                </label>
                                                <input name="contact_email" type="text" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next'
                                       value='Next' onclick="javascript:myFunction()"/>
                                <input type='submit' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish'
                                       value='Finish'/>
                            </div>

                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd'
                                       name='previous' value='Previous'/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
