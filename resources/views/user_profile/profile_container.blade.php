@extends('user_profile.profile')
@section('profile-content')
    <div class="row">
        <div class="col-lg-7">
            <form action="{{action('UsersController@update')}}" method="post">
                {{csrf_field()}}
                <input name="_method" type="hidden" value="PUT">
                <div class="contact-box">
                    <div class="ibox">
                        <h5 style="color: #7da8c3">Bio</h5>
                        <div class="ibox-content">
                            <div class="scroll_content">
                                @if(Auth::user()->description == null)
                                    <textarea name="description" class="form-control" cols="90" rows="8"
                                              placeholder="Add a short description aboyt yourself..."></textarea>
                                @else
                                    <p style="text-align: justify">
                                            <textarea name="description" class="form-control" cols="90" rows="8">
                                                {{Auth::user()->description}}
                                            </textarea>
                                    </p>
                                @endif
                            </div>
                            <div style="float: left; padding-bottom: 4px">
                                <input type="submit" class="btn btn-default" value="Update">
                            </div>
                            <div style="float: right; padding-bottom: 4px">
                                <h4 style="color: #7da8c3">555/1000</h4>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-5">
            <div class="contact-box">
                <div class="ibox">
                    <h5 style="color: #7da8c3">Interest</h5>
                    <div class="ibox-content">
                        <div class="scroll_content">
                            <div class="row text-center" style="padding-bottom: 5%">
                                @foreach($user_interests as $row)
                                    <div class="col-sm-4">
                                        <h4>{{$row['interest']->interest}}</h4>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div style="float: right; padding-bottom: 4px">
                            <h4 style="color: #7da8c3"></h4>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
   {{-- <div class="row">
        <div class="col-lg-7">
            <div class="ibox">
                <div class="contact-box">
                    <a href="profile.html">
                        <div class="col-sm-4">
                            <div class="text-center">
                                <img alt="image" class="user-rateing " src="/assets/img/a2.jpg">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h2><strong>John Smith</strong></h2>
                            <p>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                            </p>
                            <address>
                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et
                                magnis
                            </address>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                    <a href="profile.html">
                        <div class="col-sm-4">
                            <div class="text-center">
                                <img alt="image" class="user-rateing " src="/assets/img/a2.jpg">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <h2><strong>John Smith</strong></h2>
                            <p>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                                <span class="fa fa-star star-checked"></span>
                            </p>
                            <address>
                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et
                                magnis
                            </address>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </div>

            </div>

        </div>
        <script>

            $(document).ready(function () {

                // Add slimscroll to element
                $('.scroll_content').slimscroll({
                    height: '200px'
                })

            });

        </script>
    </div>--}}
@stop