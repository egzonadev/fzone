@extends('user_profile.profile')
@section('profile-content')
    <div class="row ">
        <div class="col-lg-12">
            <div class="ibox-title">
                <div align="center">
                    <h2> <b> Edit Information</b></h2>
                </div>
            </div>
        <div class="col-lg-6 col-lg-offset-3">
            <div  class="ibox float-e-margins">
                <div class="ibox-content">
                    @foreach($user as $row)
                    <form method="get" class="form-horizontal">
                        <div class="form-group"><label class="col-lg-2 control-label">Account Type</label>
                            <div class="col-lg-10">
                                <input type="text" value="{{$row['usertype']}}" disabled="" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Full Name</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{$row['name']}}" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{$row['username']}}" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                                <input type="text" value="{{$row['email']}}" disabled="" class="form-control">
                            </div>
                        </div>
                        {{--<div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>--}}
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="gender">
                                    <option>{{$row['gender']}}</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Age</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{$row['age']}}" class="form-control" name="age">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Phone Number</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{$row['phone']}}" name="phone" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Street Name</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{$row['street']}}" class="form-control" name="street">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Plz/Otr</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{$row['pcode']}}" name="pcode" class="form-control"></div>
                        </div>

                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <button class="btn btn-danger" type="submit">Cancel</button>
                                <button class="btn btn-primary" type="submit" style="margin-left: 55px">Save changes</button>
                            </div>
                        </div>
                    </form>
                        @endforeach
                </div>
            </div>
        </div>
        </div>
    </div>
@stop

