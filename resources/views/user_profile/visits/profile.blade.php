@extends('user_profile.visits.master_visit')
@section('profile-content')
    <div class="row">
        <div class="col-lg-7">
                <div class="contact-box">
                    <div class="ibox">
                        <h5 style="color: #7da8c3">Bio</h5>
                        <div class="ibox-content">
                            <div class="scroll_content">
                                @foreach ($friend as $row)
                                <p style="text-align: justify">
                                    {{$row->description}}
                                </p>
                                    @endforeach
                            </div>
                            <div style="float: right; padding-bottom: 4px">
                                <h4 style="color: #7da8c3">555/1000</h4>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-lg-5">
            <div class="contact-box">
                <div class="ibox">
                    <h5 style="color: #7da8c3">Interest</h5>
                    <div class="ibox-content">
                        <div class="scroll_content">
                            <div class="row text-center" style="padding-bottom: 5%">
                                @foreach($friend_interests as $row)
                                    <div class="col-sm-4">
                                        <h4>{{$row['interest']->interest}}</h4>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div style="float: right; padding-bottom: 4px">
                            <h4 style="color: #7da8c3"></h4>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop