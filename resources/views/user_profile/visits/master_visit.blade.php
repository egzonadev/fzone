@extends('layout.master')
@section('main_content')
    <div class="user-background-img"></div>
    <div class="row shadow-lg p-3 mb-5 bg-white rounded">
        @foreach($friend as $user)
            <div id="user-profile-pic" class="col-lg-2 text-center">
                <div class="text-center">
                    <img src="{{asset('/storage/profile/'.$user->image)}}" class="img-circle img-md"/>
                    <h4><b>{{$user->name}}</b></h4><br>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="wrapper white-bg page-heading">
                    <ul class="nav navbar-top-links navbar-left" style="padding-top: 7%">
                        <li {{{ (Request::is('user/'.$user->id) ? 'class=active' : '') }}}>
                            <a class="dropdown-toggle count-info" href="{{ url('/user/'.$user->id) }}">
                                <img src="/assets/img/icon/icons8-user-64.png" alt="" id="user-icon-mini-navbar">
                                <span class="user-mini-navbar">Profile</span>
                            </a>
                        </li>
                        <li {{{ (Request::is('user/'.$user->id.'/activities') ? 'class=active' : '') }}}>
                            <a class="dropdown-toggle count-info"
                               href="{{action('UsersController@activities', $user->id)}}">
                                <img src="/assets/img/icon/activity.png" id="user-icon-mini-navbar">
                                <span class="user-mini-navbar">Activities</span>
                            </a>
                        </li>
                        <li {{{ (Request::is('user/'.$user->id.'/friends') ? 'class=active' : '') }}}>
                            <a class="dropdown-toggle count-info" href="{{ url('/user/'.$user->id.'/friends') }}">
                                <img src="/assets/img/icon/friends.png" alt="" id="user-icon-mini-navbar">
                                <span class="user-mini-navbar">Friends</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-lg-offset-1 pull-right" style="margin-top: 3%">
                <button type="button" class="btn btn-danger" style="height: 35px" title="Report"><i
                            class="fa fa-warning"></i></button>
                <button type="button" class="btn btn-outline btn-danger" style="height: 35px;" title="Send Message"><i
                            class="fa fa-envelope-o"></i></button>
                <form method="post" action="{{action('UsersController@addFriends', $user->id)}}">
                    {{csrf_field()}}
                    @if(App\Friend::friends($user->id))

                    @elseif(App\Friend::notfriends($user->id))
                        <button class="btn btn-danger" style="height: 35px" > PENDING
                        </button>
                    @else
                        <button type="submit" class="btn btn-danger" style="height: 35px" title="Add friend"> ADD FRIEND
                        </button>
                    @endif
                </form>
            </div>
        @endforeach
    </div>
    <div class="container-bg">
        <div class="page-container">
            @yield('profile-content')
        </div>
    </div>
@stop