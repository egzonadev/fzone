@extends('user_profile.visits.master_visit')
@section('profile-content')
    <div class="row">
        @foreach($friendsList as $friends)
        <div class="col-lg-4 col-sm-4 col-xs-4">
            <div class="">
                <a href="{{action('UsersController@profile', $friends['user']->id)}}">
                    <div class="col-sm-3">
                        <div class="text-center">
                            @if($friends['user']->usertype == 'Individual')
                                <img class="img-circle-friends m-t-xs img-responsive" src="/storage/profile/{{$friends['user']->image}}" alt="">
                            @else
                                <img src="/assets/img/user.jpg" class="img-circle-friends m-t-xs img-responsive"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h3><strong>{{$friends['user']->name}}</strong></h3>
                        <p style="color: #cc5965"> {{$friends['user']->street}}</p>
                    </div>
                    {{--<div class="col-sm-2">
                        <a class="btn btn-danger fa fa-plus" style=" font-size: 20px; "></a>
                    </div>--}}
                    <div class="clearfix"></div>
                </a>
            </div>
        </div>
        @endforeach
    </div>
@stop