@extends('user_profile.profile')
@section('profile-content')
    <div class="container" id="user-profile">
        @if(!$activities->isEmpty())
         @if(!$upcoming_activities->isEmpty())
            <div class="item active">
                <h3 style="padding-top: 10px">Upcoming</h3>
                <div class="row">
                    @foreach($upcoming_activities as $activity)
                        <div class="col-lg-4 col-md-4 col-xs-12" id="profile-card">
                            <div class="widget-head-color-box navy-bg p-lg text-center"
                                 style="background-image: url('{{asset('/images/categories/'.$activity->category->first()->image)}}')">
                                <img src="/storage/profile/{{$activity['GetUser']->image}}" class="img-small-profile" alt="profile">
                                @if( ! App\ActivityInterested::isFollowingActivity($activity->id))
                                    <?php $heart_style = '' ?>
                                    <form method="GET" action="{{action('ActivitiesController@addInterested', $activity->id)}}" class="form-horizontal">
                                        @else
                                            <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                                            <form method="GET" action="{{action('ActivitiesController@addNotInterested', $activity->id)}}" class="form-horizontal">
                                                @endif
                                                {{csrf_field()}}
                                                <button style="{{$heart_style}}" id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="submit">
                                                    <i class="fa fa-heart"></i>
                                                </button>
                                            </form>
                            </div>
                            <div class="widget-text-box">
                                <a href="{{action('EventsController@index', $activity->id)}}">
                                    <h3 href="" class="media-heading m-t-md"><b>{{$activity->title}}</b></h3>
                                </a>
                                <h5 class="media-heading m-t-md" style="max-width: 270px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                    {{$activity->description}}<br><br>
                                </h5>
                                <div class="client-avatar">
                                    <row>
                                        <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                                  style="font-size: 15px"></i>
                                        </small>
                                        <span class="m-l-xs">{{ date("d/m/Y", strtotime( $activity->start_date_time ) ) }}  </span>
                                        <?php
                                        $attenders = App\Activity::GetAllActivityAttenders($activity->id);
                                        $count_attenders = App\Activity::GetAttendersCount($activity->id);
                                        ?>
                                        @foreach($attenders as $attender)
                                            <img alt="image" src="/storage/profile/{{$attender->image}}">
                                        @endforeach
                                        <span class="m-l-xs"> + {{$count_attenders}} attenders</span>
                                    </row>
                                </div>
                                <br>
                                <div id="did-divider" class="divider"></div>
                                <div class="text-right" style="  padding-top: 10px;">
                                    <small id="locaion-in-cards" class="icon" style="color: #cc5965">
                                        <i class="fa fa-map-marker">
                                            {{$activity->location . ', ' . $activity->city }}</i></small>
                                    @if( ! App\ActivityAttender::isAttendingActivity($activity->id))
                                        <?php $attending = 'Attend' ?>
                                        <form method="GET"
                                              action="{{action('ActivitiesController@Attend', $activity->id)}}"
                                              class="form-horizontal">
                                            @else
                                                <?php $attending = 'Attending' ?>
                                                <form method="GET"
                                                      action="{{action('ActivitiesController@RemoveAttend', $activity->id)}}"
                                                      class="form-horizontal">
                                                    @endif
                                                    {{csrf_field()}}
                                                    <button type="submit" class="btn btn-danger btn-xs">
                                                        <strong>{{$attending}}</strong>
                                                    </button>
                                                </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
         @endif
         @if(!$previous_activities->isEmpty())
            <div class="item active">
                <h3 style="padding-top: 5%">Previous</h3>
                <div class="row">
                    @foreach($previous_activities as $activity)
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="widget-head-color-box navy-bg p-lg text-center"
                                 style="background-image: url('{{asset('/images/categories/'.$activity->category->first()->image)}}')">
                                <img src="/storage/profile/{{$activity['GetUser']->image}}" class="img-small-profile" alt="profile">
                                <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline"
                                        type="button"><i
                                            class="fa fa-heart"></i></button>
                            </div>
                            <div class="widget-text-box">
                                <a href="{{action('EventsController@index', $activity->id)}}">
                                    <h3 href="" class="media-heading m-t-md"><b>{{$activity->title}}</b></h3>
                                </a>
                                <h5 class="media-heading m-t-md" style="text-align: justify">
                                    {{$activity->description}}<br><br>
                                </h5>
                                <div class="client-avatar">
                                    <row>
                                        <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                                  style="font-size: 15px"></i>
                                        </small>
                                        <span class="m-l-xs">{{ date("d/m/Y", strtotime( $activity->start_date_time ) ) }}  </span>
                                        <?php
                                        $attenders = App\Activity::GetAllActivityAttenders($activity->id);
                                        $count_attenders = App\Activity::GetAttendersCount($activity->id);
                                        ?>
                                        @foreach($attenders as $attender)
                                            <img alt="image" src="/storage/profile/{{$attender->image}}">
                                        @endforeach
                                        <span class="m-l-xs"> + {{$count_attenders}} attenders</span>
                                    </row>
                                </div>
                                <br>
                                <div id="did-divider" class="divider"></div>
                                <div class="text-right" style="  padding-top: 10px;">
                                    <small id="locaion-in-cards" class="icon" style="color: #cc5965">
                                        <i class="fa fa-map-marker">
                                            {{$activity->location . ', ' . $activity->city }}</i></small>
                                    @if( ! App\ActivityAttender::isAttendingActivity($activity->id))
                                        <?php $attending = 'Attend' ?>
                                        <form method="GET"
                                              action="{{action('ActivitiesController@Attend', $activity->id)}}"
                                              class="form-horizontal">
                                            @else
                                                <?php $attending = 'Attending' ?>
                                                <form method="GET"
                                                      action="{{action('ActivitiesController@RemoveAttend', $activity->id)}}"
                                                      class="form-horizontal">
                                                    @endif
                                                    {{csrf_field()}}
                                                    <button type="submit" class="btn btn-danger btn-xs">
                                                        <strong>{{$attending}}</strong>
                                                    </button>
                                                </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
        @else
            <div class="alert alert-info">
                There are no activities under you account. <a class="alert-link" href="/activity/create"> Click here to create your first activity.</a>
            </div>
        @endif
    </div>
@stop