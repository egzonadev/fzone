@extends('layout.master')
@section('main_content')
    <div class="user-background-img"></div>
    <div class="row shadow-lg p-3 mb-5 bg-white rounded">
        <div id="user-profile-pic" class="col-lg-2 text-center">
            <div class="text-center" >
                <img src="{{asset('/storage/profile/'.Auth::user()->image)}}" class="img-circle img-md"/>
                <h4><b>{{Auth::user()->name}}</b></h4><br>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="wrapper white-bg page-heading">
                <ul class="nav navbar-top-links navbar-left" style="padding-top: 7%">
                    <li {{{ (Request::is('user/profile') ? 'class=active' : '') }}}>
                        <a class="dropdown-toggle count-info" href="/user/profile">
                            <img src="/assets/img/icon/icons8-user-64.png" alt="" id="user-icon-mini-navbar">
                            <span class="user-mini-navbar">Profile</span>
                        </a>
                    </li>
                    <li {{{ (Request::is('user/activities') ? 'class=active' : '') }}}>
                        <a class="dropdown-toggle count-info" href="/user/activities">
                            <img src="/assets/img/icon/activity.png"  id="user-icon-mini-navbar">
                            <span class="user-mini-navbar">Activities</span>
                        </a>
                    </li>
                    <li {{{ (Request::is('user/friends') ? 'class=active' : '') }}}>
                        <a class="dropdown-toggle count-info" href="/user/friends">
                            <img src="/assets/img/icon/friends.png" alt="" id="user-icon-mini-navbar">
                            <span class="user-mini-navbar">Friends</span>
                        </a>
                    </li>
                    <li {{{ (Request::is('/user/messages/'.Auth::user()->id) ? 'class=active' : '') }}}>
                        <a class="dropdown-toggle count-info" href="/user/messages/{{Auth::user()->id}}">
                            <img href="add-activity" src="/assets/img/icon/emails.png" alt=""
                                 id="user-icon-mini-navbar">
                            <span href="/add-activity" class="user-mini-navbar">Messages</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-bg">
        <div class="page-container">
            @yield('profile-content')
        </div>
    </div>
@stop