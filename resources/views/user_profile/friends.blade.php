@extends('user_profile.profile')
@section('profile-content')
    <div class="row">
        @foreach($friendRequest as $friends)
            <div class="col-lg-4 col-sm-4 col-xs-4">
                <div class="">
                    <a href="{{action('UsersController@profile', $friends['friend_req']->id)}}">
                        <div class="col-sm-3">
                            <div class="text-center">
                                @if($friends['friend_req']->usertype == 'Individual')
                                    <img src="{{asset('/storage/profile/'.$friends['friend_req']->image)}}"
                                         class="img-circle-friends m-t-xs img-responsive">
                                @else
                                    <img src="/assets/img/user.jpg" class="img-circle-friends m-t-xs img-responsive"/>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h3><strong>{{$friends['friend_req']->name}}</strong></h3>
                            <p style="color: #cc5965"> {{$friends['friend_req']->street}}</p>
                        </div>
                        <div class="col-sm-2">
                            <form method="GET" action="{{action('UsersController@acceptFriend', $friends->id)}}">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger fa fa-check"
                                        style=" font-size: 13px; "></button>
                            </form>
                            <hr>
                            <form method="GET" action="{{action('UsersController@ignoreFriend', $friends->id)}}">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger fa fa-close"
                                        style=" font-size: 15px; "></button>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </div>
            </div>
        @endforeach

        @foreach($friendsList as $friends)
            <div class="col-lg-4 col-sm-4 col-xs-4">
                <div class="">
                    <a href="{{action('UsersController@profile', $friends['user']->id)}}">
                        <div class="col-sm-3">
                            <div class="text-center">
                                @if($friends['user']->usertype == 'Individual')
                                    <img src="{{asset('/storage/profile/'.$friends['user']->image)}}"
                                         class="img-circle-friends m-t-xs img-responsive">
                                @else
                                    <img src="/assets/img/user.jpg" class="img-circle-friends m-t-xs img-responsive"/>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h3><strong>{{$friends['user']->name}}</strong></h3>
                            <p style="color: #cc5965"> {{$friends['user']->street}}</p>
                        </div>
                        {{--<div class="col-sm-2">
                            <a class="btn btn-danger fa fa-plus" style=" font-size: 20px; "></a>
                        </div>--}}
                        <div class="clearfix"></div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@stop