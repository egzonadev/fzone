@extends('layout.master')
@section('page-content')
   <div style="margin-top: 5%"></div>
    <div>
        <div class="col-lg-4 col-md-4 col-xs-4">
            <form method="GET" action="{{action('AdvancedSearch@searchAll')}}">
            {{csrf_field()}}
            <div class="ibox float-e-margins" style="background-color: white">
                <div class="ibox-title"
                     style="background-color: rgba(255,47,50,0.69); color: white; border-radius: 5px">
                    <h5>Search</h5>
                </div>
                <div class="form-group" id="toastTypeGroup" style="display: inline-block">
                    <div class="radio col-lg-12 col-md-10 col-xs-10" style="margin-left: 20px">
                        <label>
                            <input class="checkbox-search" type="radio" name="toast" value="success"/>Indoor
                            <input class="checkbox-search " type="radio" name="toast" value="info"
                                   style="margin-left: 25px"/>
                            <label style="margin-left: 20px" for="">Outdoor</label>
                            <input class="checkbox-search" type="radio" name="toast" value="info"
                                   style="margin-left: 25px"/>
                            <label style="margin-left: 20px" for=""> Both</label>
                        </label>
                    </div>
                </div>
                <div class="col-lg-offset-1">
                    <div class="form-group" style="margin-right: 5%">
                    <input type="text" name="title" class="form-control advSearch" placeholder="Title or Name" value="{{Request::input('title')}}">
                    </div>
                    <div class="form-group" style="margin-right: 5%">
                        <input type="date" name="date" class="form-control advSearch" placeholder="Date"  value="{{Request::input('date')}}">
                    </div>
                    <div class="form-group" style="margin-right: 5%">
                        <input type="text" name="address" class="form-control advSearch" placeholder="Enter Address or Plz"  value="{{Request::input('address')}}">
                    </div>
                    <div class="form-group" style="margin-right: 5%">
                        <input type="text" name="radious" class="form-control advSearch" placeholder="Enter Radious in km"  value="{{Request::input('radious')}}">
                    </div>
                </div>

                <div class="form-group" id="event_radio" style="margin-left: 20px">
                    <div class="radio">
                        <label>
                            <input type="radio" name="event_type" id="p1" value="success" />Private Event
                        </label>
                    </div>
                    <div class="radio">
                        <label class="radio">
                            <input type="radio" name="event_type" id="b2" value="info" />Business Event
                        </label>
                    </div>
                    <div class="radio">
                        <label class="radio">
                            <input type="radio" name="event_type" id="b3" value="warning" />Business
                        </label>
                    </div>
                </div>
                <br>

            </div>
            <div class="ibox float-e-margins" style="background-color: white">
                <div class="ibox-title"
                     style="background-color: rgba(255,47,50,0.69); color: white; border-radius: 5px">
                    <h5>Categories</h5>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox-content">
                            <ul class="todo-list m-t">
                                <?php $categories = App\Category::getAllCategories();?>
                                @foreach($categories as $category)
                                <li>
                                    <input type="checkbox" 
                                           name="category_{{$category->id}}" 
                                           class="i-checks" 
                                           {{Request::has('category_' . $category->id) ? 'checked' : '' }} />
                                    <span class="m-l-xs">{{$category->name}}</span>
                                </li>
                                @endforeach 
                            </ul>
                        </div>
                    </div>
                </div>
                <br>
                <div align="center">
                   
                    <button class="btn btn-danger" type="submit"><i class="fa fa-search-plus"></i>&nbsp;
                        SEARCH
                    </button>
                </div>
            </div>
            </form> 
        </div>
        <div class="col-lg-8 col-md-8 col-xs-8">
            <div id="cultSlide" class="carousel carousel-fade" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <div class="item active">
                        @if(count($activities) > 0)
                        @foreach($activities as $activity)
                        <div class="col-lg-4 col-md-6 col-xs-6">
                            <div class="widget-head-color-box navy-bg p-lg text-center"
                            style="background-image: url('{{asset('/images/categories/'. App\ActivityCategories::getCategoryImage( $activity->id )->image )}}')">
                                 <img src="/storage/profile/{{$activity->image}}" class="img-small-profile" alt="profile">
                                 @if( ! App\ActivityInterested::isFollowingActivity($activity->id))
                                 <?php $heart_style = '' ?>
                                 <form method="GET" action="{{action('ActivitiesController@addInterested', $activity->id)}}" class="form-horizontal">
                                 @else 
                                 <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                                 <form method="GET" action="{{action('ActivitiesController@addNotInterested', $activity->id)}}" class="form-horizontal">
                                 @endif
                                     {{csrf_field()}}
                                     <button style="{{$heart_style}}" id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="submit">
                                         <i class="fa fa-heart"></i>
                                     </button>
                                 </form>
                            </div>

                            <div class="widget-text-box">
                                <h3 class="media-heading m-t-md"><b>{{$activity->title}}</b></h3>
                                <h5 class="media-heading m-t-md" style="max-width: 270px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                        {{$activity->description}}
                                </h5>

                                <div class="client-avatar">
                                    <row>
                                        <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                                    style="font-size: 15px"></i>
                                        </small>
                                        <span class="m-l-xs">{{ date("d/m/Y", strtotime( $activity->start_date_time ) ) }}  </span>
                                        <?php 
                                        $attenders = App\Activity::GetAllActivityAttenders($activity->id);
                                        $count_attenders = App\Activity::GetAttendersCount($activity->id);
                                        ?>
                                        @foreach($attenders as $attender)
                                        <img alt="image" src="/storage/profile/{{$attender->image}}">
                                        @endforeach 
                                        <span class="m-l-xs"> +{{$count_attenders}} attenders</span>
                                    </row>
                                </div>
                                <br>
                                <div id="did-divider" class="divider"></div>
                                <div class="text-right" style="  padding-top: 10px;">
                                    <small id="locaion-in-cards" class="icon" style="color: #cc5965">
                                        <i class="fa fa-map-marker">
                                    {{$activity->location . ', ' . $activity->city }}</i></small>
                                    @if( ! App\ActivityAttender::isAttendingActivity($activity->id))
                                    <?php $attending = 'Attend' ?>
                                    <form method="GET" action="{{action('ActivitiesController@Attend', $activity->id)}}" class="form-horizontal">
                                    @else 
                                    <?php $attending = 'Attending' ?>
                                    <form method="GET" action="{{action('ActivitiesController@RemoveAttend', $activity->id)}}" class="form-horizontal">
                                    @endif
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-danger btn-xs"><strong>{{$attending}}</strong>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach 
                        @else
                        <div class="col-lg-4 col-md-6 col-xs-6">
                            <h3>Keine Veranstaltung wurde gefunden!</h3>
                        </div>
                        @endif
                        
                    </div>

                </div>
            </div>
            <div class="row text-center">
                    {{$activities->links()}}
            </div>
        </div>
    </div>
    <br>
@stop
