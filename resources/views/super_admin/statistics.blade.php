@extends('super_admin.master')
@section('content')

    <div class="row border-bottom gray-bg page-heading-business">
        <div class="col-lg-10">
            <h2>Statistics</h2>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/admin">Dashboard</a>
                </li>
            </ol>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Analysis</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="doughnutChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Statistics</h5>
                    </div>
                    <div class="ibox-content" style="padding-left: 5%">
                        <div>
                            <canvas id="lineChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Multiple Statistics</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="mstat" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Realtime Statistics</h5>
                    </div>
                    <div class="ibox-content" style="padding-left: 5%">
                        <div>
                            <canvas id="previous" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Vertical Bar Chart</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="vbarChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Hierarchical Bar Chart</h5>
                    </div>
                    <div class="ibox-content" style="padding-left: 5%">
                        <div>
                            <canvas id="hbarChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   {{-- <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Analysis</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="doughnutChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Statistics</h5>
                    </div>
                    <div class="ibox-content" style="padding-left: 5%">
                        <div>
                            <canvas id="lineChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

    <script>
        var url2 = "{{url('/dash-donought1')}}";
        var type = new Array();
        var count = new Array();
        $(document).ready(function(){
            $.get(url2, function(response){
                response.forEach(function(data){
                    type.push(data.usertype);
                    count.push(data.count);
                });
                var q_pie = document.getElementById("doughnutChart").getContext('2d');
                var qpie = new Chart(q_pie, {
                    type: 'doughnut',
                    data: {
                        labels:type,
                        datasets: [{
                            backgroundColor: ["#a3e1d4","#dedede","#b5b8cf","#0685C8"],
                            label: 'Users',
                            data: count,
                            fill: false,
                            borderWidth: 5,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {position: 'right'},
                        pieceLabel: {
                            mode: 'value',
                            render: 'percentage',
                            fontSize: 14,
                            fontStyle: 'bold',
                            fontColor: '#000',
                        }
                    }
                });
            });
        });
    </script>
    <script>
        var url_dash2 = "{{url('/dash-donought2')}}";
        var c = new Array();
        var m = new Array();
        $(document).ready(function(){
            $.get(url_dash2, function(response) {
                response.forEach(function (data) {
                    c.push(data.count);
                    m.push(data.month);
                });
                var c_bar = document.getElementById("lineChart").getContext('2d');
                var cbar = new Chart(c_bar, {
                    type: 'line',
                    data: {
                        labels: m,
                        datasets: [{
                            backgroundColor: 'rgba(26,179,148,0.5)',
                            borderColor: "rgba(26,179,148,0.7)",
                            pointBackgroundColor: "rgba(26,179,148,1)",
                            data: c,
                            fill: true,
                            borderWidth: 5,
                        }]
                    },
                    options: {
                        legend: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) { if (Number.isInteger(value)) { return value; } },
                                    stepSize: 1
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>

    {{--bar charts--}}
    <script>
        var vbar = "{{url('/dash-donought1')}}";
        var bartype = new Array();
        var barcount = new Array();
        $(document).ready(function(){
            $.get(vbar, function(response){
                response.forEach(function(data){
                    bartype.push(data.usertype);
                    barcount.push(data.count);
                });
                var bar1 = document.getElementById("vbarChart").getContext('2d');
                var bar11 = new Chart(bar1, {
                    type: 'bar',
                    data: {
                        labels:bartype,
                        datasets: [{
                            backgroundColor: ["#a3e1d4","#dedede","#b5b8cf","#0685C8"],
                            label: 'Users',
                            data: barcount,
                            fill: false,
                            borderWidth: 5,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {position: 'top'},
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) { if (Number.isInteger(value)) { return value; } },
                                    stepSize: 1
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>
    <script>
        var hbar = "{{url('/dash-donought2')}}";
        var barc = new Array();
        var barm = new Array();
        $(document).ready(function(){
            $.get(hbar, function(response) {
                response.forEach(function (data) {
                    barc.push(data.count);
                    barm.push(data.month);
                });
                var hbar = document.getElementById("hbarChart").getContext('2d');
                var hbar1 = new Chart(hbar, {
                    type: 'horizontalBar',
                    data: {
                        labels: barm,
                        datasets: [{
                            backgroundColor: 'rgba(26,179,148,0.5)',
                            data: barc,
                            label: 'Businesses',
                        }]
                    },
                    options: {
                        legend: {position: 'top'},
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) { if (Number.isInteger(value)) { return value; } },
                                    stepSize: 1
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>

    {{--Statistics 2 charts--}}
    <script>
        var multi1 = "{{url('/multiStat')}}";
        var multic = new Array();
        var multim = new Array();
        $(document).ready(function(){
            $.get(multi1, function(response){
                response.forEach(function(data){
                    multim.push(data.month);
                    multic.push(data.count);
                });
                var multi1 = document.getElementById("mstat").getContext('2d');
                var multi1q = new Chart(multi1, {
                    type: 'line',
                    data: {
                        labels:multim,
                        datasets: [{
                            backgroundColor: 'rgba(26,179,148,0.5)',
                            borderColor: "rgba(26,179,148,0.7)",
                            pointBackgroundColor: "rgba(26,179,148,1)",
                            data: multic,
                            fill: true,
                            label: 'Upcoming Events',
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {position: 'top'},
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) { if (Number.isInteger(value)) { return value; } },
                                    stepSize: 1
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>
    <script>
        var previous_url = "{{url('/previous')}}";
        var previous_c = new Array();
        var previous_m = new Array();
        $(document).ready(function(){
            $.get(previous_url, function(response){
                response.forEach(function(data){
                    previous_m.push(data.month);
                    previous_c.push(data.count);
                });
                var previous = document.getElementById("previous").getContext('2d');
                var previous1 = new Chart(previous, {
                    type: 'line',
                    data: {
                        labels:previous_m,
                        datasets: [{
                            backgroundColor: 'rgba(26,179,148,0.5)',
                            borderColor: "rgba(26,179,148,0.7)",
                            pointBackgroundColor: "rgba(26,179,148,1)",
                            data: previous_c,
                            fill: true,
                            label: 'Previous Events',
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {position: 'top'},
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) { if (Number.isInteger(value)) { return value; } },
                                    stepSize: 1
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>
@stop