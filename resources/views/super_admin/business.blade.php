@extends('super_admin.master')
@section('content')
    @include('includes.messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Business List</h5>
                    <div class="ibox-tools">
                        <a>
                            <i class="fa fa-gear"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-title">
                    <button type="button" class="btn btn-outline btn-success">Add</button>
                    <button type="button" class="btn btn-outline btn-success">Edit</button>
                    <button type="button" class="btn btn-outline btn-success">Delete</button>
                </div>
                <div style="padding: 20px" class="ibox-content">
                    <div class="table-responsive">
                        <table class="table dataTables-example">
                            <thead>
                            <tr style="background-color: #e0e0e0">
                                <th>FIRMA</th>
                                <th>MEMBERSHIP</th>
                                <th>PLZ / ORT</th>
                                <th>TELEFON</th>
                                <th>TYP</th>
                                <th>ADDRESS</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($premium_users as $premium)
                                <tr>
                                    <td>
                                        <input style="width: 18px; height: 18px" type="checkbox"
                                               class="i-checks" name="input[]">
                                        <a>{{$premium->name}}</a>
                                    </td>
                                    <td project-status>
                                        <a class="sa-table">{{$premium->usertype}}</a>
                                    </td>
                                    <td class="project-title">
                                        <a class="sa-table">{{$premium->pcode}}</a>
                                    </td>
                                    <td class="project-completion">
                                        <a class="sa-table">{{$premium->phone}}</a>
                                    </td>
                                    <td class="project-completion">
                                        <a class="sa-table">Indor</a>
                                    </td>
                                    <td class="project-actions">
                                        <small class="label label-danger pull-left">{{$premium->street}}</small>
                                    </td>
                                    <td class="project-actions">
                                        <a class="btn btn-white btn-sm pull-left" data-toggle="modal"
                                           data-target="#modal-upgrade-{{$premium['id']}}" title="Upgrade"><i
                                                    class="fa fa-edit" style="font-size: 20px"></i></a>
                                        <a href="#" class="btn btn-white btn-sm pull-left demo3"><i
                                                    class="fa fa-trash" style="font-size: 20px"></i></a>
                                    </td>

                                    <div class="modal fade" id="modal-upgrade-{{$premium['id']}}">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header  text-center">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">&times;
                                                    </button>
                                                    <div class="icon-box" style="font-size: 80px">
                                                        <i class="text-red fa fa-arrow-circle-o-up"></i>
                                                    </div>
                                                    <h4 class="modal-title"><b>Upgrade Business to Premium</b></h4>
                                                </div>
                                                <div class="modal-body col-lg-offset-1">
                                                    <h2>Firm: {{$premium['name']}}<strong></strong></h2>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left"
                                                            data-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <form action="{{action('AdminController@upgrade', $premium['id'])}}"
                                                          method="post">
                                                        {{csrf_field()}}
                                                        <input name="_method" type="hidden" value="PUT">
                                                        <button class="btn btn-info" type="submit">UPGRADE</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/assets/js/datatables.min.js"></script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>
    <!-- Sweet alert -->
    <script src="assets/js/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.demo1').click(function () {
                swal({
                    title: "Welcome in Alerts",
                    text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                });
            });
            $('.demo2').click(function () {
                swal({
                    title: "Good job!",
                    text: "You clicked the button!",
                    type: "success"
                });
            });

            $('.demo3').click(function () {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this user anymore",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#fb2d30",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    swal("Deleted!", "This user has been deleted.", "success");
                });
            });

            $('.demo4').click(function () {
                swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#fb2d30",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
            });


        });

    </script>
@stop
