@extends('super_admin.master')
@section('content')

    <div class="row border-bottom gray-bg page-heading-business">
        <div class="col-lg-10">
            <h2>Dashboard</h2>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/admin">Dashboard & Statistics</a>
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
        <?php
        $newBusiness = \App\Business::where('created_at', '>=', \Carbon\Carbon::now()->subDays(7))->count();
        $newUser = \App\User::where('created_at', '>=', \Carbon\Carbon::now()->subDays(7))->count();
        $upcoming = \App\Activity::where('start_date_time', '>=', \Carbon\Carbon::now())->count();
        $premium = \App\User::where('usertype', '=', 'Premium')->count();
        ?>
        <div class="col-md-12">
            <div class="col-md-3 col-xs-12">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-xs-6 text-right">
                            <h2 align="left" class="font-bold">{{$newBusiness}}</h2>
                            <small style="font-size: 12px" align="left"><b>BUSINESSES</b></small>
                        </div>
                        <div class="col-md-6" align="right">
                            <i class="fa fa-university fa-4x"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="widget style1 navy-bg-nu">
                    <div class="row">
                        <div class="col-xs-6 ">
                            <h2 align="left" class="font-bold">{{$newUser}}</h2>
                            <small style="font-size: 12px;" align="left"><b>NEW USERS</b></small>
                        </div>
                        <div class="col-md-6" align="right">
                            <i class="fa fa-user-o fa-4x"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="widget style1 navy-bg-nub">
                    <div class="row">
                        <div class="col-xs-6">
                            <h2 align="left" class="font-bold">{{$upcoming}}</h2>
                            <small style="font-size: 12px" align="left"><b>UPCOMING</b></small>
                        </div>
                        <div class="col-md-6" align="right">
                            <i class="fa fa-calendar fa-4x"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="widget style1 navy-bg-nupre">
                    <div class="row">
                        <div class="col-xs-6">
                            <h2 align="left" class="font-bold">{{$premium}}</h2>
                            <small style="font-size: 12px" align="left"><b>PREMIUM</b></small>
                        </div>
                        <div class="col-md-6" align="right">
                            <i class="fa fa-diamond fa-4x"></i>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="col-md-3">
                <div class="widget style1 navy-bg-monthly">
                    <div class="row">
                        <div class="col-xs-6">
                            <h2 align="left" class="font-bold">100</h2>
                            <small style="font-size: 12px" align="left"><b>PROFITS</b></small>
                        </div>
                        <div class="col-md-6" align="right">
                            <i class="fa fa-credit-card-alt fa-4x"></i>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Analysis</h5>
                        <h5 class="pull-right">
                            <a class="weekmonthyear">Last 24 hours >></a>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="doughnutChart" height="300"></canvas>
                        </div>
                    </div>
                </div>
            </div>
           <div class="col-md-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Statistics</h5>
                    </div>
                    <div class="ibox-content" style="padding-left: 5%">
                        <div>
                            <canvas id="lineChart" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="ibox">
                <div class="ibox-title">
                    <h3>Top Users</h3>
                </div>
                <div class="ibox-content" style="padding-left: 5%">
                    <div class="activity-stream">
                        <?php
                        $privates = \App\User::where('usertype', '=', 'Individual')->latest()->take(4)->get();
                        ?>
                        @foreach($privates as $private)
                        <div class="chat-element ">
                            <a href="#" class="pull-left">
                                <img alt="image" class="img-circle-superadmin" src="{{asset('/storage/profile/'.$private->image)}}"/>
                            </a>
                            <div class="media-body ">
                                <strong>{{$private->name}}</strong>
                                <p class="m-b-xs">
                                    {{$private->street}}
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox ">
                <?php
                $avg_top_businesses = \App\BusinessReviews::getTopBusinesses();
                $selected_businesses = array();
                $count = 0;
                foreach($avg_top_businesses as $business){
                    $selected_businesses[ $count ] = $business->business_id;
                    $count++;
                }

                $top_businesses = \App\Business::whereIn('id', $selected_businesses)->take(4)->get();
                ?>
                <div class="ibox-title">
                    <h3>Top Businesses</h3>
                </div>
                <div class="ibox-content" style="padding-left: 5%">
                    <div class="activity-stream">
                        @foreach($top_businesses as $business)
                        <div class="chat-element ">
                            <a href="#" class="pull-left">
                                <img alt="image" class="img-circle-top-business"
                                     src="/storage/business/logo/{{$business->path_logo_image}}"/>
                            </a>
                            <div class="media-body ">
                                <strong>{{$business->name}}</strong>
                                <p class="m-b-xs">
                                    {{$business->type}}
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox ">
                <?php
                $activities = \App\Activity::where('created_at', '<=', \Carbon\Carbon::now())->take(4)->get();
                ?>
                <div class="ibox-title">
                    <h3>Top Activities</h3>
                </div>
                <div class="ibox-content" style="padding-left: 5%">
                    <div class="activity-stream">
                        @foreach($activities as $activity)
                        <div class="chat-element ">
                            <a href="#" class="pull-left">
                                <img alt="image" class="img-circle-superadmin" src="{{asset('/storage/profile/'.$activity['GetUser']->image)}}"/>
                            </a>
                            <div class="media-body ">
                                <strong>{{$activity['GetUser']->name}}</strong>
                                <p class="m-b-xs">
                                    {{$activity->title}}
                                </p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var url2 = "{{url('/dash-donought1')}}";
        var type = new Array();
        var count = new Array();
        $(document).ready(function(){
            $.get(url2, function(response){
                response.forEach(function(data){
                    type.push(data.usertype);
                    count.push(data.count);
                });
                var q_pie = document.getElementById("doughnutChart").getContext('2d');
                var qpie = new Chart(q_pie, {
                    type: 'doughnut',
                    data: {
                        labels:type,
                        datasets: [{
                            backgroundColor: ["#a3e1d4","#dedede","#b5b8cf","#0685C8"],
                            label: 'Users',
                            data: count,
                            fill: false,
                            borderWidth: 5,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {position: 'bottom'},
                        pieceLabel: {
                            mode: 'value',
                            render: 'percentage',
                            fontSize: 14,
                            fontStyle: 'bold',
                            fontColor: '#000',
                        }
                    }
                });
            });
        });
    </script>
    <script>
        var url_dash2 = "{{url('/dash-donought2')}}";
        var c = new Array();
        var m = new Array();
        $(document).ready(function(){
            $.get(url_dash2, function(response) {
                response.forEach(function (data) {
                    c.push(data.count);
                    m.push(data.month);
                });
                var c_bar = document.getElementById("lineChart").getContext('2d');
                var cbar = new Chart(c_bar, {
                    type: 'line',
                    data: {
                        labels: m,
                        datasets: [{
                            backgroundColor: 'rgba(26,179,148,0.5)',
                            borderColor: "rgba(26,179,148,0.7)",
                            pointBackgroundColor: "rgba(26,179,148,1)",
                            data: c,
                            fill: true,
                            borderWidth: 5,
                        }]
                    },
                    options: {
                        legend: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            });
        });
    </script>

@stop