@extends('super_admin.master')
@section('content')
    @include('includes.messages')
    <link href="/assets/css/basic.css" rel="stylesheet">
    <link href="/assets/css/dropzone.css" rel="stylesheet">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row" style="background-color: white">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h3>User Profile Details</h3>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <form action="{{action('AdminController@update')}}"
                          method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT" />
                        <div style="padding: 20px" class="ibox-content">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{Auth::user()->username}}" name="username" class="form-control">
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{Auth::user()->name}}" name="name" class="form-control">
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" value="{{Auth::user()->email}}" name="email" class="form-control">
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{Auth::user()->phone}}" name="phone" class="form-control">
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" name="pass" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div >
                                    <img alt="image" class="img-circle-sa"
                                         src="{{asset('/storage/profile/'.Auth::user()->image)}}">
                                    <br>
                                    <div style="padding-top: 5%"></div>
                                    <input  type="file" name="image" class="form-control col-lg-2"/>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-lg-8 col-lg-offset-2">
                                <br><br><button type="submit" class="btn btn-lg btn-primary col-lg-4">
                                    <i class="fa fa-edit"></i> UPDATE </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        Dropzone.options.dropzoneForm = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            dictDefaultMessage: "<strong>Drop files here or click to upload. </strong></br>"
        };

        $(document).ready(function () {

            var editor_one = CodeMirror.fromTextArea(document.getElementById("code1"), {
                lineNumbers: true,
                matchBrackets: true
            });

            var editor_two = CodeMirror.fromTextArea(document.getElementById("code2"), {
                lineNumbers: true,
                matchBrackets: true
            });

            var editor_two = CodeMirror.fromTextArea(document.getElementById("code3"), {
                lineNumbers: true,
                matchBrackets: true
            });

        });
    </script>
    <!-- DROPZONE -->
    <script src="/assets/js/dropzone.js"></script>
@stop
