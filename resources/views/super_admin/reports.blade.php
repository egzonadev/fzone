@extends('super_admin.master')
@section('content')
    @include('includes.messages')
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/datatables.min.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="assets/css/sweetalert.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <div class="ibox">
                        <div class="ibox-title">
                            <h5>Reports</h5>
                            <div class="ibox-tools">
                                <a>
                                    <i class="fa fa-gear"></i>
                                </a>
                            </div>
                        </div>
                        <div style="padding: 20px" class="ibox-content">
                            <div class="table-responsive">
                                <table class="table dataTables-example">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>NAME</th>
                                        <th>REPORTED</th>
                                        <th>DATE</th>
                                        <th>REASON</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($reports as $report)
                                        <tr>
                                            <td>
                                                <input style="width: 18px; height: 18px" type="checkbox"
                                                       class="i-checks" name="input[]">
                                            </td>
                                            <td project-status>
                                                <a href="#">
                                                    <img alt="image" class="img-circle-superadmin"
                                                         src="{{asset('/storage/profile/'.$report->userReporting->image)}}">
                                                    <small class="sa-table">{{$report->userReporting->name}}</small>
                                                </a>
                                            </td>
                                            <td class="project-title">
                                                <a class="sa-table">Reported, {{$report->business->name}}</a>
                                            </td>
                                            <td class="project-completion">
                                                <a class="sa-table">{{$report->created_at}}</a>
                                            </td>
                                            <td class="project-actions">
                                                <small class="label label-danger pull-left">{{$report->status}}</small>
                                            </td>
                                            <td class="project-actions">
                                                <a href="#" class="btn btn-white btn-sm pull-left"><i
                                                            class="fa fa-edit" style="font-size: 20px"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit"></i></a>
                                                <a href="#" class="btn btn-white btn-sm pull-left"><i
                                                            class="fa fa-trash" style="font-size: 20px"
                                                            data-toggle="modal"
                                                            data-target="#modal-delete-business-{{$report['id']}}"></i></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="modal-delete-business-{{$report['id']}}">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header  text-center">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;
                                                        </button>
                                                        <div class="icon-box" style="font-size: 80px">
                                                            <i class="text-red fa fa-close"></i>
                                                        </div>
                                                        <h4 class="modal-title">
                                                            <b>Are you sure?</b>
                                                        </h4>
                                                        <p>The deleted report can not be undone!</p>
                                                    </div>

                                                    <form action="{{action('AdminController@destroyBusiness', $report->id)}}"
                                                          method="post">
                                                        {{csrf_field()}}
                                                        <input name="_method" type="hidden" value="delete">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left"
                                                                    data-dismiss="modal">
                                                                Cancel
                                                            </button>
                                                            <button type="submit" class="btn btn-danger"
                                                                    style="height: 35px"
                                                                    title="Report">
                                                                <i class="fa fa-trash"> Delete</i>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @foreach($eventReports as $eventReport)
                                        <tr>
                                            <td>
                                                <input style="width: 18px; height: 18px" type="checkbox"
                                                       class="i-checks" name="input[]">
                                            </td>
                                            <td project-status>
                                                <a href="#">
                                                    <img alt="image" class="img-circle-superadmin"
                                                         src="{{asset('/storage/profile/'.$eventReport->userReporting->image)}}">
                                                    <small class="sa-table">{{$eventReport->userReporting->name}}</small>
                                                </a>
                                            </td>
                                            <td class="project-title">
                                                <a class="sa-table">Reported, {{$eventReport->event->title}}</a>
                                            </td>
                                            <td class="project-completion">
                                                <a class="sa-table">{{$eventReport->created_at}}</a>
                                            </td>
                                            <td class="project-actions">
                                                <small class="label label-info pull-left">{{$eventReport->status}}</small>
                                            </td>
                                            <td class="project-actions">
                                                <a href="#" class="btn btn-white btn-sm pull-left"><i
                                                            class="fa fa-edit" style="font-size: 20px"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit"></i></a>
                                                <a href="#" class="btn btn-white btn-sm pull-left"><i
                                                            class="fa fa-trash" style="font-size: 20px"
                                                            data-toggle="modal"
                                                            data-target="#modal-delete-{{$eventReport['id']}}"></i></a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="modal-delete-{{$eventReport['id']}}">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header  text-center">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">&times;
                                                        </button>
                                                        <div class="icon-box" style="font-size: 80px">
                                                            <i class="text-red fa fa-close"></i>
                                                        </div>
                                                        <h4 class="modal-title">
                                                            <b>Are you sure?</b>
                                                        </h4>
                                                        <p>The deleted report can not be undone!</p>
                                                    </div>

                                                    <form action="{{action('AdminController@destroy', $eventReport->id)}}"
                                                          method="post">
                                                        {{csrf_field()}}
                                                        <input name="_method" type="hidden" value="delete">
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default pull-left"
                                                                    data-dismiss="modal">
                                                                Cancel
                                                            </button>
                                                            <button type="submit" class="btn btn-danger"
                                                                    style="height: 35px"
                                                                    title="Report">
                                                                <i class="fa fa-trash"> Delete</i>
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <div class="modal fade" id="modal-edit">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header  text-center">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">&times;
                                                    </button>
                                                    <div class="icon-box" style="font-size: 80px; color: red;">
                                                        <i class="text-red fa fa-warning"></i>
                                                    </div>
                                                    <h3>You can not edit reports.</h3>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="/assets/js/datatables.min.js"></script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>
    <!-- Sweet alert -->
    <script src="assets/js/sweetalert.min.js"></script>
    <script>

        $(document).ready(function () {

            $('.demo1').click(function () {
                swal({
                    title: "Welcome in Alerts",
                    text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                });
            });

            $('.demo2').click(function () {
                swal({
                    title: "Good job!",
                    text: "You clicked the button!",
                    type: "success"
                });
            });

            $('.demo3').click(function () {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this user anymore",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#fb2d30",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    swal("Deleted!", "This user has been deleted.", "success");
                });
            });

            $('.demo4').click(function () {
                swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#fb2d30",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
            });


        });

    </script>
@stop
