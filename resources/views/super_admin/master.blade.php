<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/assets/css/chartist.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/datatables.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <!-- Custom and plugin javascript -->
    <script src="/assets/js/inspinia.js"></script>
    <script src="/assets/js/pace.min.js"></script>
    <!-- ChartJS-->
    <script src="/assets/js/demo/Chart.min.js"></script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>
    <!-- Sweet Alert -->
    <link href="/assets/css/sweetalert.css" rel="stylesheet">
    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">

    <title>FFZ | SUPER ADMIN</title>
@include('includes.head')
<body>
<div id="wrapper" style="background-color: white">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header-sa">
                    <div class="dropdown profile-element">
                        <span>
                            <img alt="image" class="img-squaree" src="/assets/img/logo.PNG"/>
                        </span>
                    </div>
                </li>
                <li>
                    <a>
                        <span class="nav-label">Navigation </span>
                    </a>
                </li>
                <li {{{ (Request::is('admin') ? 'class=active' : '') }}}>
                    <a href="/admin"><i class="fa fa-dashboard"></i>
                        <?php $business_count = \App\Business::all()->count(); ?>
                        <span class="nav-label ">Dashboard </span>
                        <span class="label label-danger pull-right">{{$business_count}}</span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/users') ? 'class=active' : '') }}}>
                    <a href="/adminn/users"><i class="fa fa-users"></i>
                        <span class="nav-label">Users </span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/businesses') ? 'class=active' : '') }}}>
                    <a href="/adminn/businesses"><i class="fa fa-calendar"></i>
                        <span class="nav-label">Businesses </span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/events') ? 'class=active' : '') }}}>
                    <?php $events = \App\Activity::all()
                        ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
                        ->count(); ?>
                    <a href="/adminn/events"><i class="fa fa-file"></i>
                        <span class="nav-label">Events </span>
                        <span class="label label-danger pull-right">{{$events}}</span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/reports') ? 'class=active' : '') }}}>
                    <a href="/adminn/reports"><i class="fa fa-windows"></i>
                        <span class="nav-label">Reports </span>
                    </a>
                </li>
                <hr>
                <li>
                    <a>
                        <span class="nav-label">Components </span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/statistics') ? 'class=active' : '') }}}>
                    <a href="/adminn/statistics"><i class="fa fa-pie-chart"></i>
                        <span class="nav-label">Statistics </span>
                    </a>
                </li>
                <li {{{ (Request::is('admin/banners') ? 'class=active' : '') }}}>
                    <a href="#"><i class="fa fa-briefcase"></i>
                        <span class="nav-label">Banners </span>
                        <span class="label label-danger pull-right">5</span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/settings') ? 'class=active' : '') }}}>
                    <a href="#"><i class="fa fa-gear"></i>
                        <span class="nav-label">Settings </span>
                    </a>
                </li>
                <li {{{ (Request::is('adminn/profile') ? 'class=active' : '') }}}>
                    <a href="/adminn/profile"><i class="fa fa-user"></i>
                        <span class="nav-label">Profile </span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: white">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary-meli " href="#"><i
                                class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="http://www.freizeitzoone.ch/">
                        <div class="form-group">
                            <input type="text" placeholder="Search..." class="form-control " name="top-search"
                                   id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-calendar" style="font-size: large"></i>
                            <?php $user_request_count = \App\User::all()->where('has_requested_upgrade','true')->count();?>
                            <span class="label label-danger">{{$user_request_count}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box">
                                    <div class="media-body">
                                        <h4 align="center"><b>{{$user_request_count}}</b> Request For Upgrade</h4>
                                    </div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="#">
                                        <i class="fa fa-diamond"></i> <strong>Premium Request</strong>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell" style="font-size: large"></i> <span
                                    class="label label-danger">{{$events}}</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="/adminn/events">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> {{$events}} New Events
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="#">
                                        <strong>Events Notification</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <small>{{Auth::user()->name}}</small>
                            <img id="user-login-icon" src="{{asset('/storage/profile/'.Auth::user()->image)}}" alt="">
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <div class="" align="center">
                                <a href="profile.html">
                                    <img alt="image" class="img-circle-sa" src="{{asset('/storage/profile/'.Auth::user()->image)}}">
                                    <h3 class="m-b-xs"><strong>{{Auth::user()->name}}</strong></h3>
                                    <address class="m-t-md">
                                        <h5>Super Admin</h5>
                                    </address>
                                </a>
                                <div class="contact-box-footer">
                                    <button class="btn btn-w-m btn-success" >
                                        <a href="/adminn/profile" style="color: white">EDIT PROFILE</a></button>
                                    <button href="{{ route('logout') }}" class="btn btn-w-m btn-danger"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        LOGOUT
                                    </button>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        </ul>
                    </li>
                </ul>

            </nav>
        </div>
        {{--Here are called the Files/Pages--}}
        <div class="wrapper wrapper-content">
            @yield('content')
            <div class="footer">
                <div class="pull-right">
                    Version: <strong>2.0</strong>.
                </div>
                <div>
                    <strong>Copyright</strong> Freitzeitzone &copy; 2018
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
