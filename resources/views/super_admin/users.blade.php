@extends('super_admin.master')
@section('content')
    @include('includes.messages')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                    <div class="ibox-title">
                        <h5>User List</h5>
                        <div class="ibox-tools">
                            <a>
                                <i class="fa fa-gear"></i>
                            </a>
                        </div>
                    </div>
                    <div style="padding: 20px" class="ibox-content">

                        <div class="table-responsive">
                            <table class="table dataTables-example">
                                <thead>
                                <tr>
                                    <th><input style="width: 18px; height: 18px" type="checkbox" name="input[]"></th>
                                    <th>NAME</th>
                                    <th>ADDRESS</th>
                                    <th>DATE JOINED</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($private_users as $private)
                                <tr>
                                    <td>
                                        <input style="width: 18px; height: 18px" type="checkbox" class="i-checks" name="input[]">
                                    </td>
                                    <td project-status>
                                        <a href="#">
                                            <img alt="image" class="img-circle-superadmin" src="{{asset('/storage/profile/'.$private->image)}}">
                                            <small class="sa-table">{{$private->name}}</small>
                                        </a>
                                    </td>
                                    <td class="project-title">
                                        <a class="sa-table">{{$private->street}}</a>
                                    </td>
                                    <td class="project-completion">
                                        <a class="sa-table">{{Carbon\Carbon::parse($private['created_at'])->format('d/m/Y')}}</a>
                                    </td>
                                    <td class="project-actions">
                                        <a href="#" class="btn btn-white btn-sm pull-left"><i class="fa fa-edit"
                                                                                              style="font-size: 20px"></i></a>
                                        <a href="#" class="btn btn-white btn-sm pull-left demo3"><i class="fa fa-trash"
                                                                                                    style="font-size: 20px"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Mainly scripts -->
    <script src="/assets/js/datatables.min.js"></script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]

            });

        });

    </script>
    <!-- Sweet alert -->
    <script src="assets/js/sweetalert.min.js"></script>
    <script>

        $(document).ready(function () {

            $('.demo1').click(function () {
                swal({
                    title: "Welcome in Alerts",
                    text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                });
            });

            $('.demo2').click(function () {
                swal({
                    title: "Good job!",
                    text: "You clicked the button!",
                    type: "success"
                });
            });

            $('.demo3').click(function () {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this user anymore",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#fb2d30",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    swal("Deleted!", "This user has been deleted.", "success");
                });
            });

            $('.demo4').click(function () {
                swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#fb2d30",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel plx!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                        }
                    });
            });


        });

    </script>
@stop
