<style>
    .slidecontainer {
        width: 100%;
    }

    .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 15px;
        border-radius: 5px;
        background: #d3d3d3;
        outline: none;
        opacity: 0.7;
        -webkit-transition: .2s;
        transition: opacity .2s;
    }

    .slider:hover {
        opacity: 1;
    }

    .slider::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: #ff0000;
        cursor: pointer;
    }

    .slider::-moz-range-thumb {
        width: 25px;
        height: 25px;
        border-radius: 50%;
        background: #4CAF50;
        cursor: pointer;
    }
</style>
@include('includes.sweetalert')
<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                class="navbar-toggle collapsed" type="button">
            <i class="fa fa-reorder"></i>
        </button>
        <a href="/" style="padding-top: 10px" class="navbar-brand"><img src="/assets/img/logo.PNG" alt=""></a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <ul id="search-navbar" class="nav navbar-nav col-lg-3 col-xs-7">
            <form method="GET" action="{{action('AdvancedSearch@searchTitle')}}">
                {{csrf_field()}}
                <div class="input-group m-b">
                    <input type="text" name="title" class="form-control" placeholder="Search what you want..">
                    <span class="input-group-btn">
                         <button type="submit" class="btn btn-danger">
                             <i class="fa fa-search" style="height: 20px"></i>
                         </button>
                </span>
                </div>
            </form>
        </ul>
        <ul class="nav navbar-top-links navbar-right">
            @guest
                {{--Dropdown search--}}
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">SEARCH</a>
                    <ul class="dropdown-menu dropdown-messages ">
                        <form method="GET" action="{{action('AdvancedSearch@searchAll')}}">
                            {{csrf_field()}}
                            <div class="ibox float-e-margins" style="background-color: white">
                                <div class="form-group" id="toastTypeGroup" style="display: inline-block">
                                    <div class="radio col-lg-12 col-md-10 col-xs-10" style="margin-left: 20px">
                                        <label>
                                            <input class="checkbox-search" type="radio" name="toast" value="success"
                                                   checked/>Both
                                            <input class="checkbox-search " type="radio" name="toast" value="info"
                                                   style="margin-left: 25px"/>
                                            <label style="margin-left: 20px" for="">Indoor</label>
                                            <input class="checkbox-search" type="radio" name="toast" value="info"
                                                   style="margin-left: 25px"/>
                                            <label style="margin-left: 20px" for="">Outdoor</label>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-offset-1">
                                    <div class="form-group" style="margin-right: 5%">
                                        <select class="form-control advSearch" name="category">
                                            <option value="all">All</option>
                                            <option value="1">Essen & Trinken</option>
                                            <option value="2">Familie & Kinder</option>
                                            <option value="3">Firmenanlasse & Ausfluge</option>
                                            <option value="4">Fun and Adventure</option>
                                            <option value="5">Kultur</option>
                                            <option value="6">Romantik</option>
                                            <option value="7">Sport</option>
                                            <option value="8">Wellness & Beauty</option>
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 5%">
                                        <input type="date" name="date" class="form-control advSearch" placeholder="Date"
                                               value="{{Request::input('date')}}">
                                    </div>
                                    <div class="form-group" style="margin-right: 5%">
                                        <input type="text" name="address" class="form-control advSearch"
                                               placeholder="Enter Address or Plz" value="{{Request::input('address')}}">
                                    </div>
                                </div>
                                <div class="form-group" id="event_radio" style="display: inline-block">
                                    <div class="radio col-lg-12 col-md-10 col-xs-10">
                                        <label>
                                            <input type="radio" name="eventuser" value="private"/>
                                            Private Event
                                            <i style="margin-left: 25px"></i>
                                            <input type="radio" name="eventuser" value="business"/>
                                            Business Event
                                            <i style="margin-left: 25px"></i>
                                            <input type="radio" name="eventuser" value="business"/>
                                            Business
                                        </label>
                                    </div>
                                </div>

                                <div class="slidecontainer" align="center">
                                    <input type="range" min="1" max="400" value="50" class="slider" id="myRange">
                                    <h2 style="box-shadow:  0 0 3px 0 #919191; width: 135px;"><span id="demo"></span> km
                                        area</h2>
                                </div>
                                <br>
                            </div>
                            <div align="center">
                                <button class="btn btn-danger" type="submit">SEARCH</button>
                            </div>
                        </form>
                    </ul>

                    <script>
                        $('.dropdown-menu').click(function (e) {
                            e.stopPropagation();
                        });
                    </script>
                </li>
                <li {{{ (Request::is('home') ? 'class=active' : '') }}}><a href="/">HOME</a></li>
                <li {{{ (Request::is('contact') ? 'class=active' : '') }}}><a href="/contact">CONTACT US</a></li>
                <li><a href="/login">LOGIN</a></li> /
                <li><a href="/signup">SIGN UP</a></li>
                {{--<li class="dropdown" href="#">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Sign
                        Up</a>
                    <ul role="menu" class="dropdown-menu btn-danger text-center">
                        <li><a href="{{action('Auth\RegisterController@showRegistrationForm', 'Individual')}}"> As a
                                user </a></li>
                        <div class="divider"></div>
                        <li><a href="{{action('Auth\RegisterController@showRegistrationForm', 'Business')}}"> As a
                                business</a></li>
                    </ul>
                </li>--}}
            @else
                <?php
                $friends = \App\Friend::where('follower_id', \Illuminate\Support\Facades\Auth::user()->id)
                    ->where('status', 'Pending')
                    ->count();
                $messages = \App\Message::where('receiver_user_id', \Illuminate\Support\Facades\Auth::user()->id)
                    ->where('ReadType', 'NotRead')
                    ->count();
                ?>
                {{--Dropdown search--}}
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">SEARCH</a>
                    <ul class="dropdown-menu dropdown-messages ">
                        <form method="GET" action="{{action('AdvancedSearch@searchAll')}}">
                            {{csrf_field()}}
                            <div class="ibox float-e-margins" style="background-color: white">
                                <div class="form-group" id="toastTypeGroup" style="display: inline-block">
                                    <div class="radio col-lg-12 col-md-10 col-xs-10" style="margin-left: 20px">
                                        <label>
                                            <input class="checkbox-search" type="radio" name="toast" value="success"
                                                   checked/>Both
                                            <input class="checkbox-search " type="radio" name="toast" value="info"
                                                   style="margin-left: 25px"/>
                                            <label style="margin-left: 20px" for="">Indoor</label>
                                            <input class="checkbox-search" type="radio" name="toast" value="info"
                                                   style="margin-left: 25px"/>
                                            <label style="margin-left: 20px" for="">Outdoor</label>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-offset-1">
                                    <div class="form-group" style="margin-right: 5%">
                                        <select class="form-control advSearch" name="category">
                                            <option value="all">All</option>
                                            <option value="1">Essen & Trinken</option>
                                            <option value="2">Familie & Kinder</option>
                                            <option value="3">Firmenanlasse & Ausfluge</option>
                                            <option value="4">Fun and Adventure</option>
                                            <option value="5">Kultur</option>
                                            <option value="6">Romantik</option>
                                            <option value="7">Sport</option>
                                            <option value="8">Wellness & Beauty</option>
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-right: 5%">
                                        <input type="date" name="date" class="form-control advSearch" placeholder="Date"
                                               value="{{Request::input('date')}}">
                                    </div>
                                    <div class="form-group" style="margin-right: 5%">
                                        <input type="text" name="address" class="form-control advSearch"
                                               placeholder="Enter Address or Plz" value="{{Request::input('address')}}">
                                    </div>
                                </div>
                                <div class="form-group" id="event_radio" style="display: inline-block">
                                    <div class="radio col-lg-12 col-md-10 col-xs-10">
                                        <label>
                                            <input type="radio" name="eventuser" value="private"/>
                                            Private Event
                                            <i style="margin-left: 25px"></i>
                                            <input type="radio" name="eventuser" value="business"/>
                                            Business Event
                                            <i style="margin-left: 25px"></i>
                                            <input type="radio" name="eventuser" value="business"/>
                                            Business
                                        </label>
                                    </div>
                                </div>

                                <div class="slidecontainer" align="center">
                                    <input type="range" min="1" max="400" value="50" class="slider" id="myRange">
                                    <h2 style="box-shadow:  0 0 3px 0 #919191; width: 135px;"><span id="demo"></span> km
                                        area</h2>
                                </div>
                                <br>
                            </div>
                            <div align="center">
                                <button class="btn btn-danger" type="submit">SEARCH</button>
                            </div>
                        </form>
                    </ul>

                    <script>
                        $('.dropdown-menu').click(function (e) {
                            e.stopPropagation();
                        });
                    </script>
                </li>
                <li {{{ (Request::is('home') ? 'class=active' : '') }}}><a href="/">HOME</a></li>
                <li {{{ (Request::is('contact') ? 'class=active' : '') }}}><a href="/contact">CONTACT US</a></li>

                @if(Auth::user()->usertype == 'Business')
                    <li href="#">
                        <a class="count-info demo1" href="#">
                            <img id="alert-icon" src="/assets/img/icon/Notification.jpg" alt="">
                            <span class="label label-danger">0</span>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-toggle count-info demo1" href="#">
                            <img src="/assets/img/icon/Friends-icon-jpg.jpg" alt="">
                            <span class="label label-danger">{{$friends}}</span>
                        </a>
                    </li>
                    <li href="#">
                        <a class="dropdown-toggle count-info demo1"
                           href="#">
                            <img src="/assets/img/icon/Messages-icon-jpg.jpg" alt="">
                            <span class="label label-danger">{{$messages}}</span>
                        </a>
                    </li>
                @elseif(Auth::user()->usertype == 'Premium')
                    <?php $businesses = \App\Business::where('user_id', Auth::id())->take(1)->get(); ?>
                    @foreach($businesses as $business)
                        <li href="#">
                            <a class="count-info" href="/business/notifications/{{$business->id}}">
                                <img id="alert-icon" src="/assets/img/icon/Notification.jpg" alt="">
                                <span class="label label-danger">0</span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-toggle count-info" href="#">
                                <img src="/assets/img/icon/Friends-icon-jpg.jpg" alt="">
                                <span class="label label-danger">{{$friends}}</span>
                            </a>
                        </li>
                        <li href="#">
                            <a class="dropdown-toggle count-info"
                               href="/business/messages/{{$business->id}}">
                                <img src="/assets/img/icon/Messages-icon-jpg.jpg" alt="">
                                <span class="label label-danger">{{$messages}}</span>
                            </a>
                        </li>
                    @endforeach
                @elseif(Auth::user()->usertype == 'Individual')
                    <li>
                        <a class="dropdown-toggle count-info" href="/user/friends">
                            <img src="/assets/img/icon/Friends-icon-jpg.jpg" alt="">
                            <span class="label label-danger">{{$friends}}</span>
                        </a>
                    </li>
                    <li href="#">
                        <a class="dropdown-toggle count-info"
                           href="/user/messages/{{\Illuminate\Support\Facades\Auth::user()->id}}">
                            <img src="/assets/img/icon/Messages-icon-jpg.jpg" alt="">
                            <span class="label label-danger">{{$messages}}</span>
                        </a>
                    </li>
                    <li href="#">
                        <a class="count-info" href="/activity/create">
                            <button class="btn btn-danger " type="button"><i class="fa fa-plus-circle"></i>&nbsp;ADD
                                ACTIVITY
                            </button>
                        </a>
                    </li>
                @endif
                <li class="dropdown" href="#">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::user()->usertype == 'Individual')
                            <img id="user-login-icon" src="{{asset('/storage/profile/'.Auth::user()->image)}}" alt="">
                        @else
                            <img id="user-login-icon" src="/assets/img/icon/User-icon.png" alt="">
                        @endif
                        <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu btn-danger">
                        @if(Auth::user()->usertype == 'Business' || Auth::user()->usertype == 'Premium')
                            <?php $last_business_id = \App\Business::where('user_id', Auth::id())->take(1)->get();
                            ?>
                            @if($last_business_id->isEmpty())
                                <li><a href="{{ url('business/create') }}">Dashboard</a></li>
                            @else
                                <li><a href="{{ url('business/' . $last_business_id[0]->id) }}">Dashboard</a></li>
                            @endif
                        @elseif(Auth::user()->usertype == 'Individual')
                            <li><a href="/user/profile">Profile</a></li>
                        @endif
                        <div class="divider"></div>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        </ul>
    </div>
</nav>
<script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;

    slider.oninput = function () {
        output.innerHTML = this.value;
    }
</script>