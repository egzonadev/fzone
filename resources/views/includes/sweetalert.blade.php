<!-- Sweet Alert -->
<link href="/assets/css/sweetalert.css" rel="stylesheet">
<link href="/assets/css/animate.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">

<script src="/assets/js/sweetalert.min.js"></script>

<script>
    $(document).ready(function () {
        $('.demo1').click(function () {
            swal({
                title: "GO  PREMIUM",
                text: "More options and fun are with premium package,  go now!",

            });
        });
        $('.top').click(function () {
            swal({
                title: "YOU HAVE REACHED YOUR LIMIT",
                text: "You may only add 2 top businesses!",
                type: "warning"
            });
        });
        $('.simple').click(function () {
            swal({
                title: "YOU HAVE REACHED YOUR LIMIT",
                text: "You may only add 5 businesses!",
                type: "warning"
            });
        });
    });
</script>