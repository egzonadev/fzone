<div class="row">
    <h1 style="margin-top: 20px"></h1>
    <div class="col-lg-12 col-lg-offset-1 ">
        @auth
            @if(Auth::user()->usertype == 'Business' )
            @else
                <div class="col-lg-10">
                    <div class="input-group m-b">
                        <input type="text" class="form-control" placeholder="Create your company activity here...">
                        <span class="input-group-btn">
                         <a href="/activity/create" class="btn btn-danger">
                             CREATE NOW
                         </a>
                </span>
                    </div>
                </div>
            @endif
            <div class="col-lg-3" id="footer-text-color">
                <h2 id="footer-text-color"><b>Quick Links</b></h2>
                <li style="list-style-type: none;" class=""><a id="footer-text-color" href="#">About us</a></li>
                <li style="list-style-type: none;"><a id="footer-text-color" href="/">Site Map</a></li>
                <li style="list-style-type: none;"><a id="footer-text-color" href="#">Our Support</a></li>
                <li style="list-style-type: none;"><a id="footer-text-color" href="#">Terms & Condition</a></li>
            </div>
            <div class="col-lg-3" id="footer-text-color">
                <h2 id="footer-text-color"><b>Contact Us</b></h2>
                <p>You can contact us three way.</p>
                <p>Phone, Email, Office</p>
                <p class="fa fa-phone"> +111 111 111</p>
                <p class="fa fa-envelope"> freizeitzone@gmail.com</p>
                <p class="fa fa-map-marker"> Switzerland</p>
            </div>
            <div class="col-lg-4" id="footer-text-color">
                <h2 id="footer-text-color"><b>Newsletter</b></h2>
                <div class="input-group m-b">
                    <input type="text" class="form-control" placeholder="E-mail">
                    <span class="input-group-btn">
                         <a href="#" class="btn btn-danger">
                             <i class="fa fa-arrow-right"></i>
                         </a>
                </span>
                </div>
                <div class="social" style="margin-left: -10px; margin-top: 5px">
                    <ul>
                        <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-envelope"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-whatsapp"></i></a></li>
                    </ul>
                </div>
            </div>
        @endauth
        @guest
            <div class="col-lg-3" id="footer-text-color">
                <h2 id="footer-text-color"><b>Quick Links</b></h2>
                <li style="list-style-type: none;" class=""><a id="footer-text-color" href="#">About us</a></li>
                <li style="list-style-type: none;"><a id="footer-text-color" href="/">Site Map</a></li>
                <li style="list-style-type: none;"><a id="footer-text-color" href="#">Our Support</a></li>
                <li style="list-style-type: none;"><a id="footer-text-color" href="#">Terms & Condition</a></li>
            </div>
            <div class="col-lg-3" id="footer-text-color">
                <h2 id="footer-text-color"><b>Contact Us</b></h2>
                <p>You can contact us three way.</p>
                <p>Phone, Email, Office</p>
                <p class="fa fa-phone"> +111 111 111</p>
                <p class="fa fa-envelope"> freizeitzone@gmail.com</p>
                <p class="fa fa-map-marker"> Switzerland</p>
            </div>
            <div class="col-lg-4" id="footer-text-color">
                <h2 id="footer-text-color"><b>Newsletter</b></h2>
                <div class="input-group m-b">
                    <input type="text" class="form-control" placeholder="E-mail">
                    <span class="input-group-btn">
                         <a href="#" class="btn btn-danger">
                             <i class="fa fa-arrow-right"></i>
                         </a>
                </span>
                </div>
                <div class="social" style="margin-left: -10px; margin-top: 5px">
                    <ul>
                        <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-envelope"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-whatsapp"></i></a></li>
                    </ul>
                </div>
            </div>
        @endguest
    </div>
</div>

<div class="row" style="background-color: rgba(26,34,54,0.95); border-top: 1px solid lightgray">
    <div class="col-lg-8 col-lg-offset-2 m-t-lg m-b-lg">
        <ol class="breadcrumb pull-left" style="background-color: #1a2236; color: white">
            <li>
                <p>
                    <strong>&copy; 2018 by
                        <small style="color: #cc5965; text-decoration: underline">fushionlab@gmail.com</small>
                    </strong><br/>
                </p>
            </li>
        </ol>
        <ol class="breadcrumb pull-right" style="background-color: #1a2236; color: white">
            <li>
                <a href="index-2.html">Support</a>
            </li>
            <li>
                <a>Contact Us</a>
            </li>
            <li class="active">
                <strong>Add More</strong>
            </li>
        </ol>
    </div>
</div>
<style>
    @import url(http://fonts.googleapis.com/css?family=Fjalla+One);
    @import url(http://fonts.googleapis.com/css?family=Gudea);

    .footer1 {
        background: #fff url("../images/footer/footer-bg.png") repeat scroll left top;
        padding-top: 40px;
        padding-right: 0;
        padding-bottom: 20px;
        padding-left: 0;
        /*	border-top-width: 4px;
            border-top-style: solid;
            border-top-color: #003;*/
    }

    .title-widget {
        color: #898989;
        font-size: 20px;
        font-weight: 300;
        line-height: 1;
        position: relative;
        text-transform: uppercase;
        font-family: 'Fjalla One', sans-serif;
        margin-top: 0;
        margin-right: 0;
        margin-bottom: 25px;
        margin-left: 0;
        padding-left: 28px;
    }

    .title-widget::before {
        background-color: #ea5644;
        content: "";
        height: 22px;
        left: 0px;
        position: absolute;
        top: -2px;
        width: 5px;
    }

    .widget_nav_menu ul {
        list-style: outside none none;
        padding-left: 0;
    }

    .widget_archive ul li {
        background-color: rgba(0, 0, 0, 0.3);
        content: "";
        height: 3px;
        left: 0;
        position: absolute;
        top: 7px;
        width: 3px;
    }

    .widget_nav_menu ul li {
        font-size: 13px;
        font-weight: 700;
        line-height: 20px;
        position: relative;
        text-transform: uppercase;
        border-bottom: 1px solid rgba(0, 0, 0, 0.05);
        margin-bottom: 7px;
        padding-bottom: 7px;
        width: 95%;
    }

    .title-median {
        color: #636363;
        font-size: 20px;
        line-height: 20px;
        margin: 0 0 15px;
        text-transform: uppercase;
        font-family: 'Fjalla One', sans-serif;
    }

    .footerp p {
        font-family: 'Gudea', sans-serif;
    }

    #social:hover {
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -o-transform: scale(1.1);
    }

    #social {
        -webkit-transform: scale(0.8);
        /* Browser Variations: */
        -moz-transform: scale(0.8);
        -o-transform: scale(0.8);
        -webkit-transition-duration: 0.5s;
        -moz-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
    }

    /*
        Only Needed in Multi-Coloured Variation
                                                   */
    .social-fb:hover {
        color: #3B5998;
    }

    .social-tw:hover {
        color: #4099FF;
    }

    .social-gp:hover {
        color: #d34836;
    }

    .social-em:hover {
        color: #f39c12;
    }

    .nomargin {
        margin: 0px;
        padding: 0px;
    }

    .footer-bottom {
        background-color: #15224f;
        min-height: 30px;
        width: 100%;
    }

    .copyright {
        color: #fff;
        line-height: 30px;
        min-height: 30px;
        padding: 7px 0;
    }

    .design {
        color: #fff;
        line-height: 30px;
        min-height: 30px;
        padding: 7px 0;
        text-align: right;
    }

    .design a {
        color: #fff;
    }

    @import "//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css";

    .social {
        margin: 0;
        padding: 0;
    }

    .social ul {
        margin: 0;
        padding: 5px;
    }

    .social ul li {
        margin: 5px;
        list-style: none outside none;
        display: inline-block;
    }

    .social i {
        width: 30px;
        height: 30px;
        color: #FFF;
        background-color: #909AA0;
        font-size: 16px;
        text-align: center;
        padding-top: 9px;
        border-radius: 50%;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        -o-border-radius: 50%;
        transition: all ease 0.3s;
        -moz-transition: all ease 0.3s;
        -webkit-transition: all ease 0.3s;
        -o-transition: all ease 0.3s;
        -ms-transition: all ease 0.3s;
    }

    .social i:hover {
        color: #FFF;
        text-decoration: none;
        transition: all ease 0.3s;
        -moz-transition: all ease 0.3s;
        -webkit-transition: all ease 0.3s;
        -o-transition: all ease 0.3s;
        -ms-transition: all ease 0.3s;
    }

    .social .fa-facebook:hover {
        background: #4060A5;
    }

    .social .fa-twitter:hover {
        background: #00ABE3;
    }

    .social .fa-google-plus:hover {
        background: #e64522;
    }

    .social .fa-github:hover {
        background: #343434;
    }

    .social .fa-envelope:hover {
        background: #e80903;
    }

    .social .fa-whatsapp:hover {
        background: #8fe23f;
    }

    .social .fa-pinterest:hover {
        background: #cb2027;
    }

    .social .fa-linkedin:hover {
        background: #0094BC;
    }

    .social .fa-flickr:hover {
        background: #FF57AE;
    }

    .social .fa-instagram:hover {
        background: #375989;
    }

    .social .fa-vimeo-square:hover {
        background: #83DAEB;
    }

    .social .fa-stack-overflow:hover {
        background: #FEA501;
    }

    .social .fa-dropbox:hover {
        background: #017FE5;
    }

    .social .fa-tumblr:hover {
        background: #3a5876;
    }

    .social .fa-dribbble:hover {
        background: #F46899;
    }

    .social .fa-skype:hover {
        background: #00C6FF;
    }

    .social .fa-stack-exchange:hover {
        background: #4D86C9;
    }

    .social .fa-youtube:hover {
        background: #FF1F25;
    }

    .social .fa-xing:hover {
        background: #005C5E;
    }

    .social .fa-rss:hover {
        background: #e88845;
    }

    .social .fa-foursquare:hover {
        background: #09B9E0;
    }

    .social .fa-youtube-play:hover {
        background: #DF192A;
    }
</style>