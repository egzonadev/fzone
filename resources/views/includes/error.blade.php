@if (\Session::has('error'))
    <script>
        $(function () {
            toastr.warn('{{\Session::get('error')}}')
        });
    </script>
@elseif (\Session::has('success'))
    <script>
        $(function () {
            toastr.success('{{\Session::get('success')}}')
        });
    </script>
@endif