<div class="row" style="margin-top: 4%">
    {{--Happy Clients--}}
    <div>
        <h1 class="category-names">
            Happy Clients
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="contact-box center-version">
                <a href="profile.html">
                    <img alt="image" class="img-circle" src="assets/img/a2.jpg">
                    <h3 class="m-b-xs"><strong>John Smith</strong></h3>
                    <div class="font-bold">Graphics designer</div>
                    <p style="padding: 15px">Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and help others decide which apps they’d like to try. You can ask for ratings and respond to reviews to improve your app’s discoverability, encourage downloads, and build rapport with people who use your app</p>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="contact-box center-version">
                <a href="profile.html">
                    <img alt="image" class="img-circle" src="assets/img/a2.jpg">
                    <h3 class="m-b-xs"><strong>John Smith</strong></h3>
                    <div class="font-bold">Graphics designer</div>
                    <p style="padding: 15px">Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and help others decide which apps they’d like to try. You can ask for ratings and respond to reviews to improve your app’s discoverability, encourage downloads, and build rapport with people who use your app</p>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="contact-box center-version">
                <a href="profile.html">
                    <img alt="image" class="img-circle" src="assets/img/a2.jpg">
                    <h3 class="m-b-xs"><strong>John Smith</strong></h3>
                    <div class="font-bold">Graphics designer</div>
                    <p style="padding: 15px">Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and help others decide which apps they’d like to try. You can ask for ratings and respond to reviews to improve your app’s discoverability, encourage downloads, and build rapport with people who use your app</p>
                </a>
            </div>
        </div>
    </div>
</div>