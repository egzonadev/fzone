
<!-- Css Styles --> 
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/animate.css">
<link rel="stylesheet" href="/assets/css/font-awesome.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/toastr.min.css">
<link rel="stylesheet" href="/assets/css/jquery.steps.css">
<link rel="stylesheet" href="/assets/css/custom.css">
<link rel="stylesheet" href="/assets/css/morris-0.4.3.min.css">
<link rel="stylesheet" href="/assets/toastr/toastr.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-datepicker3.min.css">

<!-- JS scripts -->
{{-- <script src="/assets/js/jquery-3.1.1.min.js"></script> --}}
<script src="/assets/js/jquery-2.1.1.js"></script>   
<script src="/assets/js/bootstrap.min.js"></script>  
<script src="/assets/js/jquery.metisMenu.js"></script>
<script src="/assets/js/jquery.slimscroll.min.js"></script>
<script src="/assets/js/inspinia.js"></script>
<script src="/assets/js/pace.min.js"></script>
<script src="/assets/js/jquery.peity.min.js"></script>
<script src="/assets/js/jquery.steps.min.js"></script>
<script src="/assets/js/jquery.validate.min.js"></script>
<script src="/assets/toastr/toastr.min.js"></script>
{{--Morrich graphs--}}
<script src="/assets/js/raphael-2.1.0.min.js"></script>
<script src="/assets/js/morris.js"></script>

{{-- Adnani Data Picker Bootstrap --}}
<script src="/assets/js/moment-with-locales.js"></script>
<script src="/assets/js/bootstrap-datepicker.min.js"></script>


{{-- Adnani File Uploader --}}
<link href="/file-input/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/> 
<script src="/file-input/js/plugins/sortable.js" type="text/javascript"></script>
<script src="/file-input/js/fileinput.js" type="text/javascript"></script> 
 