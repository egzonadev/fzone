<?php
$premium_users = \App\User::all()->where('usertype', 'Premium');
foreach ($premium_users as $user) {
    $null [] = \App\Business::all()->where('user_id', $user->id)
        ->where('category', 'Fun and Adventure')
        ->first();
}?>
@if (!empty($null[0]) || !$fun_activities->isEmpty() )
    <div class="row">
        <div>
            <h1 class="category-names">
                Fun & Adventures
            </h1>
        </div>
        <?php if (!empty($null[0])) { $j = 0; ?>
        <div class="landing-page">
            <div id="premium" class="carousel carousel-fade" data-ride="carousel">
                <?php
                //Columns must be a factor of 12 (1,2,3,4,6,12)
                $numOfCols = 3;
                $rowCount = 0;
                $bootstrapColWidth = 12 / $numOfCols;
                ?>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        @foreach($premium_users as $user)
                            <?php $premium = \App\Business::where('user_id', $user->id)
                                ->where('category', 'Fun and Adventure')->get();?>
                        @endforeach
                        @foreach($premium as $business)
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                                <div class="widget-head-color-box navy-bg p-lg text-center"
                                     style="background-image: url('/storage/business/cover/{{$business['path_cover_image']}}');">
                                    <img src="/storage/business/logo/{{$business['path_logo_image']}}"
                                         class="img-circle-premium circle-border-premium m-b-md"
                                         alt="profile">
                                    <div>
                                        <img src="assets/img/selo_premium.png" class="solo-premium m-b-md"
                                             alt="profile">
                                    </div>
                                </div>
                                <div class="widget-text-box text-center">
                                    <a href="{{action('BusinessesController@details', $business['id'])}}">
                                        <h2 class="media-heading m-t-md"
                                            style="max-width: 90%; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                            <b>{{$business['name']}}</b></h2></a>
                                    <h4>{{$business['category']}}</h4>
                                    <p style="color: red">
                                        <?php $avg_premium = \App\BusinessReviews::getTopPremium(); ?>
                                        @if(!$avg_premium == null)
                                            @foreach($avg_premium as $top_avg)
                                                @if($top_avg->business_id == $business['id'])
                                                    @for ($i = 0; $i < round($top_avg->average, 0); $i++)
                                                        <i class="fa fa-star"></i>
                                                    @endfor
                                                    @for ($i = round($top_avg->average, 0); $i < 5; $i++)
                                                        <i class="fa fa-star-o"></i>
                                    @endfor
                                    @endif
                                    @endforeach
                                    @else
                                        <p style="color: red;">
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </p>
                                        @endif
                                        </p>
                                        <?php
                                        if ((date('H:i', strtotime('+2 hour')) > date('H:i', strtotime($business['closing_hours'])))
                                            ||
                                            (date('H:i', strtotime('+2 hour')) < date('H:i', strtotime($business['opening_hours'])))
                                        ) {
                                            $openStatus = 'Closed Now';
                                        } else {
                                            $openStatus = 'Open Now';
                                        }
                                        ?>
                                        <h4 style="color: red">{{ $openStatus  }} </h4>
                                        <a href="{{action('ActivitiesController@new',$business['id'])}}"
                                           class="btn btn-danger">CREATE ACTIVITY</a>
                                        <br>
                                </div>
                            </div>
                            <?php $rowCount++; ?>
                            <?php if ($rowCount % $numOfCols == 0) echo '</div><div class="item">';?>
                        @endforeach
                    </div>
                    <ol class="carousel-indicators text-center">
                        <li data-target="#premium" data-slide-to="0" class="active"></li>
                        @foreach($premium as $activity)
                            <?php $j++; ?>
                            <?php if ($j % $numOfCols == 0) echo '<li data-target="#premium" data-slide-to="1"></li>';?>
                        @endforeach
                    </ol>
                    <a class="left carousel-control" href="#premium" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a id="right-left-arrow" class="right carousel-control" href="#premium" role="button"
                       data-slide="next">
                    <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png"
                                                                         alt=""></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <?php }
        if (!$fun_activities->isEmpty()) { $i = 0;?>
        <div class="landing-page">
            <div id="fun" class="carousel carousel-fade" data-ride="carousel">
                <?php
                //Columns must be a factor of 12 (1,2,3,4,6,12)
                $numOfCols = 3;
                $rowCount = 0;
                $bootstrapColWidth = 12 / $numOfCols;
                ?>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        @foreach($fun_activities as $activity)
                            <div class="col-lg-4 col-md-4 col-xs-4">
                                <div class="widget-head-color-box navy-bg p-lg text-center"
                                     style="background-image: url('{{asset('/images/categories/'.$activity->category->first()->image)}}')">
                                    @if($activity['GetUser']->usertype == 'Individual')
                                        <img src="{{asset('/storage/profile/'.$activity['GetUser']->image)}}"
                                             class="img-small-profile"/>
                                    @else
                                        @if($activity->business_id == 0)
                                            <img src="/assets/img/user.jpg" class="img-small-profile"/>
                                        @else
                                            <img src="/storage/business/logo/{{$activity['getBusiness']->path_logo_image}}"
                                                 class="img-small-profile"/>
                                        @endif
                                    @endif
                                    @if( ! App\ActivityInterested::isFollowingActivity($activity->id))
                                        <?php $heart_style = '' ?>
                                        <form method="GET"
                                              action="{{action('ActivitiesController@addInterested', $activity->id)}}"
                                              class="form-horizontal">
                                            @else
                                                <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                                                <form method="GET"
                                                      action="{{action('ActivitiesController@addNotInterested', $activity->id)}}"
                                                      class="form-horizontal">
                                                    @endif
                                                    {{csrf_field()}}
                                                    <button style="{{$heart_style}}" id="heart-favorite"
                                                            class="btn btn-danger btn-circle btn-outline"
                                                            type="submit">
                                                        <i class="fa fa-heart"></i>
                                                    </button>
                                                </form>
                                </div>
                                <div class="widget-text-box">
                                    <a href="{{action('EventsController@index', $activity->id)}}">
                                        <h3 href="" class="media-heading m-t-md"><b>{{$activity->title}}</b></h3>
                                    </a>
                                    <h5 class="media-heading m-t-md"
                                        style="max-width: 270px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                        {{$activity->description}}<br><br>
                                    </h5>

                                    <div class="client-avatar">
                                        <row>
                                            <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                                      style="font-size: 15px"></i>
                                            </small>
                                            <span class="m-l-xs">{{ date("d/m/Y", strtotime( $activity->start_date_time ) ) }}  </span>
                                            <?php
                                            $attenders = App\Activity::GetAllActivityAttenders($activity->id);
                                            $count_attenders = App\Activity::GetAttendersCount($activity->id);
                                            ?>
                                            @foreach($attenders as $attender)
                                                @if($attender->usertype == 'Individual')
                                                    <img src="{{asset('/storage/profile/'.$attender->image)}}"/>
                                                @else
                                                    <img src="/assets/img/user.jpg"/>
                                                @endif
                                            @endforeach
                                            <span class="m-l-xs"> + {{$count_attenders}} attenders</span>
                                        </row>
                                    </div>
                                    <br>
                                    <div id="did-divider" class="divider"></div>
                                    <div class="text-right" style="  padding-top: 10px;">
                                        <small id="locaion-in-cards" class="icon" style="color: #cc5965">
                                            <i class="fa fa-map-marker">
                                                {{$activity->location . ', ' . $activity->city }}</i></small>
                                        @if( ! App\ActivityAttender::isAttendingActivity($activity->id))
                                            <?php $attending = 'Attend' ?>
                                            <form method="GET"
                                                  action="{{action('ActivitiesController@Attend', $activity->id)}}"
                                                  class="form-horizontal">
                                                @else
                                                    <?php $attending = 'Attending' ?>
                                                    <form method="GET"
                                                          action="{{action('ActivitiesController@RemoveAttend', $activity->id)}}"
                                                          class="form-horizontal">
                                                        @endif
                                                        {{csrf_field()}}
                                                        <button type="submit" class="btn btn-danger btn-xs">
                                                            <strong>{{$attending}}</strong>
                                                        </button>
                                                    </form>
                                    </div>
                                </div>
                            </div>
                            <?php $rowCount++; ?>
                            <?php if ($rowCount % $numOfCols == 0) echo '</div><div class="item">';?>
                        @endforeach
                    </div>
                    <ol class="carousel-indicators text-center">
                        <li data-target="#fun" data-slide-to="0" class="active"></li>
                        @foreach($fun_activities as $activity)
                            <?php $i++; ?>
                            <?php if ($i % $numOfCols == 0) echo '<li data-target="#fun" data-slide-to="1"></li>';?>
                        @endforeach
                    </ol>
                    <a class="left carousel-control" href="#fun" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a id="right-left-arrow" class="right carousel-control" href="#fun" role="button"
                       data-slide="next">
                    <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png" alt="">
                    </span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="row text-center">
                    <a href="#"><h4><b>Show All</b></h4></a>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
@endif
