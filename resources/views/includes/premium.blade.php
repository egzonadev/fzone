<?php
$premium_users = \App\User::all()->where('usertype', 'Premium');
foreach ($premium_users as $user) {
    $premium [] = \App\Business::all()->where('user_id', $user->id)->first();
}
if (!$premium_users->isEmpty()) { $i = 0; ?>
<div class="row">
    <div>
        <h1 class="category-names">
            Premium Business
        </h1>
    </div>
    <div class="landing-page">
        <div id="premium" class="carousel carousel-fade" data-ride="carousel">
            <?php
            //Columns must be a factor of 12 (1,2,3,4,6,12)
            $numOfCols = 3;
            $rowCount = 0;
            $bootstrapColWidth = 12 / $numOfCols;
            ?>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    @foreach($premium as $business)
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                            <div class="widget-head-color-box navy-bg p-lg text-center"
                                 style="background-image: url('/storage/business/cover/{{$business->path_cover_image}}');">
                                <img src="/storage/business/logo/{{$business->path_logo_image}}"
                                     class="img-circle-premium circle-border-premium m-b-md"
                                     alt="profile">
                                <div>
                                    <img src="assets/img/selo_premium.png" class="solo-premium m-b-md" alt="profile">
                                </div>
                            </div>
                            <div class="widget-text-box text-center">
                                <a href="{{action('BusinessesController@details', $business->id)}}">
                                    <h2 class="media-heading m-t-md"
                                        style="max-width: 90%; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                        <b>{{$business->name}}</b></h2></a>
                                <h4>{{$business->category}}</h4>
                                <p style="color: red">
                                    <?php $avg_premium = \App\BusinessReviews::getTopPremium(); ?>
                                    @if(!$avg_premium == null)
                                        @foreach($avg_premium as $top_avg)
                                            @if($top_avg->business_id == $business->id)
                                                @for ($i = 0; $i < round($top_avg->average, 0); $i++)
                                                    <i class="fa fa-star"></i>
                                                @endfor
                                                @for ($i = round($top_avg->average, 0); $i < 5; $i++)
                                                    <i class="fa fa-star-o"></i>
                                                @endfor
                                            @endif
                                        @endforeach
                                    @else
                                    <p style="color: red;">
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </p>
                                    @endif
                                    </p>
                                    <?php
                                    if ((date('H:i', strtotime('+2 hour')) > date('H:i', strtotime($business->closing_hours)))
                                        ||
                                        (date('H:i', strtotime('+2 hour')) < date('H:i', strtotime($business->opening_hours)))
                                    ) {
                                        $openStatus = 'Closed Now';
                                    } else {
                                        $openStatus = 'Open Now';
                                    }
                                    ?>
                                    <h4 style="color: red">{{ $openStatus  }} </h4>
                                    <a href="{{action('ActivitiesController@new',$business->id)}}"
                                       class="btn btn-danger">CREATE ACTIVITY</a>
                                    <br>
                            </div>
                        </div>
                        <?php $rowCount++; ?>
                        <?php if ($rowCount % $numOfCols == 0) echo '</div><div class="item">';?>
                    @endforeach
                </div>
                <ol class="carousel-indicators text-center">
                    <li data-target="#premium" data-slide-to="0" class="active"></li>
                    @foreach($premium as $activity)
                        <?php $i++; ?>
                        <?php if ($i % $numOfCols == 0) echo '<li data-target="#premium" data-slide-to="1"></li>';?>
                    @endforeach
                </ol>
                <a class="left carousel-control" href="#premium" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a id="right-left-arrow" class="right carousel-control" href="#premium" role="button"
                   data-slide="next">
                    <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png"
                                                                         alt=""></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<?php } ?>