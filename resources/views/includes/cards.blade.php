<div>
    <h1 class="category-names">
        Fun & Adventures
    </h1>
</div>
<div class="landing-page">
    <div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/categories/bicycle.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/categories/airplane.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/categories/canon.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="item">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/ff.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Slide2 at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/ff.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Slide3333 at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <ol class="carousel-indicators text-center">
            <li data-target="#inSlider" data-slide-to="0" class="active"></li>
            <li data-target="#inSlider" data-slide-to="1"></li>
            <li data-target="#inSlider" data-slide-to="2"></li>
        </ol>
        <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="right-left-arrow" class="right carousel-control" href="#inSlider" role="button" data-slide="next">
            <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png" alt=""></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row text-center">
        <a href=""><h4> <b>Show All</b></h4></a>
    </div>
</div>

{{-- Food & Drinks--}}
<div>
    <h1 class="category-names">
        Food & Drinks
    </h1>
</div>
<div class="landing-page">
    <div id="foodSlide" class="carousel carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                {{-- @foreach($user_a as $row)
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">

                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                        <img src="{{asset('/storage/profile/'.$row['image'])}}" class="img-small-profile" alt="profile">

                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline        " type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                @endforeach --}}
            </div>
            <div class="item">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/ff.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Slide2 at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/ff.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Slide3333 at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ol class="carousel-indicators text-center">
            <li data-target="#foodSlide" data-slide-to="0" class="active"></li>
            <li data-target="#foodSlide" data-slide-to="1"></li>
            <li data-target="#foodSlide" data-slide-to="2"></li>
        </ol>
        <a class="left carousel-control" href="#foodSlide" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="right-left-arrow" class="right carousel-control" href="#foodSlide" role="button" data-slide="next">
            <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png" alt=""></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row text-center">
        <a href=""><h4> <b>Show All</b></h4></a>
    </div>
</div>

{{-- Culture--}}
<div>
    <h1 class="category-names">
        Culture
    </h1>
</div>
<div class="landing-page">
    <div id="cultSlide" class="carousel carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cr.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/fd.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/ff.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Slide2 at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/ff.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Slide3333 at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-4">
                    <div class="widget-head-color-box navy-bg p-lg text-center"
                         style="background-image: url('assets/img/cc.jpg')">
                        <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                        <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i
                                    class="fa fa-heart"></i></button>
                    </div>

                    <div class="widget-text-box">
                        <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                        <h5 class="media-heading m-t-md" style="text-align: justify">
                            Here is a description, some text about the event and more details... <br><br>
                        </h5>

                        <div class="client-avatar">
                            <row>
                                <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                          style="font-size: 15px"></i></small>
                                <span class="m-l-xs">26/04/2018    </span>
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <img alt="image" src="assets/img/viewImage.jpg">
                                <span class="m-l-xs"> +50 attenders</span>
                            </row>
                        </div>
                        <br>
                        <div id="did-divider" class="divider"></div>
                        <div class="text-right" style="  padding-top: 10px;">
                            <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker">
                                    Graubunden, Bern</i></small>
                            <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ol class="carousel-indicators text-center">
            <li data-target="#cultSlide" data-slide-to="0" class="active"></li>
            <li data-target="#cultSlide" data-slide-to="1"></li>
            <li data-target="#cultSlide" data-slide-to="2"></li>
        </ol>
        <a class="left carousel-control" href="#cultSlide" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="right-left-arrow" class="right carousel-control" href="#cultSlide" role="button" data-slide="next">
            <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png" alt=""></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="row text-center">
        <a href=""><h4> <b>Show All</b></h4></a>
    </div>
</div>

{{--PAGINATION--}}
<div class="text-center">
    <ul class="pagination">
    <a href="#">&laquo;</a>
    <a href="#">1</a>
    <a href="#" class="active">2</a>
    <a href="#">3</a>
    <a href="#">&raquo;</a>
    </ul>
</div>

{{--Top Cities--}}
<div>
    <h1 class="category-names">
        Top Cities
    </h1>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="carousel slide" id="carousel3">
                        <div class="item gallery active left">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="col-sm-12">
                                        <div class="gallery-container">
                                            <img alt="image"  class="img-responsive" src="assets/img/p_big1.jpg">
                                            <h3>LUGANO</h3>
                                            <small>3 listing</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 20px">
                                        <div class="gallery-container">
                                            <img alt="image"  class="img-responsive" src="assets/img/p_big2.jpg">
                                            <h3>Bern</h3>
                                            <small>3 listing</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="col-sm-12">
                                        <div class="gallery-container">
                                            <img alt="image"  class="img-responsive" src="assets/img/p_big3.jpg">
                                            <h3>Zurich</h3>
                                            <small>3 listing</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="margin-top: 20px">
                                        <div class="gallery-container">
                                            <img alt="image"  class="img-responsive" src="assets/img/p_big1.jpg">
                                            <h3>Basel</h3>
                                            <small>3 listing</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="col-sm-12">
                                        <div class="gallery-container">
                                            <img alt="image"  class="img-responsive" src="assets/img/p_big2.jpg">
                                            <h3>Geneva</h3>
                                            <small>3 listing</small>
                                        </div>
                                    </div>
                                    <div style="line-height: 10px"></div>
                                    <div class="col-sm-12"style="margin-top: 20px">
                                        <div class="gallery-container">
                                            <img alt="image"  class="img-responsive" src="assets/img/p_big3.jpg">
                                            <h3>Bern</h3>
                                            <small>3 listing</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


{{--Happy Clients--}}
<div>
    <h1 class="category-names">
        Happy Clients
    </h1>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="contact-box center-version">
            <a href="profile.html">
                <img alt="image" class="img-circle" src="assets/img/a2.jpg">
                <h3 class="m-b-xs"><strong>John Smith</strong></h3>
                <div class="font-bold">Graphics designer</div>
                <p style="padding: 15px">Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and help others decide which apps they’d like to try. You can ask for ratings and respond to reviews to improve your app’s discoverability, encourage downloads, and build rapport with people who use your app</p>
            </a>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="contact-box center-version">
            <a href="profile.html">
                <img alt="image" class="img-circle" src="assets/img/a2.jpg">
                <h3 class="m-b-xs"><strong>John Smith</strong></h3>
                <div class="font-bold">Graphics designer</div>
                <p style="padding: 15px">Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and help others decide which apps they’d like to try. You can ask for ratings and respond to reviews to improve your app’s discoverability, encourage downloads, and build rapport with people who use your app</p>
            </a>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="contact-box center-version">
            <a href="profile.html">
                <img alt="image" class="img-circle" src="assets/img/a2.jpg">
                <h3 class="m-b-xs"><strong>John Smith</strong></h3>
                <div class="font-bold">Graphics designer</div>
                <p style="padding: 15px">Customers provide ratings and reviews on the App Store to give feedback on their experience with an app and help others decide which apps they’d like to try. You can ask for ratings and respond to reviews to improve your app’s discoverability, encourage downloads, and build rapport with people who use your app</p>
            </a>
        </div>
    </div>
</div>

{{--company loggo--}}
{{--

<div class="landing-page text-center">
    <div id="cultSlide" class="carousel carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
            <div class="row">
                <div class="col-lg-2">
                    <img src="assets/img/burj-al-arab.png" alt="">
                </div>
                <div class="col-lg-2">
                    <img src="assets/img/burj-al-arab.png" alt="">
                </div>
                <div class="col-lg-2">
                    <img src="assets/img/burj-al-arab.png" alt="">
                </div>
                <div class="col-lg-2">
                    <img src="assets/img/burj-al-arab.png" alt="">
                </div>
                <div class="col-lg-2">
                    <img src="assets/img/burj-al-arab.png" alt="">
                </div>
                <div class="col-lg-2">
                    <img src="assets/img/burj-al-arab.png" alt="">
                </div>
            </div>
            </div>
        </div>

        --}}
{{--slider info--}}{{--

        <a class="left carousel-control" href="#cultSlide" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
            <span class="sr-only">Previous</span>
        </a>
        <a id="right-left-arrow" class="right carousel-control" href="#cultSlide" role="button" data-slide="next">
            <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png" alt=""></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>--}}
