@extends('layout.master')
@section('page-content')
    <div class="container" style="margin-top: 15%">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-4 col-lg-offset-2">
                    <a href="{{action('Auth\RegisterController@showRegistrationForm', 'Individual')}}">
                        <div class="widget red-bg p-lg text-center">
                            <div class="m-b-md">
                                <i class="fa fa-user fa-4x"></i>
                                <h3 class="font-bold no-margins">
                                    As a user
                                </h3>
                                <small>Create private user account.</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="{{action('Auth\RegisterController@showRegistrationForm', 'Business')}}">
                        <div class="widget white-bg p-lg text-center">
                            <div class="m-b-md">
                                <i class="fa fa-user fa-4x"></i>
                                <h1 class="m-xs"></h1>
                                <h3 class="font-bold no-margins">
                                    As a business
                                </h3>
                                <small>Create business account.</small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop