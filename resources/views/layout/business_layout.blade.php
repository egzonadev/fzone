@extends('layout.master')
@section('main_content')
    <?php
    use App\Http\Controllers\BusinessesController;
    ?>
    <div class="business-background-img"
         style=" background-image: url('/storage/business/cover/{{$business->path_cover_image}}');">
    </div>
    <div class="container">
        <div class="row border-bottom white-bg page-heading">
            <div class="col-lg-2">
                <div id="businrss-profile-pic" class="col-lg-2 text-center">
                    <div class="text-center">
                        <img src="/storage/business/logo/{{$business->path_logo_image}}" class="img-square img-md"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="b-r-n-r">
                    <h2 class="media-heading m-t-md"><b>{{$business->name}}</b></h2>
                    <h4>{{$business->type}}</h4>
                    <p style="color: red">

                        @for ($i = 0; $i < $avg_reviews; $i++)
                            <i class="fa fa-star"></i>
                        @endfor

                        @for ($i = $avg_reviews; $i < 5; $i++)
                            {{-- <i class="fa fa-star-half-o"></i>  --}}
                            <i class="fa fa-star-o"></i>
                        @endfor

                    </p>
                    <?php
                    if ((date('H:i', strtotime('+2 hour')) > date('H:i', strtotime($business->closing_hours)))
                        ||
                        (date('H:i', strtotime('+2 hour')) < date('H:i', strtotime($business->opening_hours)))
                    ) {
                        $openStatus = 'Closed Now';
                    } else {
                        $openStatus = 'Open Now';
                    }
                    ?>
                    <h4 style="color: red">{{ $openStatus  }} </h4>
                </div>
            </div>
            <div class="col-lg-6" style="margin-top: 5px">
                <div class="pull-right">
                    @if( !\App\BusinessReports::reported($business['id']))
                        <a class="btn btn-outline btn-danger" style="height: 35px" title="Report" data-toggle="modal"
                           data-target="#modal-report-{{$business['id']}}">
                            <i class="fa fa-warning"></i>
                        </a>
                    @elseif( \App\BusinessReports::reported($business['id']))
                        <a class="btn btn-danger" style="height: 35px;" title="Report" data-toggle="modal"
                           data-target="#modal-reported">
                            <i class="fa fa-warning"></i>
                        </a>
                    @endif
                    <div class="modal fade" id="modal-reported">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header  text-center">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <div class="icon-box" style="font-size: 80px; color: red;">
                                        <i class="text-red fa fa-warning"></i>
                                    </div>
                                    <h3>You have already made a report for this business.</h3>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-report-{{$business['id']}}">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header  text-center">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <div class="icon-box" style="font-size: 80px">
                                        <i class="text-red fa fa-close"></i>
                                    </div>
                                    <h4 class="modal-title"><b>Choose report status if you want to proceed with this
                                            report.</b></h4>
                                </div>

                                <form action="{{action('UsersController@report', $business->id)}}" method="post">
                                    {{csrf_field()}}
                                    <div class="modal-body col-lg-offset-1">
                                        <h2><strong>
                                                <select name="report_status" id="report_status">
                                                    <option value="Fake Account">Fake Account</option>
                                                    <option value="Fake Profile">Fake Profile</option>
                                                    <option value="Fake">Other</option>
                                                </select>
                                            </strong>
                                        </h2>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                                            Cancel
                                        </button>

                                        <button type="submit" class="btn btn-danger" style="height: 35px"
                                                title="Report">
                                            <i class="fa fa-warning"> Report</i>
                                        </button>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <a href="/user/messages/{{Auth::user()->id}}" class="btn btn-outline btn-danger"
                       style="height: 35px;" title="Send Message"><i class="fa fa-envelope-o"></i></a>

                    {{-- Do Not Delete this, it works but the design is not correct.. --}}
                    @if( ! App\BusinessLike::likesBusiness($business->id))
                        <form method="post" action="{{action('BusinessesController@Like', $business->id)}}">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-outline btn-danger"
                                    style="height: 35px; margin-top: -35px; margin-left: -45px;" title="Favorite">
                                <i class="fa fa-heart"></i></button>
                        </form>
                    @elseif(App\BusinessLike::likesBusiness($business->id))
                        <form method="post" action="{{action('BusinessesController@Dislike', $business->id)}}">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-danger" style="height: 35px; margin-top: -35px; margin-left: -45px;" title="Favorite">
                                <i class="fa fa-heart"></i></button>
                        </form>
                    @endif
                    {{--<button type="submit" class="btn btn-outline btn-danger" style="height: 35px;" title="Favorite"><i
                                class="fa fa-heart"></i></button>--}}
                    <button type="button" class="btn btn-outline btn-danger" style="margin-top: -47%; margin-left: 52%; height: 35px;" title="Share"><i
                                class="fa fa-share-alt"></i></button>
                    {{-- Nese nuk eshte business i jem atehere hide add activity --}}
                    <a href="/activity/new/{{$business->id}}" class="btn btn-danger" style="height: 35px; margin-right: -60%; margin-top: -50%;"
                       title="Click to add activity"><i class="fa fa-plus-circle"></i> ADD ACTICITY</a>
                </div>
            </div>
            <div class="col-lg-10 col-md-9 col-md-offset-3 col-xs-12 pull-right" style="margin-bottom: 20px">
                <ul class="nav navbar-top-links" style="margin-bottom: -20px; margin-left: 5% ">
                    <li {{ Request::is('business/*/details') ? 'class=active' : '' }}>
                        <a class="dropdown-toggle count-info" href="/business/{{$business->id}}/details">
                            <span class="user-mini-navbar">Our Details</span>
                        </a>
                    </li>
                    <li {{ Request::is('business/*/events') ? ' class=active' : '' }}>
                        <a class="dropdown-toggle count-info" href="/business/{{$business->id}}/events">
                            <span class="user-mini-navbar">Events</span>
                        </a>
                    </li>
                    <li {{ Request::is('business/*/reviews') ? ' class=active' : '' }}>
                        <a class="dropdown-toggle count-info" href="/business/{{$business->id}}/reviews">
                            <span class="user-mini-navbar">Customer Reviews</span>
                        </a>
                    </li>
                </ul>
            </div>

            @yield('sub-nav')
        </div>
    </div>
@stop