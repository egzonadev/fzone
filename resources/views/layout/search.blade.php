@extends('layout.master')
@section('main_content')
    <div class="landing-img-wrap">
        <div class="landing-img">
            <h1 >Explore your great country</h1>
        </div>
    </div>
    <div>
        <h1 class="category-names">
            Search Results
        </h1>
    </div>
    <div style="margin-left: 40px; margin-right: 40px; padding-bottom: 20px">
        <div id="cultSlide" class="carousel carousel-fade" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    @if(count($activities) > 0)
                        @foreach($activities as $activity)
                            <div class="col-lg-3 col-md-4 col-xs-6">
                                <div class="widget-head-color-box navy-bg p-lg text-center"
                                     style="background-image: url('{{asset('/images/categories/'. App\ActivityCategories::getCategoryImage( $activity->id )->image )}}')">
                                        <img src="{{asset('/storage/profile/'.$activity->GetUser->image)}}" class="img-small-profile"/>
                                    @if( ! App\ActivityInterested::isFollowingActivity($activity->id))
                                        <?php $heart_style = '' ?>
                                        <form method="GET"
                                              action="{{action('ActivitiesController@addInterested', $activity->id)}}"
                                              class="form-horizontal">
                                            @else
                                                <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                                                <form method="GET"
                                                      action="{{action('ActivitiesController@addNotInterested', $activity->id)}}"
                                                      class="form-horizontal">
                                                    @endif
                                                    {{csrf_field()}}
                                                    <button style="{{$heart_style}}" id="heart-favorite"
                                                            class="btn btn-danger btn-circle btn-outline" type="submit">
                                                        <i class="fa fa-heart"></i>
                                                    </button>
                                                </form>
                                </div>

                                <div class="widget-text-box">
                                    <a href="{{action('EventsController@index', $activity->id)}}">
                                        <h3 href="" class="media-heading m-t-md"><b>{{$activity->title}}</b></h3>
                                    </a>
                                    <h5 class="media-heading m-t-md" style="max-width: 270px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                        {{$activity->description}}
                                        <br><br>
                                    </h5>

                                    <div class="client-avatar">
                                        <row>
                                            <small class="icon" style="color: red"><i class="fa fa-calendar"
                                                                                      style="font-size: 15px"></i>
                                            </small>
                                            <span class="m-l-xs">{{ date("d/m/Y", strtotime( $activity->start_date_time ) ) }}  </span>
                                            <?php
                                            $attenders = App\Activity::GetAllActivityAttenders($activity->id);
                                            $count_attenders = App\Activity::GetAttendersCount($activity->id);
                                            ?>
                                            @foreach($attenders as $attender)
                                                <img alt="image" src="/storage/profile/{{$attender->image}}">
                                            @endforeach
                                            <span class="m-l-xs"> +{{$count_attenders}} attenders</span>
                                        </row>
                                    </div>
                                    <br>
                                    <div id="did-divider" class="divider"></div>
                                    <div class="text-right" style="  padding-top: 10px;">
                                        <small id="locaion-in-cards" class="icon" style="color: #cc5965">
                                            <i class="fa fa-map-marker">
                                                {{$activity->location . ', ' . $activity->city }}</i></small>
                                        @if( ! App\ActivityAttender::isAttendingActivity($activity->id))
                                            <?php $attending = 'Attend' ?>
                                            <form method="GET"
                                                  action="{{action('ActivitiesController@Attend', $activity->id)}}"
                                                  class="form-horizontal">
                                                @else
                                                    <?php $attending = 'Attending' ?>
                                                    <form method="GET"
                                                          action="{{action('ActivitiesController@RemoveAttend', $activity->id)}}"
                                                          class="form-horizontal">
                                                        @endif
                                                        {{csrf_field()}}
                                                        <button type="submit" class="btn btn-danger btn-xs">
                                                            <strong>{{$attending}}</strong>
                                                        </button>
                                                    </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-lg-12 col-md-12 col-xs-12">
                            <h3>Keine Veranstaltung wurde gefunden!</h3>
                        </div>
                    @endif

                </div>

            </div>
        </div>
        {{--<div class="row text-center">
            {{$activities->links()}}
        </div>--}}
    </div>
@stop