<html>
<head>
    @include('includes.head')
    @include('includes.error')
</head>
<body class="top-navigation">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            @include('includes.header')
            @yield('main_content')'
        </div>
        <div class="row">
              @yield('page-content')
        </div>
    </div>
</div>
<footer style="background-color: #1a2d41; ">
    @include('includes.footer')
</footer>
</body>
</html>