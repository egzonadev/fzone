<div style="margin-top: 4%">
    @include('includes/category/fun')
</div>
<div style="margin-top: 4%">
    @include('includes/category/essen')
</div>
<div style="margin-top: 4%">
    @include('includes/category/family')
</div>
<div style="margin-top: 4%">
    @include('includes/category/wellness')
</div>
<div style="margin-top: 4%">
    @include('includes/category/ausfluge')
</div>
<div style="margin-top: 4%">
    @include('includes/category/romantik')
</div>
<div style="margin-top: 4%">
    @include('includes/category/kultur')
</div>
<div style="margin-top: 4%">
    @include('includes/category/sport')
</div>
