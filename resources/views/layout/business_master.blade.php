@extends('layout.master')
@section('main_content')
    @include('includes.sweetalert')
    <?php
    $businesses = new App\Business();
    $user_businesses = $businesses->get_All_User_Business();
    $count = 1;
    $is_upgraded = \App\Http\Controllers\UsersController::isUpgradedUser();
    ?>
    @if( $is_upgraded)
        @if($business->path_cover_image != null)
            <div class="business-background-img"
                 style=" background-image: url('/storage/business/cover/{{$business->path_cover_image}}');">
                @else
                    <div class="business-background-img"
                         style=" background-image: url('/assets/img/cover.jpg');">
                        @endif
                        @else
                            <div class="business-background-img"
                                 style=" background-image: url('/assets/img/cover.jpg');">
                                @endif
                            </div>
                            <div class="container">
                                <div class="row border-bottom white-bg page-heading" style="padding-bottom: 4%">
                                    <div class="col-lg-2">
                                        <div id="businrss-profile-pic" class="col-lg-2 text-center">
                                            <div class="text-center">
                                                @if( $is_upgraded)
                                                    @if($business->path_logo_image != null)
                                                        <img src="/storage/business/logo/{{$business->path_logo_image}}"
                                                             class="img-square img-md"
                                                             alt="Edit your business to add image"/>
                                                    @else
                                                        <img src="/assets/img/user.jpg" class="img-square img-md"/>
                                                    @endif
                                                @else
                                                    <img src="/assets/img/user.jpg" class="img-square img-md"/>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="b-r-n-r">
                                            <h2 class="media-heading m-t-md"><b>{{$business->name}}</b></h2>
                                            <h4>{{$business->type}}</h4>
                                            <p style="color: red">
                                                @for ($i = 0; $i < $avg_reviews; $i++)
                                                    <i class="fa fa-star"></i>
                                                @endfor
                                                @for ($i = $avg_reviews; $i < 5; $i++)
                                                    <i class="fa fa-star-o"></i>
                                                @endfor
                                            </p>
                                            <?php
                                            if ((date('H:i', strtotime('+2 hour')) > date('H:i', strtotime($business->closing_hours)))
                                                ||
                                                (date('H:i', strtotime('+2 hour')) < date('H:i', strtotime($business->opening_hours)))
                                            ) {
                                                $openStatus = 'Closed Now';
                                            } else {
                                                $openStatus = 'Open Now';
                                            }
                                            ?>
                                            <h4 style="color: red">{{ $openStatus  }} </h4>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 pull-right" style="margin-top: 5px">
                                        @if( !$is_upgraded && Auth::user()->has_requested_upgrade == 'false' )
                                            <form method="POST" action="{{action('BusinessesController@upgrade')}}">
                                                {{csrf_field()}}
                                                <button class="btn btn-danger" type="submit">
                                                    <i class="fa fa-plus-circle"></i>UPGRADE TO PREMIUM
                                                </button>
                                            </form>
                                        @elseif(Auth::user()->has_requested_upgrade == 'true')
                                            <button class="btn btn-danger" type="button">PENDING UPGRADE
                                            </button>
                                        @elseif($is_upgraded)
                                            <?php
                                            $top = \App\Business::all()->where('user_id', \Illuminate\Support\Facades\Auth::user()->id)
                                                ->where('business_type', 'Top')
                                                ->count();
                                            $simple = \App\Business::all()->where('user_id', \Illuminate\Support\Facades\Auth::user()->id)
                                                ->where('business_type', 'Simple')
                                                ->count();
                                            ?>
                                            @if($top < 2)
                                            <a href="{{action('BusinessesController@createBusiness','top')}}" class="btn btn-danger">
                                                ADD TOP EVENT
                                            </a>
                                            @elseif($top >= 2)
                                             <a class="top btn btn-danger">
                                                  ADD TOP EVENT
                                             </a>
                                             @endif
                                             @if($simple < 5)
                                             <a href="{{action('BusinessesController@createBusiness','simple')}}" class="btn btn-danger">
                                                 ADD ANOTHER BUSINESS</a>
                                             @elseif($simple >= 5)
                                                <a class="simple btn btn-danger">ADD ANOTHER BUSINESS</a>
                                             @endif

                                            {{--<li class="dropdown" style="list-style-type:none">
                                                <a aria-expanded="false" role="button" href="#"
                                                   class="btn btn-danger dropdown-toggle" data-toggle="dropdown"</a>
                                                <ul role="menu" class="dropdown-menu btn-danger text-center">

                                                        <li>
                                                            <a href="{{action('BusinessesController@createBusiness','top')}}">
                                                                Add Top Business</a></li>
                                                    @elseif($top >= 2)
                                                        <li><a class="top">Add Top Business</a></li>
                                                    @endif
                                                    <div class="divider"></div>
                                                    @if($simple < 5)
                                                        <li>
                                                            <a href="{{action('BusinessesController@createBusiness','simple')}}">
                                                                Add Business</a></li>
                                                    @elseif($simple >= 5)
                                                        <li><a class="simple">Add Business</a></li>
                                                    @endif
                                                </ul>
                                            </li>
--}}

                                            {{--  <select class="btn btn-danger" name="addBusiness">
                                                  <option style="display:none">ADD BUSINESS</option>
                                                  <option href="{{ url('business/create') }}" value="Business">Business</option>
                                                  <option href="{{ url('business/create') }}" value="Top Business">Top Business</option>
                                              </select>--}}
                                        @endif
                                    </div>
                                    <div class="col-lg-10 col-md-9 col-md-offset-3 col-xs-12 pull-right">
                                        <ul class="nav navbar-top-links" style="margin-bottom: -20px; margin-left: 5% ">
                                            <li{{ Request::is('business/') ? ' class=active' : '' }}>
                                                <a class="dropdown-toggle count-info"
                                                   href="/business/{{$business->id}}">
                                                    <img src="/assets/img/icon/dash.png" alt=""
                                                         id="user-icon-mini-navbar">
                                                    <span class="user-mini-navbar">Dashboard</span>
                                                </a>
                                            </li>

                                            <li{{ Request::is('business/events/*') ? ' class=active' : '' }}>
                                                @if( $is_upgraded)
                                                    <a class="dropdown-toggle count-info"
                                                       href="/business/events/{{$business->id}}">
                                                        @else
                                                            <a class="demo1 dropdown-toggle count-info btn-sm "
                                                               href="#">
                                                                @endif
                                                                <img src="/assets/img/icon/activity.png" alt=""
                                                                     id="user-icon-mini-navbar">
                                                                <span class="user-mini-navbar">Events</span>
                                                            </a></a>
                                            </li>
                                            <li {{ Request::is('business/notifications/*') ? ' class=active' : '' }}>
                                                @if( $is_upgraded)
                                                    <a class="dropdown-toggle count-info"
                                                       href="/business/notifications/{{$business->id}}">
                                                        @else
                                                            <a class="dropdown-toggle count-info btn-sm demo1" href="#">
                                                                @endif
                                                                <img src="/assets/img/icon/icons-notificatio.png" alt=""
                                                                     id="user-icon-mini-navbar">
                                                                <span class="user-mini-navbar">Notification</span>
                                                            </a></a>
                                            </li>
                                            <li {{ Request::is('business/messages/*') ? ' class=active' : '' }}>
                                                @if( $is_upgraded)
                                                    <a class="dropdown-toggle count-info"
                                                       href="/business/messages/{{$business->id}}">
                                                        @else
                                                            <a class="dropdown-toggle count-info btn-sm demo1" href="#">
                                                                @endif
                                                                <img src="/assets/img/icon/emails.png" alt=""
                                                                     id="user-icon-mini-navbar">
                                                                <span class="user-mini-navbar">Messages</span>
                                                            </a></a>
                                            </li>
                                            <li {{ Request::is('business/edit') ? ' class=active' : '' }}>
                                                <a class="dropdown-toggle count-info"
                                                   href="/business/edit/{{$business->id}}">
                                                    <img src="/assets/img/icon/icons8-user-64.png" alt=""
                                                         id="user-icon-mini-navbar">
                                                    <span class="user-mini-navbar">Edit Profile</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="fh-breadcrumb">
                                    <div class="fh-column">
                                        <div class="full-height-scroll">
                                            <ul class="list-group elements-list">
                                                @foreach($user_businesses as $u_business)
                                                    <li class="list-group-item @if($count == $business->id) {{"active mmm"}} @endif">
                                                        <a href="/business/{{$u_business->id}}">
                                                            <strong>{{$u_business->name}}</strong>
                                                        </a>
                                                    </li>
                                                    <?php $count++; ?>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="full-height">
                                        <div class="full-height-scroll white-bg border-left">
                                            <div class="element-detail-box">
                                                <div class="tab-content">
                                                    <div id="tab-1" class="active tab-pane">
                                                        @yield('business_dashboard')
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script>
                                    $(function () {
                                        $('html, body').animate({
                                            scrollTop: $(".business-background-img").offset().top + 250
                                        }, 5);
                                    });
                                </script>
                            </div>
@stop