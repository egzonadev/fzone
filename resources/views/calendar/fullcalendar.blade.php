@extends('super_admin.master')
@section('content')

   {{-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <link rel="stylesheet" href="/assets/css/fullcalendar.min.css">


    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background-color: #e5e5e5">
                    <h5>Events Type</h5>
                </div>
                <div class="ibox-content">
                    <div id='external-events'>
                        <ul class="category-list" style="padding: 10px">
                            <li><a href="#"> <i class="fa fa-circle text-navy"></i> Indoor </a></li>
                            <li><a href="#"> <i class="fa fa-circle text-danger"></i> Outdoor</a></li>
                        </ul>
                    </div>
                </div>
                <br>
                <div class="ibox-title" style="background-color: #e5e5e5">
                    <h5>Categories</h5>
                </div>
                <div class="ibox-content">
                    <div id='external-events'>
                        <ul class="category-list" style="padding: 10px">
                            <?php $fun4 = \App\ActivityCategories::all()->where('category_id',4)->count();
                            $fun1 = \App\ActivityCategories::all()->where('category_id',1)->count();
                            $fun2 = \App\ActivityCategories::all()->where('category_id',2)->count();
                            $fun3 = \App\ActivityCategories::all()->where('category_id',3)->count();
                            $fun5 = \App\ActivityCategories::all()->where('category_id',5)->count();
                            $fun6 = \App\ActivityCategories::all()->where('category_id',6)->count();
                            $fun7 = \App\ActivityCategories::all()->where('category_id',7)->count();
                            $fun8 = \App\ActivityCategories::all()->where('category_id',8)->count();
                            ?>
                            <li><a href="#"> <i style="color: #a62d19" class="fa fa-circle"></i> Fun & Adventeuer ({{$fun4}}) </a></li>
                            <li><a href="#"> <i style="color: #40b7ff" class="fa fa-circle"></i> Food & Drinks ({{$fun1}})</a></li>
                            <li><a href="#"> <i style="color: #23c6c8" class="fa fa-circle"></i> Firmenanlasse & Ausfluge ({{$fun3}})</a></li>
                            <li><a href="#"> <i style="color: #26e613" class="fa fa-circle"></i> Sport Activities ({{$fun7}})</a></li>
                            <li><a href="#"> <i style="color: #dce254" class="fa fa-circle"></i> Familly & Kids ({{$fun2}})</a></li>
                            <li><a href="#"> <i style="color: #9e3be1" class="fa fa-circle"></i> Culture ({{$fun5}})</a></li>
                            <li><a href="#"> <i style="color: #ff0000" class="fa fa-circle"></i> Romantik ({{$fun6}})</a></li>
                            <li><a href="#"> <i style="color: #f3a015" class="fa fa-circle"></i> Wellness & Beauty ({{$fun8}})</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #0e9aef; color: white"><b>Fullcalendar</b></div>
                <div class="panel-body">
                    {!! $calendar->calendar() !!}
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    {!! $calendar->script() !!}
@endsection