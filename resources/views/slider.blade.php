
<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#inSlider" data-slide-to="0" class="active"></li>
        <li data-target="#inSlider" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>slide 2 at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Slide 3 at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-head-color-box navy-bg p-lg text-center" style="background-image: url('assets/img/cc.jpg')">
                    <img src="assets/img/burj-al-arab.png" class="img-small-profile" alt="profile">
                    <button id="heart-favorite" class="btn btn-danger btn-circle btn-outline" type="button"><i class="fa fa-heart"></i></button>
                </div>

                <div class="widget-text-box">
                    <h3 class="media-heading m-t-md"><b>Kayak at Grand Canyon</b></h3>
                    <h5 class="media-heading m-t-md" style="text-align: justify">
                        Here is a description, some text about the event and more details... <br><br>
                    </h5>

                    <div class="client-avatar">
                        <row>
                            <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i></small>
                            <span class="m-l-xs">26/04/2018    </span>
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <img alt="image" src="assets/img/viewImage.jpg">
                            <span class="m-l-xs"> +50 attenders</span>
                        </row>
                    </div>
                    <br>  <div id="did-divider" class="divider"></div>
                    <div class="text-right" style="  padding-top: 10px;">
                        <small id="locaion-in-cards" class="icon" style="color: #cc5965"><i class="fa fa-map-marker"> Graubunden, Bern</i></small>
                        <button type="button" class="btn btn-danger btn-xs"><strong>ATTEND</strong></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>