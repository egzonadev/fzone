@extends('layout.master')
@section('page-content')
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"></script>
    <div style="background-color: white">
        @foreach($events as $event)
            <div class="events-background-img text-center" style="width: 100%;text-align: center;">
                <div class="" style="">
                    <div class="ibox-content2 " style="margin-left: 30%; margin-right: 30%; margin-top: 2%">
                        <h2 align="left"><b>{{$event->title}}</b></h2>
                        <p style="text-align: justify;">
                            {{$event->description}}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row shadow-lg p-3 mb-5 bg-white rounded">
                <div class="col-lg-4 text-center" id="user-profile-pic">
                    <div class="text-center">
                        <a href="{{action('UsersController@profile', $event['GetUser']->id)}}">
                            @if($event['GetUser']->usertype == 'Individual')
                            <img src="{{asset('/storage/profile/'.$event['GetUser']->image)}}" class="img-circle img-md"/>
                            @else
                            <img src="/storage/business/logo/{{$event['getBusiness']->path_logo_image}}" class="img-circle img-md"/>
                            @endif
                            <h4><b>{{$event['GetUser']->name}}</b></h4><br>
                        </a>
                    </div>
                </div>

                <div class="col-lg-4 col-lg-offset-2 pull-right" style="margin-top: 5px;">
                    @if( !\App\EventReports::reported($event['id']))
                        <a class="btn btn-outline btn-danger" style="height: 35px; margin-left: 10%" title="Report" data-toggle="modal"
                           data-target="#modal-report-{{$event['id']}}">
                            <i class="fa fa-warning"></i>
                        </a>
                    @elseif( \App\EventReports::reported($event['id']))
                        <a class="btn btn-danger" style="height: 35px;  margin-left: 10%" title="Report" data-toggle="modal"
                           data-target="#modal-reported">
                            <i class="fa fa-warning"></i>
                        </a>
                    @endif
                    <div class="modal fade" id="modal-reported">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header  text-center">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <div class="icon-box" style="font-size: 80px; color: red;">
                                        <i class="text-red fa fa-warning"></i>
                                    </div>
                                    <h3>You have already made a report for this business.</h3>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modal-report-{{$event['id']}}">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header  text-center">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <div class="icon-box" style="font-size: 80px">
                                        <i class="text-red fa fa-close"></i>
                                    </div>
                                    <h4 class="modal-title"><b>Choose report status if you want to proceed with this
                                            report.</b></h4>
                                </div>

                                <form action="{{action('UsersController@eventreport', $event->id)}}" method="post">
                                    {{csrf_field()}}
                                    <div class="modal-body col-lg-offset-1">
                                        <h2><strong>
                                                <select name="report_status" id="report_status">
                                                    <option value="Fake Event">Fake Event</option>
                                                    <option value="Offensive">Offensive</option>
                                                </select>
                                            </strong>
                                        </h2>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                                            Cancel
                                        </button>

                                        <button type="submit" class="btn btn-danger" style="height: 35px"
                                                title="Report">
                                            <i class="fa fa-warning"> Report</i>
                                        </button>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <button type="button" class="btn btn-outline btn-danger" style="height: 35px" title="Share">
                        <i class="fa fa-share-alt"></i>
                    </button>
                    {{--Favorite button--}}
                    @if( !App\ActivityInterested::isFollowingActivity($event->id))
                        <form method="GET" action="{{action('ActivitiesController@addInterested', $event->id)}}">
                            <button style="height: 35px; margin-top: -35px; margin-right: 20px" class="btn btn-danger btn-outline" type="submit">
                                <i class="fa fa-heart"></i>
                            </button>
                        </form>
                    @else
                        <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                        <form method="GET" action="{{action('ActivitiesController@addNotInterested', $event->id)}}">
                        {{csrf_field()}}
                            <button style="height: 35px; margin-top: -35px; margin-right: 20px" class="btn btn-danger" type="submit">
                                <i class="fa fa-heart"></i>
                            </button>
                        </form>
                    @endif

                    {{--Attending button--}}
                    @if( ! App\ActivityAttender::isAttendingActivity($event->id))
                         <?php $attend = 'JOIN THIS EVENTS' ?>
                         <form method="GET" action="{{action('ActivitiesController@Attend', $event->id)}}">
                             <button type="submit" class="btn btn-danger col-lg-offset-4" style="height: 35px; margin-top: -48px;">
                                 <i class="fa fa-plus-circle"></i>
                                 {{$attend}}
                             </button>
                         </form>
                    @else
                         <?php $attend = 'Attending' ?>
                         <form method="GET" action="{{action('ActivitiesController@RemoveAttend', $event->id)}}">
                                {{csrf_field()}}
                             <button type="submit" class="btn btn-danger col-lg-offset-4" style="height: 35px; margin-top: -48px;">
                                 <i class="fa fa-plus-circle"></i>
                                 {{$attend}}
                             </button>
                         </form>
                    @endif
                </div>

                <div class="col-lg-offset-1 col-md-offset-1 col-xs-offset-1" style="padding-top: 12%">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="col-lg-12">
                                <div class="col-lg-2 col-md-4 col-xs-6  forum-info">
                                    <h3 class="pull-left"><b>
                                            Date
                                        </b></h3>
                                    <h4>
                                        {{Carbon\Carbon::parse($event['start_date_time'])->format('d/m/Y')}}
                                    </h4>
                                </div>
                                <div class="col-lg-2 col-md-4 col-xs-6 forum-info">
                                    <h3><b>
                                            Time
                                        </b></h3>
                                    <h4>
                                        {{Carbon\Carbon::parse($event['start_date_time'])->format('h:m')}}
                                    </h4>
                                </div>
                                <div class="col-md-4 col-md-6 col-xs-8 ">
                                    <h3><b>Time Left</b></h3>
                                    <div class="row">
                                        <div class="img-circle-dhm-left col-lg-2 col-md-4 col-xs-6" style="margin-left: 5%">
                                            <div>
                                                <?php
                                                $now  = \Carbon\Carbon::now();
                                                $end  = \Carbon\Carbon::parse($event['start_date_time']);
                                                $left = $now->diffInDays($end);

                                                $event_date = $event['start_date_time'];
                                                $date=strtotime($event_date);
                                                $diff=$date-time();//time returns current time in seconds
                                                $hours=round(($diff-$left*60*60*24)/(60*60));
                                                ?>
                                                <h3 align="center"><b>{{$left}}</b></h3>
                                                <h5 style="margin-top: -50%; margin-left: -50%">DAYS</h5>
                                            </div>
                                        </div>
                                        <div class="img-circle-dhm-left col-lg-2 col-md-4 col-xs-6" style="margin-left: 5%">
                                            <div>
                                                <h3 align="center"><b>{{$hours}}</b></h3>
                                                <h5 style="margin-top: -50%; margin-left: -50%">HOUR</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h2><b>Event Info</b></h2>
                                <div class="col-lg-6">
                                    <p class="fa fa-map-marker cfbus">
                                        <small class="cfbust">
                                            {{$event->location}}, {{$event->city}}
                                        </small>
                                    </p>
                                    <p class="fa fa-phone cfbus">
                                        <small class="cfbust">{{$event['GetUser']->phone}}</small>
                                    </p>
                                </div>
                                <div class="col-lg-6">
                                    <p class="fa fa-envelope cfbus">
                                        <small class="cfbust">{{$event['GetUser']->email}}</small>
                                    </p>
                                    <p class="fa fa-globe cfbus">
                                        <small class="cfbust">www.info.org{{-- {{$business->website }}--}}</small>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="ibox-content">
                                    <div id="map" style="height:300px;width:100%;"></div>
                                    <input type="hidden" value="{{$event->location}}" id="location">
                                    <script>
                                        function initMap(address) {
                                            var geocoder = new google.maps.Geocoder();
                                            geocoder.geocode( { 'address':'{{$event->location}}, {{$event->city}}'}, function(results, status) {

                                                if (status == google.maps.GeocoderStatus.OK) {
                                                    var latitude = results[0].geometry.location.lat();
                                                    var longitude = results[0].geometry.location.lng();
                                                }

                                                console.log(latitude);
                                                console.log(longitude);

                                                var myLatLng = { lat: latitude, lng: longitude };

                                                var map = new google.maps.Map(document.getElementById('map'), {
                                                    zoom: 18,
                                                    center: myLatLng
                                                });

                                                var marker = new google.maps.Marker({
                                                    position: myLatLng,
                                                    map: map,
                                                    title: '{{$event->location}}, {{$event->city}}',
                                                    // draggable : true
                                                });

                                                var geocoder = new google.maps.Geocoder;
                                                var infowindow = new google.maps.InfoWindow;

                                                marker.addListener('click', function() {
                                                    map.setZoom(8);
                                                    map.setCenter(marker.getPosition());
                                                });
                                            });
                                        }


                                        function getLocation() {
                                            if (navigator.geolocation) {
                                                navigator.geolocation.getCurrentPosition(showPosition);
                                            } else {
                                                console.log( "Geolocation is not supported by this browser.");
                                            }
                                        }

                                        function showPosition(position) {
                                            var latitude = position.coords.latitude;
                                            var longitude = position.coords.longitude;
                                            initMap('', latitude, longitude);
                                        }
                                    </script>
                                    <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiEWBVUo0RaQ4qZJtn-isVV-DdbWD51y8&callback=initMap">
                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-offset-1 col-lg-4">
                            <div class="client-avatar2">
                                <row>
                                    <h2>Going</h2><br>
                                    <?php $attenders = App\Activity::getAttenders($event->id);
                                    $count_attenders = App\Activity::GetAttendersCount($event->id);
                                    ?>
                                    @foreach($attenders as $attender)
                                        @if($attender->usertype == 'Individual')
                                            <img src="{{asset('/storage/profile/'.$attender->image)}}"/>
                                        @else
                                            <img src="/assets/img/user.jpg"/>
                                        @endif
                                    @endforeach
                                    <span class="m-l-xs"> + {{$count_attenders}} attenders</span>
                                </row>
                            </div>
                            <div class="client-avatar2">
                                <row>
                                    <h2>Favorite</h2><br>
                                    <?php $followers = App\Activity::getFollowers($event->id);
                                    $count_followers = App\Activity::countFollowers($event->id);
                                    ?>
                                    @foreach($followers as $follower)
                                        @if($follower->usertype == 'Individual')
                                            <img src="{{asset('/storage/profile/'.$follower->image)}}"/>
                                        @else
                                            <img src="/assets/img/user.jpg"/>
                                        @endif
                                    @endforeach

                                    <span class="m-l-xs"> + {{$count_followers}} followers</span>
                                </row>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5%; padding-bottom: 2%">
                        <h2 style="margin-left: 40%">Similar Events</h2>
                        <div class="item col-lg-11">
                            <?php $activities = \App\Activity::where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))->take(3)->latest()->get(); ?>
                            @foreach($activities as $event)
                                <div class="col-lg-4 col-md-4 col-xs-4">
                                    <div class="widget-head-color-box navy-bg p-lg text-center"
                                         style="background-image: url('{{asset('/images/categories/'.$event->category->first()->image)}}')">
                                        @if($event['GetUser']->usertype == 'Individual')
                                            <img src="{{asset('/storage/profile/'.$event['GetUser']->image)}}" class="img-small-profile"/>
                                        @else
                                            <img src="/storage/business/logo/{{$event['getBusiness']->path_logo_image}}" class="img-small-profile"/>
                                        @endif
                                        @if( ! App\ActivityInterested::isFollowingActivity($event->id))
                                            <?php $heart_style = '' ?>
                                            <form method="GET"
                                                  action="{{action('ActivitiesController@addInterested', $event->id)}}"
                                                  class="form-horizontal">
                                                @else
                                                    <?php $heart_style = 'color:white;background-color:#ec4758' ?>
                                                    <form method="GET"
                                                          action="{{action('ActivitiesController@addNotInterested', $event->id)}}"
                                                          class="form-horizontal">
                                                        @endif
                                                        {{csrf_field()}}
                                                        <button style="{{$heart_style}}" id="heart-favorite"
                                                                class="btn btn-danger btn-circle btn-outline"
                                                                type="submit">
                                                            <i class="fa fa-heart"></i>
                                                        </button>
                                                    </form>
                                    </div>
                                    <div class="widget-text-box">
                                        <a href="{{action('EventsController@index', $event->id)}}">
                                            <h3 href="" class="media-heading m-t-md"><b>{{$event->title}}</b></h3>
                                        </a>
                                        <h5 class="media-heading m-t-md" style="max-width: 270px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden">
                                            {{$event->description}}<br><br>
                                        </h5>
                                        <div class="client-avatar">
                                            <row>
                                                <small class="icon" style="color: red"><i class="fa fa-calendar" style="font-size: 15px"></i>
                                                </small>
                                                <span class="m-l-xs">{{ date("d/m/Y", strtotime( $event->start_date_time ) ) }}  </span>
                                                <?php
                                                $attenders = App\Activity::GetAllActivityAttenders($event->id);
                                                $count_attenders = App\Activity::GetAttendersCount($event->id);
                                                ?>
                                                @foreach($attenders as $attender)
                                                    @if($attender->usertype == 'Individual')
                                                        <img src="{{asset('/storage/profile/'.$attender->image)}}"/>
                                                    @else
                                                        <img src="/assets/img/user.jpg"/>
                                                    @endif
                                                @endforeach
                                                <span class="m-l-xs"> + {{$count_attenders}} attenders</span>
                                            </row>
                                        </div>
                                        <br>
                                        <div id="did-divider" class="divider"></div>
                                        <div class="text-right" style="  padding-top: 10px;">
                                            <small id="locaion-in-cards" class="icon" style="color: #cc5965">
                                                <i class="fa fa-map-marker">
                                                    {{$event->location . ', ' . $event->city }}</i></small>
                                            @if( ! App\ActivityAttender::isAttendingActivity($event->id))
                                                <?php $attending = 'Attend' ?>
                                                <form method="GET"
                                                      action="{{action('ActivitiesController@Attend', $event->id)}}"
                                                      class="form-horizontal">
                                                    @else
                                                        <?php $attending = 'Attending' ?>
                                                        <form method="GET"
                                                              action="{{action('ActivitiesController@RemoveAttend', $event->id)}}"
                                                              class="form-horizontal">
                                                            @endif
                                                            {{csrf_field()}}
                                                            <button type="submit" class="btn btn-danger btn-xs">
                                                                <strong>{{$attending}}</strong>
                                                            </button>
                                                        </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
@stop
