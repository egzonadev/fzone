<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>


    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <link rel="stylesheet" href="/assets/css/datepicker3.css">
    <link rel="apple-touch-icon" sizes="76x76" href="/wizard/assets/img/apple-icon.png"/>
    <link rel="icon" type="image/png" href="/wizard/assets/img/favicon.png"/>


    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>

    <!-- CSS Files -->
    <link href="/wizard/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/wizard/assets/css/material-bootstrap-wizard.css" rel="stylesheet"/>

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/wizard/assets/css/demo.css" rel="stylesheet"/>
</head>

<body>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <!--      Wizard container        -->
            <div class="wizard-container">
                <div class="card wizard-card" data-color="red" id="wizardProfile">
                    <form action="" method="">
                        <!--        You can switch " data-color="purple" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
                        <div class="wizard-header">
                            <h3 class="wizard-title">
                                ADD ACTIVITY
                            </h3>
                        </div>
                        <div class="wizard-navigation">
                            <ul>
                                <li><a href="#step1" data-toggle="tab">1</a></li>
                                <li><a href="#step2" data-toggle="tab">2</a></li>
                               {{-- <li><a href="#step3" data-toggle="tab">3</a></li>--}}
                                <li><a href="#step4" data-toggle="tab">4</a></li>
                            </ul>
                        </div>
    
                        <div class="tab-content">
                            <div class="tab-pane" id="step1">
                                <div class="row">
                                    <div class="col-sm-12"></div>
                                    <div class="tab-pane" id="description">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-1">
                                                <div class="input-group">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Title Here</label>
                                                        <input name="name" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12"></div>
                                    <div class="tab-pane" id="description">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-1">
                                                <div class="form-group">
                                                    <label>Subject Description</label>
                                                    <textarea class="form-control" placeholder="" rows="6"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Example</label>
                                                    <h2 class="description">"If you like hiking, this is the best chance
                                                        to explore new places with new group of people from around the
                                                        world, JOIN US"</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    
                                <h4 class="info-text"> Select Category (checkboxes) </h4>
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="jobb" value="Design" )>
                                                <div class="icon">
                                                    <i class="fa fa-free-code-camp"></i>
                                                </div>
                                                <h6>Fun & Abenteur</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="jobb" value="Code">
                                                <div class="icon">
                                                    <i class="fa fa-ravelry"></i>
                                                </div>
                                                <h6>Kultur</h6>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="choice" data-toggle="wizard-checkbox">
                                                <input type="checkbox" name="jobb" value="Develop">
                                                <div class="icon">
                                                    <i class="fa fa-soccer-ball-o"></i>
                                                </div>
                                                <h6>Sport</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step2">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="form-group" id="data_1">
                                                    <label class="font-normal">Start Date</label>
                                                    <div class="input-group date">
                                                        <input type="date" class="form-control">
                                                    </div>
                                                </div>
    
                                                <div class="form-group" id="data_1">
                                                    <label class="font-normal">End Date</label>
                                                    <div class="input-group date">
                                                        <input type="date" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <div class="form-group" id="data_1">
                                                    <label class="font-normal">Start Time</label>
                                                    <div class="input-group date">
                                                        <input type="time" class="form-control">
                                                    </div>
                                                </div>
    
                                                <div class="form-group" id="data_1">
                                                    <label class="font-normal">End Time</label>
                                                    <div class="input-group date">
                                                        <input type="time" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4"><br>
                                            <div class="input-group">
                                                <div class="form-group label-floating">
                                                    <input name="location" type="text" placeholder="Location..."
                                                            class="form-control">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="input-group">
                                                <div class="form-group label-floating">
                                                    <input name="city" type="text" placeholder="City/Zip..."
                                                            class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div id="map" style="width:100%;height:200px;"></div>
    
                                            <script>
                                                function initMap() {
                                                    var myLatLng = {lat: 42.667542, lng: 21.166191};
    
                                                    var map = new google.maps.Map(document.getElementById('map'), {
                                                        zoom: 16,
                                                        center: myLatLng
                                                    });
    
                                                    var marker = new google.maps.Marker({
                                                        position: myLatLng,
                                                        map: map,
                                                        title: 'You Are Here.!'
                                                    });
                                                }
                                            </script>
    
                                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiEWBVUo0RaQ4qZJtn-isVV-DdbWD51y8&callback=initMap"></script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           {{-- <div class="tab-pane" id="step3">
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Choose Your Business
                                                <small>(required)</small>
                                            </label>
                                            <select class="form-control">
                                                <option disabled="" selected=""></option>
                                                <option value="Afghanistan"> Individual</option>
                                                <option value="Albania"> Business</option>
                                                <option value="...">...</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-sm-offset-1">
                                        <b>Contact Information</b>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes">
                                            </label> Use my account email address
                                        </div>
                                        <br>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes">
                                            </label> Use my account phone number
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="input-group">
                                            <div class="form-group label-floating">
                                                <input name="location" type="text"
                                                        placeholder="+ Add more emails..."
                                                        class="form-control col-lg-6">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <div class="form-group label-floating">
                                                <input name="location" type="text"
                                                        placeholder="+ Add more phone numbers..."
                                                        class="form-control">
                                            </div>
                                        </div>
                                        <small>Seperate emails with comma (hr@gmail.com, info@gmail.com)</small>
                                        <small>Seperate phone numbers with comma (12 12 12, 55 55 55)</small>
                                    </div>
    
                                </div>
                            </div>--}}
                            <div class="tab-pane" id="step4">
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <h6 class="control-label"><b>Invite Friends</b></h6>
                                        <div class="form-group label-floating">
                                            <label class="control-label">A specific name or email...</label>
                                            <input name="name" type="text" class="form-control">
                                            <small>Seperate people with comma (Adam, John) or emails (hr@gmail.com,info@gmail.com)
                                        </div>
    
                                        </small>
    
                                    </div>
                                    <br>
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <b></b>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes">
                                            </label> Invite all men
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes">
                                            </label> Invite all women
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes">
                                            </label> Invite both
                                        </div>
                                        <label class="col-sm-offset-6">Skip Invitation</label>
                                    </div>
    
                                </div>
                            </div>
                        </div>
                        <div class="wizard-footer">
                            <div class="pull-right">
                                <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next'
                                        value='Continue'/>
                                <input type='button' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish'
                                        value='Finish'/>
                            </div>
    
                            <div class="pull-left">
                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd'
                                        name='previous' value='Previous'/>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div> <!-- wizard container -->
        </div>
    </div><!-- end row --> 

</div>


<!--   Core JS Files   -->
<script src="/wizard/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="/wizard/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/wizard/assets/js/jquery.bootstrap.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="/wizard/assets/js/material-bootstrap-wizard.js"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="/wizard/assets/js/jquery.validate.min.js"></script>

</body>
</html>




{{-- This is the reference for save and edit data  --}}
{{-- https://appdividend.com/2017/08/20/laravel-5-5-tutorial-example/ --}}
<h2>Create New Activity</h2>
@include('includes.messages')
<form method="post" action="{{action('ActicitiesController@store')}}">
    {{csrf_field()}}

    <div id="div_1">
        <div class="row" id="1">
            <div class="col-md-12">  
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="title">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"> 
                <label for="price">Description:</label>
                <input type="text" class="form-control" name="description"> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul>
                    @foreach($all_categories as $category)
                        <li><a href="#" id="{{$category->id}}" onclick="AddCategory('{{$category->id}}')">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
            <script>
                function AddCategory(category){ 
                    if($('#categories').val().includes(category)){
                        // if category is already added then remove it
                        console.log('exists');
                    }else{ 
                        $('#categories').val(category + ':' + $('#categories').val() );
                    } 
                }
            </script>
            <input type="hidden" name="categories" value="" id="categories" />
        </div> 
    </div>


    {{-- // Part 2  --}} 
    <div id="div_2" style="display:none"> 
        <div class="row">
            <div class="col-lg-4">
                <label for="name">Start Date:</label>
                <input type="text" class="form-control" name="start_date" id="start_date">
            </div>
            <div class="col-lg-2">
                <label for="name">Start Time:</label>
                <input type="text" class="form-control" name="start_time" id="start_time">
            </div>
            <div class="col-lg-6">
                <label for="name">Location:</label>
                <input type="text" class="form-control" name="location" id="location", placeholder="Street House No, Post Code">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <label for="name">End Date:</label>
                <input type="text" class="form-control" name="end_date" id="end_date">
            </div>
            <div class="col-lg-2">
                <label for="name">End Time:</label>
                <input type="text" class="form-control" name="end_time" id="end_time">
            </div>
            <div class="col-lg-6">
                <label for="name">City:</label>
                <input type="text" class="form-control" name="city" id="city" placeholder="City">
            </div>
        </div>

        <script> 
            $('#start_date').datetimepicker({ 
               format: 'YYYY-MM-DD'
            }); 
            $('#end_date').datetimepicker({ 
                format: 'YYYY-MM-DD'
            });
            $('#start_time').datetimepicker({
                format: 'HH:mm'
            }); 
            $('#end_time').datetimepicker({
                format: 'HH:mm'
            }); 
        </script>
        <br /> <br />
        <div class="col-lg-12" style="padding-left:5%;padding-right:5%;">
            <div id="map" style="height:300px;width:100%;"></div>

            <script>
                // function for geting geo location
                $('#city').on('keypress', function() {
                    console.log($(this).val());
                    initMap( $('#location').val() + ' '+ $(this).val());
                });

                function initMap(address) {

                    var geocoder = new google.maps.Geocoder();

                    if (address == null ) { address = 'Prishtina Kosovo' }

                    geocoder.geocode( { 'address': address }, function(results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            var latitude = results[0].geometry.location.lat();
                            var longitude = results[0].geometry.location.lng();
                        }

                        console.log(latitude);
                        console.log(longitude);

                        var myLatLng = { lat: latitude, lng: longitude };

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 18,
                            center: myLatLng
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            title: 'Hello World!',
                            // draggable : true
                        });

                        var geocoder = new google.maps.Geocoder;
                        var infowindow = new google.maps.InfoWindow;

                        marker.addListener('click', function() {
                            map.setZoom(8);
                            map.setCenter(marker.getPosition());
                        });
                    });
                }


                function getLocation() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition);
                    } else {
                        console.log( "Geolocation is not supported by this browser.");
                    }
                }

                function showPosition(position) {
                    var latitude = position.coords.latitude;
                    var longitude = position.coords.longitude;
                    initMap('', latitude, longitude);
                }
            </script>
            <script async defer
                 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiEWBVUo0RaQ4qZJtn-isVV-DdbWD51y8&callback=initMap">
            </script>
        </div>
    </div> 
    {{-- // End of Part 2 --}} 

    <div id="div_3" style="display:none"> 
        <div class="row">
            <div class="col-lg-12">
                <label for="name">Your Business:</label>
                <div class="col-lg-12">
                    <select name="business" style="width:100%;" class="btn btn-info">
                        <option value="something">Select Category</option>
                        @foreach($businesses as $key => $business)
                            <option value="{{$key}}" class="btn btn-warning">{{$business}}</option>
                        @endforeach
                    </select>
                </div> 
            </div> 
        </div>
        <br />
        <div class="row">
            <h5>Contact Information</h5>
            <div class="col-lg-6">
                <input type="checkbox" name="use_account_email" value="Use My Account" checked> Use My Email Account
                <br />
                <input type="checkbox"  name="use_account_phone" value="Use My Phone" checked> Use my Phone
            </div>
            <div class="col-lg-6">
                <input type="text" name="extra_emails" class="form-control" placeholder="Add More Emails">
                <br />
                <input type="text" name="more_phone" class="form-control" placeholder="Add More Phone Numbers">
            </div>
        </div> 
    </div>

    <div id="div_4" style="display:none"> 
        <h3>Finish</h3>
        <div class="row">
            <div class="col-lg-4">
                <label for="name">Invite Specific Friend : </label>
                <input type="text" class="form-control" name="specific_email_invite">
            </div>
        </div>
    </div>

    <div class="row" style="display:none;" id="btnSubmit">
        <div class="col-md-12">
            <button type="submit" class="btn btn-success" style="margin-left:38px">FINISH</button> 
        </div>
    </div>

    <div class="row">
        <a id="btnContinue" class="btn btn-success">Continuue</a>
        <input type="hidden" id="next_hidden_div" value="1" />
        <script>
            $('#btnContinue').click(function() {

                $('#div_' + $('#next_hidden_div').val() ).hide('slow'); 
                $('#div_' + (parseInt($('#next_hidden_div').val()) + 1 ) ).show('slow'); 

                $('#next_hidden_div').val( parseInt($('#next_hidden_div').val()) + 1 );

                if( parseInt($('#next_hidden_div').val()) == 4 ){
                    $('#btnSubmit').show();
                    $('#btnContinue').hide();
                }
            });
        </script>
    </div>

</form> 
   