<?php $top_b = \App\Business::where('created_at', '>=',\Carbon\Carbon::now()->subMonth())
    ->where('business_type', 'Top')->get();
$i = 0;
//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 2;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
?>
@if( count($top_b ) > 0)
    <div>
        <h1 class="category-names">
            Top Events
        </h1>
    </div>
    <div class="landing-page">
        <div id="top" class="carousel carousel-fade" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    @foreach($top_b as $business)
                        <div class="col-lg-6 col-md-6 col-xs-6">
                            <div class="widget-head-color-box navy-bg p-lg text-center" data-animation="pulse"
                                 style="background-image: url('/storage/business/cover/{{$business->path_cover_image}}');">
                                <img src="/storage/business/logo/{{$business->path_logo_image}}"
                                     class="img-circle circle-border m-b-md" alt="profile">
                            </div>
                            <div class="widget-text-box text-center">
                                <a href="{{action('BusinessesController@details', $business->id)}}">
                                    <h2 class="media-heading m-t-md"><b>{{$business->name}}</b></h2></a>
                                <h4>{{$business->category}}</h4>
                                <p style="color: red">
                                    @foreach($avg_top_businesses as $top_avg)
                                        @if($top_avg->business_id == $business->id)
                                            @for ($i = 0; $i < round($top_avg->average, 0); $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                            @for ($i = round($top_avg->average, 0); $i < 5; $i++)
                                                <i class="fa fa-star-o"></i>
                                            @endfor
                                        @endif
                                    @endforeach
                                </p>
                                <?php
                                if ((date('H:i', strtotime('+2 hour')) > date('H:i', strtotime($business->closing_hours)))
                                    ||
                                    (date('H:i', strtotime('+2 hour')) < date('H:i', strtotime($business->opening_hours)))
                                ) {
                                    $openStatus = 'Closed Now';
                                } else {
                                    $openStatus = 'Open Now';
                                }
                                ?>
                                <h4 style="color: red">{{ $openStatus  }} </h4>
                                <a href="{{action('ActivitiesController@new',$business->id)}}"
                                   class="btn btn-danger col-lg-4 col-lg-offset-4">CREATE ACTIVITY </a>
                                <br>
                            </div>
                        </div>
                        <?php $rowCount++; ?>
                    @endforeach
                        <?php if ($rowCount % $numOfCols == 0) echo '</div><div class="item">';?>

                </div>
                <ol class="carousel-indicators text-center">
                    <li data-target="#top" data-slide-to="0" class="active"></li>
                    @foreach($top_b as $business)
                        <?php $i++; ?>
                        <?php if ($i % $numOfCols == 0) echo '<li data-target="#top" data-slide-to="1"></li>';?>
                    @endforeach
                </ol>
                <a class="left carousel-control" href="#top" role="button" data-slide="prev">
            <span aria-hidden="true" style="color: #cc5965">
                <img src="assets/img/icon/back-arrow.png" alt=""></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a id="right-left-arrow" class="right carousel-control" href="#top" role="button"
                   data-slide="next">
                    <span aria-hidden="true" style="color: #cc5965"><img src="assets/img/icon/right-arrow.png"
                                                                         alt=""></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
@endif