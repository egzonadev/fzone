
@extends('layout.master')

@section('main_content') 
 
    <h2>Create New Activity</h2>

    @include('includes.messages')

    <form method="post" action="{{action('ActicitiesController@store', $id)}}">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-12">  
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="title">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"> 
                <label for="price">Price:</label>
                <input type="text" class="form-control" name="description"> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Add Activity</button> 
        </div>
    </form>  
@endsection