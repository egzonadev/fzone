 
<link rel="apple-touch-icon" sizes="76x76" href="/register/assets/img/apple-icon.png" />
<link rel="icon" type="image/png" href="/register/assets/img/favicon.png" />
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
<!-- CSS Files -->
<link href="/wizard/assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="/wizard/assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

<script src="/wizard/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="/wizard/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/wizard/assets/js/jquery.bootstrap.js" type="text/javascript"></script>
<!--  Plugin for the Wizard -->
<script src="/wizard/assets/js/material-bootstrap-wizard.js"></script>
<script src="/wizard/assets/js/jquery.validate.min.js"></script>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2"> 
        <!--      Wizard container        -->
        <div class="wizard-container">
            @include('/includes.messages')
            <div class="card wizard-card" data-color="red" id="wizardProfile"> 
                    <form method="post" action="{{action('ActivitiesController@store')}}">
                        {{csrf_field()}}
                    <div class="wizard-header">
                        <h3 class="wizard-title">
                            ADD ACTIVITY
                        </h3>
                    </div>
                    <div class="wizard-navigation">
                        <ul>
                            <li><a href="#step1" data-toggle="tab">1</a></li>
                            <li><a href="#step2" data-toggle="tab">2</a></li>
                            {{--<li><a href="#step3" data-toggle="tab">3</a></li>--}}
                            <li><a href="#step4" data-toggle="tab">3</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">
                        
                        @include('/activity.includes.step_1')
                        @include('/activity.includes.step_2')
                        {{--@include('/activity.includes.step_3')--}}
                        @include('/activity.includes.step_4')
 
                    </div>
                    <div class="wizard-footer">
                        <div class="pull-right">
                            <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next'
                                    value='Continue'/>
                            <input type="submit" class='btn btn-finish btn-fill btn-danger btn-wd' name='finish'
                                    id="btnFinish" value='Finish'/>
                        </div>

                        <div class="pull-left">
                            <input type='button' class='btn btn-previous btn-fill btn-default btn-wd'
                                    name='previous' value='Previous'/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div> <!-- wizard container -->
    </div>
</div><!-- end row -->  
 
<br />
<br />