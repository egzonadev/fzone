<div class="tab-pane" id="step1">
    <div class="row">
        <div class="col-sm-12"></div>
        <div class="tab-pane" id="description">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                        <?php $businesses = \App\Business::all();
                        $selected = \App\Business::where('id',$business_id)->get();
                        ?>
                        <label class="control-label">Choose Your Business
                            <small>(required)</small>
                        </label>
                        <select class="form-control" name="business">
                            <option disabled="" selected=""></option>
                                @if($business_id != null)
                                @foreach( $selected as $business)
                                    <option value="{{$business->id}}" selected>{{$business->name}}</option>
                                @endforeach
                                @else
                                @foreach( $businesses as $business)
                                    <option value="{{$business->id}}">{{$business->name}}</option>
                                @endforeach
                                @endif
                        </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12"></div>
        <div class="tab-pane" id="description">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1">
                    <div class="input-group">
                        <div class="form-group label-floating">
                            <label class="control-label">Title Here
                                <small>(required)</small>
                            </label>
                            <input name="title" type="text" class="form-control" required autofocus>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12"></div>
        <div class="tab-pane" id="description">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-1">
                    <div class="form-group">
                        <label>Subject Description
                            <small>(required)</small>
                        </label>
                        <textarea name="description" class="form-control" placeholder="" rows="6" required
                                  autofocus></textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Example</label>
                        <h2 class="description">"If you like hiking, this is the best chance
                            to explore new places with new group of people from around the
                            world, JOIN US"</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h4 class="info-text"> Select Category (checkboxes) </h4>
    <?php //Including all the categories here... ?>
    @include('/activity.includes.categories')

</div>