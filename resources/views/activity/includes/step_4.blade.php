
<div class="tab-pane" id="step4">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <h6 class="control-label"><b>Invite Friends outside Freitzeitzone:</b></h6>
                <div class="form-group label-floating">
                    <label class="control-label">A specific name or email...</label>
                    <input name="friends" type="text" class="form-control">
                    <small>Seperate people with comma (Adam, John) or emails (hr@gmail.com,info@gmail.com)</small>
                </div>
            </div>
            <br>
            <div class="col-sm-10 col-sm-offset-1">
                <b></b>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="invite_all_men" checked>
                    </label> All with same interests.
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="invite_all_women">
                    </label> Private, only with my friends.
                </div>
                <label class="col-sm-offset-6" id="btnSkipInvitation">Skip Invitation</label>
                <script>
                    $('#btnSkipInvitation').click(function(){ 
                        $( "#btnFinish" ).trigger( "click" );
                        $( "#btnFinish" ).attr('disabled','disabled'); 
                    });
                </script>

            </div>

        </div>
    </div>