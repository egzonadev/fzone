
<?php  
    use App\Business;  
    use App\Http\Controllers\UserController; 
    $business = new Business(); 
    if( UserController::isBusinessUser() ) { 
        $required_autofocus = 'required autofocus';
        $required = '(required)';  
    } else { $required_autofocus = ''; $required = '';}
?>

 <div class="tab-pane" id="step3">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="form-group label-floating">

                    @if( $business_id != null ) 
                        {{-- This is coming from business view --}}

                        @if( ! UserController::isBusinessUser() )
                        <?php $businesses = $business->getSelectedBusiness( $business_id ); ?>  
                        @else 
                        <?php $businesses = $business->getBusinessUserBusinesses(); ?> 
                        @endif

                        <label class="control-label">Choose Your Business
                            <small>{{$required}}</small>
                        </label> 

                        <select class="form-control" name="business" {{ $required_autofocus }} >
                            <option disabled="" selected=""></option>
                            @foreach( $businesses as $business)
                                @if($business_id != null && $business_id == $business->id) 
                                <option value="{{$business->id}}" selected>{{$business->name}}</option>
                                @else 
                                <option value="{{$business->id}}">{{$business->name}}</option>
                                @endif
                            @endforeach
                        </select>
                       
                    @else

                        @if( UserController::isBusinessUser() )

                        <?php $businesses = $business->getBusinessUserBusinesses(); ?> 

                        <label class="control-label">Choose Your Business
                                <small>{{$required}}</small>
                            </label> 
                            
                            <select class="form-control" name="business" {{ $required_autofocus }} >
                                <option disabled="" selected=""></option>
                                @foreach( $businesses as $business)
                                    @if($business_id != null && $business_id == $business->id) 
                                    <option value="{{$business->id}}" selected>{{$business->name}}</option>
                                    @else 
                                    <option value="{{$business->id}}">{{$business->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @endif

                    @endif 
                </div>
            </div>
            <div class="col-sm-4 col-sm-offset-1">
                <b>Contact Information</b>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="use_account_email" checked>
                    </label> Use my account email address
                </div>
                <br>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="use_account_phone" checked>
                    </label> Use my account phone number
                </div>
            </div>
            <div class="col-sm-6 ">
                <div class="input-group">
                    <div class="form-group label-floating">
                        <input name="extra_email" type="text" placeholder="+ Add more emails..." class="form-control col-lg-6">
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-group label-floating">
                        <input name="extra_phone" type="text" placeholder="+ Add more phone numbers..." class="form-control">
                    </div>
                </div>
                <small>Seperate emails with comma (hr@gmail.com, info@gmail.com)</small>
                <small>Seperate phone numbers with comma (12 12 12, 55 55 55)</small>
            </div>

        </div>
    </div>