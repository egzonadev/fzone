 
<div class="tab-pane" id="step2">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="form-group" id="data_1">
                            <label class="font-normal">Start Date</label>
                            <div class="input-group date">
                                <input type="date" class="form-control" name="start_date" required autofocus>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="font-normal">End Date</label>
                            <div class="input-group date">
                                <input type="date" class="form-control" name="end_date" required autofocus>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="form-group" id="data_1">
                            <label class="font-normal">Start Time</label>
                            <div class="input-group date">
                                <input type="time" class="form-control" name="start_time" required autofocus>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="font-normal">End Time</label>
                            <div class="input-group date">
                                <input type="time" class="form-control" name="end_time" required autofocus>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4"><br>
                    <div class="input-group">
                        <div class="form-group label-floating">
                            <input name="location" id="location" type="text" placeholder="Location..." class="form-control" required autofocus>
                        </div>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="form-group label-floating">
                            <input name="city" id="city" type="text" placeholder="City/Zip..." class="form-control" required autofocus>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div id="map" style="width:100%;height:200px;"></div>

                    <script>
                            // function for geting geo location
                            $('#city').on('keypress', function() {
                                console.log($(this).val());
                                initMap( $('#location').val() + ' '+ $(this).val());
                            });
                              
                            function initMap(address) {
                
                                var geocoder = new google.maps.Geocoder();
                                
                                if (address == null ) { address = 'Prishtina Kosovo' }
            
                                geocoder.geocode( { 'address': address }, function(results, status) {
                
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var latitude = results[0].geometry.location.lat();
                                        var longitude = results[0].geometry.location.lng();
                                    }  
                
                                    console.log(latitude);
                                    console.log(longitude);
                
                                    var myLatLng = { lat: latitude, lng: longitude };
            
                                    var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 18,
                                        center: myLatLng
                                    });
                
                                    var marker = new google.maps.Marker({
                                        position: myLatLng,
                                        map: map,
                                        title: 'Hello World!',
                                        // draggable : true
                                    });
                                    
                                    var geocoder = new google.maps.Geocoder;
                                    var infowindow = new google.maps.InfoWindow;
            
                                    marker.addListener('click', function() {
                                        map.setZoom(8);
                                        map.setCenter(marker.getPosition());  
                                    });  
                                });
                            } 
            
            
                            function getLocation() {
                                if (navigator.geolocation) {
                                    navigator.geolocation.getCurrentPosition(showPosition);
                                } else { 
                                    console.log( "Geolocation is not supported by this browser.");
                                }
                            }
            
                            function showPosition(position) {
                                var latitude = position.coords.latitude;
                                var longitude = position.coords.longitude;
                                initMap('', latitude, longitude);
                            }
                        </script>

                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiEWBVUo0RaQ4qZJtn-isVV-DdbWD51y8&callback=initMap"></script>
                </div>
            </div>
        </div>
    </div>