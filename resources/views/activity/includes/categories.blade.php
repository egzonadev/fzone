<?php
use App\Http\Controllers\CategoriesController;
$categories = CategoriesController::getAllCategories();
?>

<div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        @foreach($categories as $category)
            <div class="col-sm-4">
                <div class="choice" data-toggle="wizard-checkbox" id="category_{{$category->id}}">
                    <input type="checkbox" name="category_{{$category->id}}" id="category">
                    <div class="icon-square">
                        <img src="{{asset('/images/categories/'.$category->image)}}"
                             style="width: 106px; height:62px;">
                    </div>
                    <h6>{{$category->name}}</h6>
                </div>
            </div>
        @endforeach
        {{--<div class="col-sm-4">
            <div class="choice" data-toggle="wizard-checkbox">
                <div class="icon-square" id="select_all">
                    <i class="fa fa-check-circle"></i>
                </div>
                <h6>Alle Auswählen</h6>
                <input type="hidden" value="false" id="checked_all" >
            </div>
        </div>--}}
    </div>
</div>
<script>
    $('#select_all').click(function(){
        console.log('clicked');
        if( $('#checked_all').val() == 'false' ){

            $('#checked_all').val('true');
            $('div[id^=category_]').addClass('active');
            $('input[name^=category_]').attr('checked', true);

        }else{

            $('#checked_all').val('false');
            $('div[id^=category_]').removeClass('active');
            $('input[name^=category_]').attr('checked', false);
        }
    });
</script>