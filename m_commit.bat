@ECHO OFF
SETLOCAL
 
ECHO Commiting Changes to the repository

set /p Message="Enter Commit Message : "

git add .

git commit -m "%Message%"

git push -u origin Business-Dashboard-2.1

ENDLOCAL
 