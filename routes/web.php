<?php

Auth::routes();
// Registration Routes...
Route::get('register/{type}', 'Auth\RegisterController@showRegistrationForm');
Route::post('register/{type}', 'Auth\RegisterController@create');

Route::resources([
    '/' =>'HomeController',
    'activity' => 'ActivitiesController',
    'business' => 'BusinessesController',
    'user/profile' => 'UsersController',
    'search' => 'AdvancedSearch',
    'message' => 'MessagesController',
    'admin' => 'AdminController',
]);
Route::get('/adminn/users', 'AdminController@users');
Route::get('/adminn/businesses', 'AdminController@businesses');
Route::put('/adminn/businesses/{id}', 'AdminController@upgrade');
Route::get('/adminn/events', 'CalendarController@index');
Route::get('/adminn/statistics', 'AdminController@statistics');
Route::get('/adminn/profile',  'AdminController@edit');
Route::put('/adminn/profile', 'AdminController@update');
Route::get('/adminn/reports',  'AdminController@reports');
Route::delete('/adminn/reports/{id}', 'AdminController@destroy');
Route::delete('/adminn/reports/{id}', 'AdminController@destroyBusiness');
// Business User
//Route::get('/business/{id}', 'BusinessesController@index');
Route::post('/business/', 'BusinessesController@upgrade');
Route::get('/business/dashboard', 'BusinessesController@dashboard' );
//creating business
Route::get('/business/create', 'BusinessesController@create' );
Route::post('/business/create', 'BusinessesController@store' );
Route::get('/business/create/{type}', 'BusinessesController@createBusiness' );
Route::post('/business/create/{type}', 'BusinessesController@storePremium' );
//editing business
Route::get('/business/edit/{id}', 'BusinessesController@edit' );
Route::put('/business/edit/{id}', 'BusinessesController@update' );
Route::get('/business/events/{id}','BusinessesController@all_events');
Route::get('/business/notifications/{id}','BusinessesController@notification');
Route::get('/business/messages/{id}','BusinessesController@messages');

// Individual User to view business
Route::get('/business/{id}/details','BusinessesController@details');
Route::get('/business/{id}/events','BusinessesController@events');
Route::get('/business/{id}/reviews','BusinessesController@reviews');
Route::post('/business/{business_id}/contact_company','BusinessesController@contact_company');
Route::post('/business/{business_id}/add_review','BusinessesController@add_review');
// functions for like and dislike business... 
Route::post('/business/{business_id}/Like','BusinessesController@Like');
Route::post('/business/{business_id}/report','UsersController@report');
Route::post('/business/{business_id}/Dislike','BusinessesController@Dislike');

// INDIVIDUAL USER ACTIVITY FOR BUSINESS
Route::get('/activity/new/{business_id}','ActivitiesController@new');
Route::get('/activity/{activity_id}/addInterested','ActivitiesController@addInterested');
Route::get('/activity/{activity_id}/addNotInterested','ActivitiesController@addNotInterested');
Route::get('/activity/{activity_id}/Attend','ActivitiesController@Attend');
Route::get('/activity/{activity_id}/RemoveAttend','ActivitiesController@RemoveAttend');

Route::get('/activity/{activity_id}/OwnerInterested','ActivitiesController@OwnerInterested');
Route::get('/activity/{activity_id}/OwnerNotInterested','ActivitiesController@OwnerNotInterested');
Route::get('/activity/{activity_id}/OwnerAttend','ActivitiesController@OwnerAttend');
Route::get('/activity/{activity_id}/OwnerRemoveAttend','ActivitiesController@OwnerRemoveAttend'); 

// MESSAGES CONTROLLER FUNCTIONS
Route::post('/seng-message', array('as' => 'SendMessage', 'uses' => 'MessagesController@send'));
Route::post('/get-message', array('as' => 'GetMessages', 'uses' => 'MessagesController@getMsg'));
Route::post('/check-new-msg', array('as' => 'CheckNewMsg', 'uses' => 'MessagesController@check_for_new_messages'));
Route::post('/save-msg-read', array('as' => 'SaveMsgRead', 'uses' => 'MessagesController@save_msg_read'));


// ADVANCED SEARCH
Route::get('/searching','AdvancedSearch@search');
Route::post('/searching','AdvancedSearch@advanceSearch');
Route::get('/searchAll','AdvancedSearch@searchAll');
Route::get('/search','AdvancedSearch@searchTitle');

//Individual user profile
Route::get('/user/activities', 'UsersController@userActivities');
Route::get('/user/profile/edit/{id}', 'UsersController@edit');
Route::put('/user/profile', 'UsersController@update');
Route::get('/user/friends', 'UsersController@userfriends');
Route::get('/user/messages/{id}', 'UsersController@messages');

Route::get('/user/{id}', 'UsersController@profile');
Route::post('/user/{id}', 'UsersController@addFriends');
Route::get('/user/{id}/activities', 'UsersController@activities');
Route::get('/user/{id}/friends', 'UsersController@friends');
Route::get('/user/{id}/friends/accepted','UsersController@acceptFriend');
Route::get('/user/{id}/friends/ignore','UsersController@ignoreFriend');

//events in detail
Route::get('event/{id}', 'EventsController@index');
Route::post('event/{id}/report','UsersController@eventreport');

//business messages
Route::get('/messages', function () {
    return view('business.business_dashboard.messages');
});

//business reviews
Route::get('/reviews', function () {
    return view('business.business_view.reviews');
});

Route::get('/contact', function () {
    return view('other.contact');
});
Route::get('/faqs', function () {
    return view('other.faq');
});
Route::get('/work', function () {
    return view('other.work');
});
Route::get('/signup', function () {
    return view('layout.signup');
});

//calendar
Route::get('events', 'CalendarController@index');
Route::get('/dash-donought1', 'AdminController@dashboard_g1');
Route::get('/dash-donought2', 'AdminController@dashboard_g2');
Route::get('/multiStat', 'AdminController@multiStat');
Route::get('/previous', 'AdminController@previous');

