<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInterests extends Model
{
    protected $table = 'user_interest';

    public function addUserInterest($user_id, $interest_id ){
        $user_interest= new UserInterests();
        $user_interest->user_id = $user_id;
        $user_interest->interest_id = $interest_id;
        $user_interest->save();
    }

    public function user(){

        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function interest(){

        return $this->hasOne('App\Interests', 'id', 'interest_id');
    }
}
