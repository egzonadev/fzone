<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\DB;

class BusinessReviews extends Model
{
      //
      protected $table = "business_reviews";

      public function ReviewsBusiness($request, $business_id){
          
          $business_reviews = new BusinessReviews();
          
          $business_reviews->business_id = $business_id;
  
          if(Auth::check()){
              $business_reviews->name = Auth::user()->name;
              $business_reviews->email = Auth::user()->email;
          } else {
              $business_reviews->name = $request->input('name');
              $business_reviews->email = $request->input('email');
          } 
          $business_reviews->rating = $request->input('rating');
          $business_reviews->message = $request->input('message');
          $business_reviews->save();
      }

      public function hasWrittenReview ($email, $business_id) {
        if(Auth::check()){
            $exists = BusinessReviews::where('email','=' , Auth::user()->email )
                                    ->where('business_id' ,'=', $business_id)
                                    ->first();
        }else{
            $exists = BusinessReviews::where('email','=',$email )
                                    ->where('business_id' ,'=', $business_id)
                                    ->first(); 
        }
        if( $exists != null ) { return true; } else { return false;}
      }

      public static function getBusinessReviewsCount($business_id){
        return BusinessReviews::where('business_id', $business_id)->count();
      }

      public static function getBusinessReviewsAvg($business_id){
        return round(BusinessReviews::where('business_id', $business_id)->avg('rating'), 0);
    }

    public static function GetBusinessReviews($business_id){
        return BusinessReviews::where('business_id', $business_id)
                              ->orderBy('created_at', 'desc')
                              ->simplePaginate(4);
    }

    public static function CountReviews($business_id) { 
        $count = BusinessReviews::where('business_id', '=',$business_id)->count();
        return $count;
    }

    public static function UserImage ($email){
        $user_exists = User::where('email','=',$email)->first();
        if($user_exists != null){
            return $user_exists->image;
        } else { return 'no-image.png'; }
    }
    
    public static function UserLocation ($email){
        $user_exists = User::where('email','=',$email)->first();
        if($user_exists != null){
            return $user_exists->street;
        } else { return ''; }
    }

    public static function getTopBusinesses()
    {
        return $top_reviews = DB::select('SELECT business_id, avg(rating) as average FROM `business_reviews` group by business_id limit 2');
    }
    public static function getTopPremium()
    {
        return $top_reviews = DB::select('SELECT business_id, avg(rating) as average FROM `business_reviews` group by business_id limit 10');
    }
}
