<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class BusinessFeature extends Model
{
    protected $table = 'business_features';

    public static function add_feature_images($request, $business_id, $id){

        if( $request->hasFile('input-feature_images') ) {
            // Are We Editing? If yes than remove previous ones. 
            if( (int)$id != 0 ) {
                $deletedRows = BusinessFeature::where('business_id', (int)$id != 0 ? $id : $business_id )->delete();
                // we also need to delete from the storage.. 
            } 

            foreach($request->file('input-feature_images') as $file)
            {
                $fileNameToStore = $file->hashName();
                $path = $file->storeAs('/public/business/features/', $fileNameToStore);   
                 
                $business_feature = new BusinessFeature();
                $business_feature->business_id = (int)$id != 0 ? $id : $business_id;
                $business_feature->path_feature_image = $fileNameToStore;
                $business_feature->save();
            }

        } else { //IF NO FILE IS IN REQUEST

            // IF WE ARE EDITING and we removed all previous files. 
            if( (int)$id != 0 ) {
               // $deletedRows = BusinessFeature::where('business_id', $business_id )->delete();
            }  // else we insert nothing.

        }
        
        
    }

    public function get_business_features($business_id){
        $business_feature = BusinessFeature::where('business_id', $business_id )
                                            ->take(4)
                                            ->get();
        return $business_feature;
    }
}
