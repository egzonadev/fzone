<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ActivityCategories extends Model
{
    //
    protected $table = "activity_categories";

    public function Add($activity_id, $category_id ){
        
        $new_activity_category = new ActivityCategories();

        $new_activity_category->activity_id = $activity_id;
        $new_activity_category->category_id = $category_id;
        
        $new_activity_category->save();
    }

    public function activity()
    {
        return $this->hasMany('App\Activity','id','activity_id');
    }

    public function category()
    {
        return $this->hasMany('App\Category','id','category_id');
    }

    public static function getCategoryImage( $activity_id ) {
        return DB::table('activity_categories')
                    ->join('categories', 'categories.id', '=', 'activity_categories.category_id')
                    ->select('categories.image')
                    ->where('activity_categories.activity_id', $activity_id)
                    ->first();
    }

}