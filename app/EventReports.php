<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EventReports extends Model
{
    protected $table = "event_reports";

    public function userReporting(){

        return $this->hasOne('App\User','id','user_id' );
    }
    public function event(){

        return $this->hasOne('App\Activity','id','event_id' );
    }

    public static function reported($id){
        $reported = EventReports::where('user_id', Auth::user()->id)
            ->where('event_id', $id)
            ->first();
        if( $reported != null ){
            return true;
        } else {
            return false;
        }
    }
}
