<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Friend extends Model
{
    public function user(){

        return $this->hasOne('App\User','id','follower_id' );
    }
    public function friend_req(){

        return $this->hasOne('App\User','id','user_id' );
    }

    public static function friends($id){
        $friends = Friend::where('user_id', Auth::user()->id)
            ->where('follower_id', $id)
            ->where('status','Accepted')
            ->first();
        if( $friends != null ){
            return true;
        } else {
            return false;
        }
    }
    public static function notfriends($id){
        $friends = Friend::where('user_id', Auth::user()->id)
            ->where('follower_id', $id)
            ->where('status','Pending')
            ->first();
        if( $friends != null ){
            return true;
        } else {
            return false;
        }
    }
}
