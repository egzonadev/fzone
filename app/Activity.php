<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use App\ActivityCategories;
use App\Category;
use App\ActivityInvitedUser;
use App\ActivityAttender;
use App\Http\Controllers\UserController;

class Activity extends Model
{
    protected $table = "activities";

    public function Add($request){
      
        $activity = new Activity;
        $activity_category = new ActivityCategories();
        
        $logged_user_id = Auth::id();

        $activity->user_id              = $logged_user_id; // we need the logged in user that is creating the activity here
        
        $activity->business_id          = $request->input('business') != null ? $request->input('business') : 0;
        
        $activity->title                = $request->input('title');
        $activity->description          = $request->input('description');
        $activity->start_date_time      = $request->input('start_date') . ' ' . $request->input('start_time');
        $activity->end_date_time        = $request->input('end_date')  . ' ' . $request->input('end_time'); 
        $activity->location             = $request->input('location');
        $activity->city                 = $request->input('city');
        // we want to have at least one email for the activity if the user did not enter any extra and also did not select
        // default email. 
        if ( $request->input('extra_email') != "" ){
            $activity->use_account_email    = true;
            $activity->extra_email          = $request->input('extra_email');
        } else {
            $activity->use_account_email    = $request->has('use_account_email') ? true : false;
            $activity->extra_email          = $request->input('extra_email');
        } 

        if ( $request->input('extra_phone') != "" ){
            $activity->use_account_phone    = true;
            $activity->extra_phone          = $request->input('extra_phone');
        } else {
            $activity->use_account_phone    = $request->has('use_account_phone') ? true : false;
            $activity->extra_phone          = $request->input('extra_phone');
        }  

        $activity->save(); 
        
        // SAVING THE CATEGORIES OF THE ACTIVITY
        // Getting the id of the just inserted activity to match with selected categories. 
        $last_activity_id = $activity->id;
        
        // now we need to enter every category of the saved activity
        $db_categories =  Category::getAllCategories(); 
        // iterating through all db categories to check which category has selected the user
        foreach($db_categories as $category){ 
            // get the input checkbox based on category name, if it exists save the relation activity _ category
            if ( $request->has('category_' . $category->id) ) { 
                $activity_category->Add( (int)$last_activity_id, (int)$category->id );
            }
        } 

        // SENDING INVITATION TO FRIENDS
        // get friends to invite preg_replace remove any whitespace in between... 
        $specific_friends = preg_replace('/\s+/', '', $request->input('friends'));

        if( $specific_friends != "" ) {
            $coma_separated_friends = explode( ',', $specific_friends);
            // we need to check if the user said to invite friends by email.. or by name 
             
            foreach($coma_separated_friends as $friend){
                if( strpos($friend, '@') ) { // this means if one name contains an @ user wants to invite by email
                    $user = DB::table('users')->where('email', $friend)->first();
                } else {
                    $user = DB::table('users')->where('name', $friend)->first();
                }

                // NOW here we save activity with invited friends
                if( $user != null ){
                    ActivityInvitedUser::Add( (int)$last_activity_id, (int)$user->id );
                }
                
            }
        }
        
        // INVITING ALL FRIENDS
        if( $request->has('invite_all') ){
            // ALGORITHM
            //1. get all friends of the logged in user join with users and friends on user_id
            //2. get the friends id and save it in the table 
            // the activity id and all the user_ids invited 

            $following_friend = DB::table('friends')->where('user_id',  $logged_user_id )->get();

            foreach($following_friend as $friend){
                ActivityInvitedUser::Add( (int)$last_activity_id, (int)$friend->follower_id );
            }
        }


        // INVITING ALL GIRLS but don't do anything if the option invite all is already selected
        if( $request->has('invite_all_women') && ! $request->has('invite_all')){
            // ALGORITHM
            $following_girl = DB::table('users')
                                    ->join('friends', 'users.id', '=', 'friends.follower_id')
                                    ->select('friends.*')
                                    ->where([
                                        ['friends.user_id','=', $logged_user_id],
                                        ['users.gender', '=', 'female'],
                                    ])->get();
            foreach($following_girl as $friend){
                ActivityInvitedUser::Add( (int)$last_activity_id, (int)$friend->follower_id );
            }
        }

        // INVITING ALL boys but don't do anything if the option invite all is already selected
        if( $request->has('invite_all_men') && ! $request->has('invite_all')){
            // ALGORITHM
            $following_girl = DB::table('users')
                                    ->join('friends', 'users.id', '=', 'friends.follower_id')
                                    ->select('friends.*')
                                    ->where([
                                        ['friends.user_id','=', $logged_user_id],
                                        ['users.gender', '=', 'male'],
                                    ]) ->get();

            foreach($following_girl as $friend){
                ActivityInvitedUser::Add( (int)$last_activity_id, (int)$friend->follower_id );
            }
        }

        return $activity->business_id;

    }

    public function GetUser(){

        return $this->hasOne('App\User','id','user_id' );
    }

    public function getBusiness(){

        return $this->hasOne('App\Business','id','business_id' );
    }

    public static function whoCreatedEvent($b_id){
        $user_event = Activity::where('business_id','=', $b_id)
            ->take(10)
            ->get();
        return $user_event;
    }

    public function category()
    {
        return $this->belongsToMany('App\Category','activity_categories');
    }

    public function categoryColor()
    {
        return $this->hasMany('App\ActivityCategories','activity_id');
    }

    public static function GetAllActivityAttenders($activity_id){

        $all_attenders = DB::table('activity_attenders')
                        ->join('activities', 'activities.id', '=', 'activity_attenders.activity_id')
                        ->join('users', 'users.id', '=', 'activity_attenders.user_id')
                        ->select('users.*')
                        ->where('activities.id','=', $activity_id) 
                        ->take(3)
                        ->get(); 

        return $all_attenders;
    }

    public static function GetAttendersCount($activity_id){
        $all_attenders = ActivityAttender::where('activity_id','=', $activity_id)->count();
        return $all_attenders;
    }

    //Display on event
    public static function getAttenders($activity_id){

        $all_attenders = DB::table('activity_attenders')
            ->join('activities', 'activities.id', '=', 'activity_attenders.activity_id')
            ->join('users', 'users.id', '=', 'activity_attenders.user_id')
            ->select('users.*')
            ->where('activities.id','=', $activity_id)
            ->take(10)
            ->get();

        return $all_attenders;
    }

    public static function getFollowers($activity_id){

        $all_attenders = DB::table('activity_interesteds')
            ->join('activities', 'activities.id', '=', 'activity_interesteds.activity_id')
            ->join('users', 'users.id', '=', 'activity_interesteds.user_id')
            ->select('users.*')
            ->where('activities.id','=', $activity_id)
            ->take(10)
            ->get();

        return $all_attenders;
    }

    public static function countFollowers($activity_id){
        $all_attenders = ActivityInterested::where('activity_id','=', $activity_id)->count();
        return $all_attenders;
    }
}
