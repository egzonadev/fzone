<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function isBusinessUser() {
        if( Auth::user()->usertype == 'Business' || Auth::user()->usertype == 'Premium'  ) {
            return true;
        }
    }

    public static function isBusiness() {
        if( Auth::user()->usertype == 'Business' || Auth::user()->usertype == 'Business' ) {
            return true;
        }
    }
}