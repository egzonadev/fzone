<?php

namespace App\Http\Controllers;

use App\ActivityCategories;
use Request;
// use Illuminate\Http\Request;
use Auth;
use App\Activity;
use Illuminate\Support\Facades\DB;
use App\Category;

class AdvancedSearch extends Controller
{
    //
    public function index(){
        return $this->search();
    }
    public function search(){
        $activities = DB::table('activities')
            ->join('users', 'users.id', '=', 'activities.user_id')
            ->select('activities.*', 'users.image')
            ->whereDate('start_date_time','>=', date('Y-m-d'))
            ->orderBy('activities.title', 'asc')
            ->paginate(6);
        return view('advance_search', compact('activities'));
    }
    public function searchAll (){

        $title = Request::input('title');
        $date = Request::input('date');
        $address = Request::input('address');
        $radious = Request::input('radious');
        $category = Request::input('category');
        $event_private = Request::input('EventIndividual');
        $event_business = Request::input('EventBusiness');
        $e_business = Request::input('E_Business');

        $query = DB::table('activities')
                        ->join('users', 'users.id', '=', 'activities.user_id')
                        ->join('businesses', 'businesses.id', '=', 'activities.business_id')
                        ->join('activity_categories', 'activity_categories.activity_id', '=', 'activities.id')
                        ->select('activities.*', 'users.image', 'users.usertype','businesses.path_logo_image');

        if($category != null){
            if ($category == 'all'){
                $activities = Activity::all();
            }else{
                $activities = $query->orWhere('activity_categories.category_id', $category)->get();
            }
        }
        if($title != null){
            $activities = $query->orWhere('activities.title', 'LIKE', '%' . $title . '%');
        }
        if($date != null) {
            $activities = $query->orWhereDate('activities.start_date_time','LIKE', '%' . $date . '%');
        }

        if($address != null) {
            $activities = $query->orWhere('activities.city', 'LIKE',  '%' . $address . '%');
        }

        if($radious != null) {
            $activities = $query->orWhere('activities.city', 'LIKE',  '%' . $address . '%');
        }

        if($event_private != null) {
            $activities = $query->orWhere('users.usertype', '=',  'Individual');
        }

        if($event_business != null) {
            $activities = $query->orWhere('users.usertype', '=',  'Business');
        }

        if($e_business != null) {
            $activities = $query->orWhere('users.usertype', '=',  'Premium');
        }

    /*    $categories = Category::all();
        $selected_categories = array();
        $count = 0;

        foreach($categories as $category){
            if(Request::has('category_' . $category->id)) {
                $selected_categories[ $count ] = $category->id;
                $count++;
            }
        }

        if( count($selected_categories) > 0 ) {
            $activities = $query->join('activity_categories', 'activity_categories.activity_id', '=', 'activities.id')
                ->join('categories', 'categories.id', '=', 'activity_categories.category_id');
            $activities = $query->orWhereIn('categories.id', $selected_categories);
        }*/

        $activities = $query->orderBy('activities.title', 'asc')
            ->distinct()
            ->paginate(8);


        return view('layout.search')->with('activities', $activities);
    }

    public function searchTitle (){

        $title = Request::input('title');

        if($title != null){
            $activities = Activity::where('title', 'LIKE', '%' . $title . '%')->take(12)
                ->get();
        }
        return view('layout.search')->with('activities', $activities);
    }

    public function advanceSearch (){

        $title = Request::input('title');
        $date = Request::input('date');
        $address = Request::input('address');
        $radious = Request::input('radious');
        $category = Request::input('category');
        $event_private = Request::input('EventIndividual');
        $event_business = Request::input('EventBusiness');
        $e_business = Request::input('E_Business');

        $query = DB::table('activities')
            ->join('users', 'users.id', '=', 'activities.user_id')
            ->join('businesses', 'businesses.id', '=', 'activities.business_id')
            ->join('activity_categories', 'activity_categories.activity_id', '=', 'activities.id')
            ->select('activities.*', 'users.image', 'users.usertype','businesses.path_logo_image');

        if($category != null){
            if ($category == 'all'){
                $activities = Activity::all();
            }else{
                $activities = $query->orWhere('activity_categories.category_id', $category)->get();
            }
        }
        if($title != null){
            $activities = $query->orWhere('activities.title', 'LIKE', '%' . $title . '%');
        }
        if($date != null) {
            $activities = $query->orWhereDate('activities.start_date_time','LIKE', '%' . $date . '%');
        }

        if($address != null) {
            $activities = $query->orWhere('activities.city', 'LIKE',  '%' . $address . '%');
        }

        if($radious != null) {
            $activities = $query->orWhere('activities.city', 'LIKE',  '%' . $address . '%');
        }

        if($event_private != null) {
            $activities = $query->orWhere('users.usertype', '=',  'Individual');
        }

        if($event_business != null) {
            $activities = $query->orWhere('users.usertype', '=',  'Business');
        }

        if($e_business != null) {
            $activities = $query->orWhere('users.usertype', '=',  'Premium');
        }

        $categories = Category::all();
            $selected_categories = array();
            $count = 0;

            foreach($categories as $category){
                if(Request::has('category_' . $category->id)) {
                    $selected_categories[ $count ] = $category->id;
                    $count++;
                }
            }

            if( count($selected_categories) > 0 ) {
                $activities = $query->join('activity_categories', 'activity_categories.activity_id', '=', 'activities.id')
                    ->join('categories', 'categories.id', '=', 'activity_categories.category_id');
                $activities = $query->orWhereIn('categories.id', $selected_categories);
            }

        $activities = $query->orderBy('activities.title', 'asc')
            ->distinct()
            ->paginate(8);


        return view('advance_search')->with('activities', $activities);
    }

}

//https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#findnearsql
// SELECT id, 
// ( 3959 * acos( cos( radians(37) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-122) ) + sin( radians(37) ) 
// * sin( radians( lat ) ) ) ) AS distance 
// FROM markers 
// HAVING distance < 25 
// ORDER BY distance LIMIT 0 , 20;

//cr.jpg
//fd.jpg
//cc.jpg
//ff.jpg