<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Business;
use App\Activity;
use App\BusinessFeature;
use App\BusinessContact;
use App\BusinessReviews;
use App\ActivityInterested;
use App\ActivityAttender;
use App\BusinessVisits;
use App\BusinessLike;

use Auth;
use Illuminate\Support\Facades\Route;

// use Request;

class BusinessesController extends Controller
{
    protected $id;

    public function __construct(Request $request)
    {
        $this->id = Route::current()->parameter('id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }
        $business = Business::where('user_id', Auth::id())->count();
        if ($business <= 0) {
            return redirect('/business/create');
        }

        // a duhet me kan me user id te userit qe i ka kriju businessin ?
        $business = Business::where('user_id', Auth::id())->take(1)->get();

        return $this->show($business[0]->id);

        $business = Business::where('user_id', Auth::id())->count();
        if ($business <= 0) {
            return redirect('/business/create');
        }
        $business = Business::where('id', $id)->take(1)->get();
        return $this->show($business[0]->id);

    }

    public function dashboard()
    {
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }

        return $this->index();
    }

    /**ACTIVITIES / EVENTS OF A BUSINESS */
    public function all_events($business_id)
    {
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }
        if (!\App\Http\Controllers\UsersController::isUpgradedUser()) {
            return view('/error');
        }
        $business = Business::find($business_id);
        $activities = Activity::where('business_id', $business_id)
            ->whereDate('start_date_time', '>=', date('Y-m-d'))
            ->simplePaginate(3);
        $notification = null;
        $messages = null;
        $avg_reviews = BusinessReviews::getBusinessReviewsAvg($business_id); //3.0 3 +0.5=3.05
        return view('business.business_dashboard.events', compact('business', 'activities', 'notification', 'messages', 'avg_reviews'));
    }

    /**ACTIVITIES / EVENTS OF A BUSINESS */
    public function notification($business_id)
    {
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }
        if (!\App\Http\Controllers\UsersController::isUpgradedUser()) {
            return view('/error');
        }
        $business = Business::find($business_id);
        $activities = null;
        $notification = null;
        $messages = null;
        $avg_reviews = BusinessReviews::getBusinessReviewsAvg($business_id); //3.0 3 +0.5=3.05
        $interested = ActivityInterested::GetIntersted($business_id);
        $reviews = BusinessReviews::GetBusinessReviews($business_id);

        return view('business.business_dashboard.notification',
            compact('business', 'activities', 'notification',
                'messages', 'avg_reviews', 'interested', 'reviews'));
    }

    public function messages($business_id)
    {
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }
        if (!\App\Http\Controllers\UsersController::isUpgradedUser()) {
            return view('/error');
        }
        $business = Business::find($business_id);
        $activities = null;
        $notification = null;
        $messages = null;
        $avg_reviews = BusinessReviews::getBusinessReviewsAvg($business_id);
        $friends = DB::table('users')
            ->join('friends', 'users.id', '=', 'friends.follower_id')
            ->select('*')
            ->where('friends.user_id', '=', Auth::user()->id)
            ->where('friends.status', '=', 'Accepted')
            ->paginate(10);

        return view('business.business_dashboard.messages',
            compact('business', 'activities',
                'notification', 'messages', 'avg_reviews', 'friends'));
    }

    public function upgrade()
    {
        $user = Auth::user()->find(Auth::user()->id);
        $user->has_requested_upgrade = 'true';
        $user->save();
        return back()->with('success', 'Request sent.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // validate business user 
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }

        return view('/business.create');
    }

    public function createBusiness($btype)
    {
        // validate business user
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }

        return view('/business.add', compact('btype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   {
       // Handling Form Validations
       $this->validate($request, [
           'name' => 'required | max:250',
           'phone' => 'required | max:100',
           'email' => 'required | max:100',
           'street' => 'max:250',
           'house_no' => ' max:250',
           'post_code' => ' max:20',
           'city' => ' max:250',
           'country' => ' max:250',
           'input-cover_image' => ' mimes:jpeg,jpg,png,gif | max:1999',
           'input-logo_image' => ' mimes:jpeg,jpg,png,gif | max:1999'
       ]); // hiqjau required

       $business = new Business();
       $business->add_edit_business($request, 0,'Simple');
       $last_business_id = \App\Business::where( 'user_id' , Auth::id() )->take(1)->get();
       return redirect('/business/'.$last_business_id[0]->id)->with('success', 'Business Added');
   }
    public function storePremium(Request $request, $btype)
    {
        // Handling Form Validations
        $this->validate($request, [
            'name' => 'required | max:250',
            'phone' => 'required | max:100',
            'email' => 'required | max:100',
            'street' => 'max:250',
            'house_no' => ' max:250',
            'post_code' => ' max:20',
            'city' => ' max:250',
            'country' => ' max:250',
            'input-cover_image' => ' mimes:jpeg,jpg,png,gif | max:1999',
            'input-logo_image' => ' mimes:jpeg,jpg,png,gif | max:1999'
        ]); // hiqjau required

        $business = new Business();
        $business->add_edit_business($request, 0, $btype);
        $last_business_id = \App\Business::where('user_id', Auth::id())->take(1)->get();
        return redirect('/business/' . $last_business_id[0]->id)->with('success', 'Business Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }

        $business = Business::find($id);
        $activities = null;
        $notification = null;
        $messages = null;
        $count_reviews = BusinessReviews::getBusinessReviewsCount($id);
        $avg_reviews = BusinessReviews::getBusinessReviewsAvg($id);
        return view('business.business_dashboard.dashboard', compact('business', 'activities', 'notification', 'messages', 'count_reviews', 'avg_reviews'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // validate business user
        if (!UserController::isBusinessUser()) {
            return view('/error');
        }

        $business = Business::find($id);
        return view('/business.edit')->with('business', $business);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Handling Form Validations
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'opening_hours' => 'required',
            'street' => 'required',
            'house_no' => 'required',
            'post_code' => 'required',
            'city' => 'required',
            'country' => 'required'
        ]);

        $business = new Business();
        $business->edit_business($request, $id);

        return redirect('/business/' . $id)->with('success', 'Business updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Business Details for non business user
     */
    public function details($id)
    {
        if ($id == null) {
            return view('/error');
        }

        $business = Business::find($id);
        if ($business != null) {

            //THIS ADDS A VISIT DAILY FOR A UNIQUE IP ADDRESS...
            $business_visit = new BusinessVisits();
            $business_visit->add_visit($id);

            $business_feature = new BusinessFeature();
            $business_features = $business_feature->get_business_features($business->id);
            $avg_reviews = BusinessReviews::getBusinessReviewsAvg($id); //3.0 3 +0.5=3.05
        } else {
            return view('/error');
        }

        return view('business.business_view.details', compact('business', 'business_features', 'avg_reviews'));
    }

    /**ACTIVITIES / EVENTS OF A BUSINESS */
    public function events($id)
    {
        if ($id == null) {
            return view('/error');
        }

        $business = Business::find($id);
        if ($business != null) {
            $upcoming_activities = Activity::where([
                ['business_id', '=', $business->id],
                ['start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour'))],
            ])->take(3)->get();

            $previous_activities = Activity::where([
                ['business_id', '=', $business->id],
                ['start_date_time', '<', date('Y-m-d H:i', strtotime('+2 hour'))],
            ])->take(3)->get();
            $avg_reviews = BusinessReviews::getBusinessReviewsAvg($id); //3.0 3 +0.5=3.05
        } else {
            return view('/error');
        }

        return view('business.business_view.events', compact('business', 'upcoming_activities', 'previous_activities', 'avg_reviews'));
    }

    /**ACTIVITIES / EVENTS OF A BUSINESS */
    public function reviews($id)
    {
        if ($id == null) {
            return view('/error');
        }

        $business = Business::find($id);
        if ($business != null) {
            $reviews = BusinessReviews::GetBusinessReviews($id);
            $count_reviews = BusinessReviews::getBusinessReviewsCount($id);
            $avg_reviews = BusinessReviews::getBusinessReviewsAvg($id); //3.0 3 +0.5=3.05

        } else {
            return view('/error');
        }
        return view('business.business_view.reviews', compact('business', 'reviews', 'count_reviews', 'avg_reviews'));
    }

    public function contact_company(Request $request, $business_id)
    {

        $this->validate($request, [
            'name' => Auth::check() ? '' : 'required | string | max:250',
            'email' => Auth::check() ? '' : 'required | string | email | max:255',
            'message' => 'required | string | max:500'
        ]);

        $business_contact = new BusinessContact();
        $business_contact->ContactBusiness($request, $business_id);

        return redirect('business/' . $business_id . '/details')->with('success', 'Your message was sent');
    }

    /**Adding Business Review */
    public function add_review(Request $request, $business_id)
    {

        $this->validate($request, [
            'name' => Auth::check() ? '' : 'required | string | max:250',
            'email' => Auth::check() ? '' : 'required | string | email | max:255',
            'message' => 'required | string | max:450',
            'rating' => 'required | string | max:1'
        ]);

        $business_review = new BusinessReviews();

        if ($business_review->hasWrittenReview($request->input('email'), $business_id)) {
            return redirect('business/' . $business_id . '/reviews')->with('error', 'A review has been sent with this email');
        }

        $business_review->ReviewsBusiness($request, $business_id);

        return redirect('business/' . $business_id . '/reviews')->with('success', 'Your review was sent');
    }

    /**
     * Own Defined Functions
     */
    public static function isMyBusiness($business_id)
    {
        $result = Business::where([
            ['id', '=', $business_id],
            ['user_id', '=', Auth::id()],
        ])->get();
        if (count($result) == 1) {
            return true;
        }
    }

    public function Like($business_id)
    {

        if (!Auth::check()) {
            return view('/login');
        }

        $business = new BusinessLike();
        $business->like($business_id);

        return redirect('business/' . $business_id . '/details')->with('success', 'Your like is saved');
    }

    public function Dislike($business_id)
    {

        if (!Auth::check()) {
            return view('/login');
        }

        $business = new BusinessLike();
        $business->dislike($business_id);

        return redirect('business/' . $business_id . '/details')->with('success', 'Your like has been removed');
    }
}
