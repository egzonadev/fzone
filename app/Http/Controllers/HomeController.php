<?php

namespace App\Http\Controllers;

use App\Activity;
use App\ActivityCategories;
use App\Category;
use App\Friend;
use Illuminate\Http\Request;
use App\BusinessReviews;
use App\Business;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    { 
        //Fun and adventure
        $fun_category = Category::find(4);
        $fun_activities = $fun_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //essen
        $essen_category = Category::find(1);
        $essen_activities = $essen_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //family
        $family_category = Category::find(2);
        $family_activities = $family_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //wellness
        $wellness_category = Category::find(8);
        $wellness_activities = $wellness_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //ausfluge
        $ausfluge_category = Category::find(3);
        $ausfluge_activities = $ausfluge_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //romantik
        $romantik_category = Category::find(6);
        $romantik_activities = $romantik_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //kultur
        $kultur_category = Category::find(5);
        $kultur_activities = $kultur_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        //sport
        $sport_category = Category::find(7);
        $sport_activities = $sport_category->activity()
            ->where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->latest()->get();

        $avg_top_businesses = BusinessReviews::getTopBusinesses();
        $selected_businesses = array();
        $count = 0; 
        foreach($avg_top_businesses as $business){ 
            $selected_businesses[ $count ] = $business->business_id; 
            $count++; 
        } 

        $top_businesses = Business::whereIn('id', $selected_businesses)->get();



        return view('layout.home',
            compact('essen_activities', 'fun_activities',
                    'family_activities','wellness_activities',
                    'ausfluge_activities','romantik_activities',
                    'kultur_activities','sport_activities', 'top_businesses', 'avg_top_businesses'));
    }
}
