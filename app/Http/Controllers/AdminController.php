<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Business;
use App\BusinessReports;
use App\EventReports;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function index()
    {
        return view('super_admin.dashboard');
    }

    public function statistics(){
        return view('super_admin.statistics');
    }

    public function reports(){
        $reports = BusinessReports::all();
        $eventReports  = EventReports::all();
        return view('super_admin.reports',compact('reports','eventReports'));
    }

    public function dashboard_g1(){
        $users = User::select('usertype', DB::raw("COUNT(id) as count"))
            ->groupBy('usertype')
            ->get();
        return response()->json($users);
    }

    public function dashboard_g2(){
        $business = User::orWhere('usertype','Premium')->orWhere('usertype','Business')
            ->select( DB::raw("COUNT(id) as count"), DB::raw("MONTHNAME(created_at) as month"))
            ->groupBy('month')
            ->orderBy('created_at', 'asc')
            ->get();
        return response()->json($business);
    }

    public function multiStat(){
        $activity = Activity::where('start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour')))
            ->select( DB::raw("COUNT(id) as count"), DB::raw("MONTHNAME(start_date_time) as month"))
            ->groupBy('month')
            ->orderBy('start_date_time', 'asc')
            ->get();
        return response()->json($activity);
    }

    public function previous(){
        $activity = Activity::where('start_date_time', '<', date('Y-m-d H:i', strtotime('+2 hour')))
            ->select( DB::raw("COUNT(id) as count"), DB::raw("MONTHNAME(start_date_time) as month"))
            ->groupBy('month')
            ->orderBy('start_date_time', 'asc')
            ->get();
        return response()->json($activity);
    }

    public function users(){
        $private_users = \App\User::where('usertype', '=', 'Individual')->get();
        return view('super_admin.users', compact('private_users'));
    }

    public function businesses(){
        $premium_users = \App\User::where('has_requested_upgrade', '=', 'true')->get();
        return view('super_admin.business',compact('premium_users'));
    }

    public function upgrade($id){
        $user = User::find($id);
        $user->has_requested_upgrade = 'false';
        $user->is_upgraded = 'true';
        $user->usertype = 'Premium';
        $user->save();
        return back()->with('success', 'You have successfully upgraded business to premium.');
    }

    public function events(){
        return view('super_admin.events');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('super_admin.profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        if ( Input::hasFile('image')) {
            $request->file('image')->store('/public/profile/');
            $filename = $request->file('image')->hashName();
            $user->image = $filename;
        }

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->password = bcrypt($request['pass']);
        $user->save();
        return back()->with('success', 'Your profile has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = EventReports::findOrFail($id);
            $event->delete();
        return back()->with('success', 'This report has been deleted.');
    }

    public function destroyBusiness($id){
        $business = BusinessReports::findOrFail($id);
        $business->delete();
        return back()->with('success', 'This report has been deleted.');
    }
}
