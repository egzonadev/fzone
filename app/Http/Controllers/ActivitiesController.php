<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Activity;
use App\ActivityInterested;
use App\ActivityAttender;

class ActivitiesController extends Controller
{
    public function __construct()
    {
        // This will block anyone who is not registered from continuing with this request
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('/activity.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (UserController::isBusinessUser()) {
            return back()->with('success','Business can not create activity');
        }
        return view('/activity.create')->with('business_id', null);
    }

    public function new($business_id)
    {
        if (UserController::isBusinessUser()) {
            return back()->with('success','Business can not create activity');
        }
        return view('activity.create',compact('business_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the fields
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'location' => 'required',
            'city' => 'required',
        ]);

        // CHECKING IF THE EVENT IS BEING SAVED IN THE PAST...
        $start_date = date("d-m-Y", strtotime($request->input('start_date')));
        $end_date = date("d-m-Y", strtotime($request->input('end_date')));
        $start_time = date("H:i", strtotime($request->input('start_time')));
        $end_time = date("H:i", strtotime($request->input('end_time')));

        $start_date_time = date("d-m-Y H:i", strtotime( $request->input('start_date') . ' ' . $request->input('start_time') ) );
        $end_date_time = date("d-m-Y H:i", strtotime( $request->input('end_date') . ' ' . $request->input('end_time') ) );

        $current_date = date('Y-m-d H:i', strtotime('+2 hour')); 
        
        // if user selects a date in the past than't we throw in an error
        if ( date('"d-m-Y', strtotime($start_date) ) < date('"d-m-Y') ) {
            return redirect('/activity/create')->with('error', 'Starting date of the event cannot be in the past');
        }

        // ACTIVITY CANNOT END LESS THAN START DATE
        if( $end_date_time < $start_date_time ) {
            return redirect('/activity/create')->with('error', 'Ending Date and time of the event cannot be less than starting date');
        }

        // if( $end_time < $start_time ) {
        //     return redirect('/activity/create')->with('error', 'Ending Time of the event cannot be less than starting time');
        // }

        $activity = new Activity();
        $business_id = $activity->Add($request);

        if (Auth::user()->usertype == 'Individual') {
            return redirect('/user/activities')->with('success', 'New activity has been added successfully.');
        } else {
            if($business_id > 0){
                return redirect('/business/events/' . $business_id)->with('success', 'New activity has been added successfully.');
            } else {
                return redirect('/business')->with('success', 'New activity has been added successfully.'); 
            }
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addInterested($activity_id) {

        if (UserController::isBusinessUser()) {
            return back()->with('success','Business can not add interests on activity.');
        }

        if( ! Auth::check() ) {
            return back()->with('error' , 'Please log in to proceed');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $interested = new ActivityInterested();
        $interested->is_interested($activity_id);
        return back()->with('success', 'Your interest has been saved');
        // return redirect('business/' .$activity->business_id . '/events')->with('success', 'Your interest has been saved');
    }

    public function addNotInterested($activity_id) {

        if( ! Auth::check() ) {
            return redirect('/login');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $interested = new ActivityInterested();
        $interested->is_not_interested($activity_id);
        return back()->with('success', 'Your interest has been removed');
        // return redirect('business/' .$activity->business_id . '/events')->with('success', 'Your interest has been removed');
    }

    public function Attend($activity_id) {

        if (UserController::isBusinessUser()) {
            return back()->with('success','Business can not attend activity.');
        }
        if( ! Auth::check() ) {
            return redirect('/login');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $attend = new ActivityAttender();
        $attend->attend($activity_id);
        return back()->with('success', 'Your attendance is saved');
        // return redirect('business/' .$activity->business_id . '/events')->with('success', 'Your attendance is saved');
    }

    public function RemoveAttend($activity_id) {

        if( ! Auth::check() ) {
            return redirect('/login');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $attend = new ActivityAttender();
        $attend->do_not_attend($activity_id);
        return back()->with('success', 'Your attendance has been removed');
        // return redirect('business/' .$activity->business_id . '/events')->with('success', 'Your attendance has been removed');
    }

    public function OwnerInterested($activity_id) {

        if( ! Auth::check() ) {
            return redirect('/login');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $interested = new ActivityInterested();
        $interested->is_interested($activity_id);
        return back()->with('success', 'Your interest has been saved');
        // return redirect('business/events/' .$activity->business_id)->with('success', 'Your interest has been saved');
    }

    public function OwnerNotInterested($activity_id) {

        if( ! Auth::check() ) {
            return redirect('/login');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $interested = new ActivityInterested();
        $interested->is_not_interested($activity_id);
        return back()->with('success', 'Your interest has been removed');
        // return redirect('business/events/' .$activity->business_id )->with('success', 'Your interest has been removed');
    }

    public function OwnerAttend($activity_id) {

        if( ! Auth::check() ) {
            return redirect('/login');
        }
  
        $activity = Activity::where('id', $activity_id)->first();
        
        $attend = new ActivityAttender();
        $attend->attend($activity_id);
        return back()->with('success', 'Your attendance is saved'); 
        // return redirect('business/events/' .$activity->business_id)->with('success', 'Your attendance is saved'); 
    }

    public function OwnerRemoveAttend($activity_id) {

        if( ! Auth::check() ) {
            return redirect('/login');
        }

        $activity = Activity::where('id', $activity_id)->first();
        
        $attend = new ActivityAttender();
        $attend->do_not_attend($activity_id);
        return back()->with('success', 'Your attendance has been removed');
        // return redirect('business/events/' .$activity->business_id)->with('success', 'Your attendance has been removed');
    }

}
