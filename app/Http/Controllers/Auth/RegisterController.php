<?php

namespace App\Http\Controllers\Auth;

use App\Interests;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $request;


    public function showRegistrationForm($usertype)
    {
        $interests = Interests::all();
        if ($usertype == 'Business'){
            return view('auth.registerbusiness', compact('interests'));
        }elseif ($usertype == "Individual"){
            return view('auth.register', compact('interests'));
        }
    }


    public function __contruct(Request $request)
    {
        $this->request = $request;
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'usertype' => 'required|string',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|string|max:255',
            'street' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $type = $data['usertype'];
        $request = app('request');
        $user = new User();
        if ($type == 'Individual'){
            //saving private user image
            $request->file('image')->store('/public/profile/');
            $filename = $request->file('image')->hashName();

            $user->usertype = $type;
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->image = $filename;
            $user->gender = $data['gender'];
            $user->age = $data['age'];
            $user->phone = $data['phone'];
            $user->street = $data['street'];
            $user->pcode = $data['pcode'];
            $user->save();
            $user_id = $user->id;

            //saving the interests
            $interest = new Interests();
            $interest->addInterest($data, $user_id);
        }elseif ($type == "Business"){
            $user->usertype = $type;
            $user->name = $data['name'];
            $user->street = $data['street'];
            $user->pcode = $data['plz'] .', '. $data['ort'];
            $user->phone = $data['phone'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);

            //contacting person
            $user->jobtitle = $data['jobtitle'];
            $user->contact_name = $data['contact_name'];
            $user->contact_phone = $data['contact_phone'];
            $user->contact_email = $data['contact_email'];
            $user->save();
        }
        return $user;
    }
}
