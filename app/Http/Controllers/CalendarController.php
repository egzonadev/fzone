<?php

namespace App\Http\Controllers;

use App\Activity;
use App\ActivityCategories;
use Illuminate\Http\Request;
use App\Event;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;


class CalendarController extends Controller
{
    public function index()
    {
        $color = '#40b7ff';
        $events = [];
        $data = Activity::all();
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $category_id = $value->categoryColor->first()->category_id;
                if ($category_id == 1) {
                    $color = '#40b7ff';
                }elseif ($category_id == 2) {
                    $color = '#dce254';
                }elseif ($category_id == 3) {
                    $color = '#23c6c8';
                }elseif ($category_id == 4) {
                    $color = '#a62d19';
                }elseif ($category_id == 5) {
                    $color = '#9e3be1';
                }elseif ($category_id == 6) {
                    $color = '#ff0000';
                }elseif ($category_id == 7) {
                    $color = '#26e613';
                } else{
                    $color = '#f3a015';
                }
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->start_date_time),
                    new \DateTime($value->end_date_time . ' +1 day'),
                    null,

                    // Add color and link on event

                    [
                        'color' => $color,
                        'url' => '/event/' . $value->id,
                    ]
                );
            }
        }
        $calendar = Calendar::addEvents($events);
        return view('calendar.fullcalendar', compact('calendar'));
    }

}