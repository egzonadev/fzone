<?php

namespace App\Http\Controllers;

use App\Activity;
use App\BusinessReports;
use App\BusinessReviews;
use App\EventReports;
use App\Friend;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\UserInterests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function __construct()
    {
        // This will block anyone who is not registered from continuing with this request
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public static function isUpgradedUser() {
        if( Auth::check() && strcmp(Auth::user()->is_upgraded, 'true') == 0 ) {
            return true;
        }
    }

    public function index()
    {
        $user_id = Auth::user()->id;
        $user_interests = UserInterests::all()->where('user_id', $user_id);
        return view('user_profile.profile_container', compact('user_interests'));
    }

    public function profile($id)
    {
        $friend = User::all()->where('id', $id);
        $friend_interests = UserInterests::all()->where('user_id', $id);
        return view('user_profile.visits.profile', compact('friend', 'friend_interests'));
    }

    public function activities($id)
    {
        $friend = User::all()->where('id', $id);
        $activities = Activity::all()->where('user_id', $id);

        if ($activities != null) {
            $upcoming_activities = Activity::where([
                ['user_id', '=', $id],
                ['start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour'))],
            ])->take(3)->latest()->get();

            $previous_activities = Activity::where([
                ['user_id', '=', $id],
                ['start_date_time', '<', date('Y-m-d H:i', strtotime('+2 hour'))],
            ])->take(3)->latest()->get();
        }
        return view('user_profile.visits.activities', compact('activities', 'upcoming_activities', 'previous_activities', 'friend'));
    }

    public function addFriends($id)
    {
        $friend = new Friend();
        $friend->user_id = Auth::user()->id;
        $friend->follower_id = $id;
        $friend->save();
        return back()->with('success', 'Your friend request has been sent.');
    }
    public function report(Request $request, $id)
    {
        $status = $request->input('report_status');
        $report = new BusinessReports();
        $report->reporter = Auth::user()->id;
        $report->business_id = $id;
        $report->status = $status;
        $report->save();
        return back()->with('success', 'Your report is under review.');
    }

    public function eventreport(Request $request, $id)
    {
        $status = $request->input('report_status');
        $report = new EventReports();
        $report->user_id = Auth::user()->id;
        $report->event_id = $id;
        $report->status = $status;
        $report->save();
        return back()->with('success', 'Your report has been sent.');
    }

    public function acceptFriend($id)
    {
        $friend = Friend::find($id);
        $friend->status = 'Accepted';
        $friend->save();

        $follow = Friend::all()->where('id', $id)->pluck('user_id');
        foreach ($follow as $follower) {
            $newfriend = new Friend();
            $newfriend->user_id = Auth::user()->id;
            $newfriend->follower_id = $follower;
            $newfriend->status = 'Accepted';
            $newfriend->save();
        }

        return back()->with('success', 'Your have added a new friend in your friend list.');
    }

    public function ignoreFriend($id)
    {
        $friend = Friend::find($id);
        $friend->delete();
        return back()->with('success', 'Friend request has been rejected.');
    }

    public function friends($id)
    {
        $friend = User::all()->where('id', $id);
        $friendsList = Friend::where('user_id', $id)
            ->where('status', 'Accepted')
            ->get();
        return view('user_profile.visits.friends', compact('friendsList', 'friend'));
    }

    public function userActivities()
    {
        $user_id = Auth::user()->id;
        $activities = Activity::all()->where('user_id', $user_id);

        if ($activities != null) {
            $upcoming_activities = Activity::where([
                ['user_id', '=', $user_id],
                ['start_date_time', '>', date('Y-m-d H:i', strtotime('+2 hour'))],
            ])->take(3)->latest()->get();

            $previous_activities = Activity::where([
                ['user_id', '=', $user_id],
                ['start_date_time', '<', date('Y-m-d H:i', strtotime('+2 hour'))],
            ])->take(3)->latest()->get();
        }
        return view('user_profile.activities', compact('activities', 'upcoming_activities', 'previous_activities'));
    }

    public function userfriends()
    {
        $friendRequest = Friend::all()->where('follower_id', Auth::user()->id)
            ->where('status', 'Pending');
        $friendsList = Friend::where('user_id', Auth::user()->id)
            ->where('status', 'Accepted')
            ->get();
        return view('user_profile.friends', compact('friendsList', 'friendRequest','user_request'));
    }

    public function messages($id)
    {
        $activities = null;
        $notification = null;
        $messages = null;
        $friends = DB::table('users')
            ->join('friends', 'users.id', '=', 'friends.follower_id')
            ->select('*')
            ->where('friends.user_id', '=', $id)
            ->where('friends.status', '=', 'Accepted')
            ->paginate(10);

        return view('user_profile.messages',
            compact('activities',
                'notification', 'messages', 'friends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user_profile.edit_profile');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::all()->where('id', $id);
        return view('user_profile.edit_profile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $user = User::find(Auth::user()->id);
        $user->description = Input::get('description');
        $user->save();
        return back()->with('success', 'Your biography has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
