<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use App\Message;

class MessagesController extends Controller
{
    //
    public function send(){
        $ajax_message = Input::get('message');
        $receiver = Input::get('sel');
        $sender = Auth::id();

        $message = new Message;
        $message->sender_user_id = $sender;
        $message->receiver_user_id = $receiver;
        $message->message = $ajax_message;
        $message->save();

        return "true";
    }

    public function getMsg() {
        $selected_user = Input::get('sel');
        $limit = Input::get('lim');
        if( (int)$limit > 8 ) {
            $messages = $this->getMessages( ((int)$limit + 8) , $selected_user);
        } else {
            $messages = $this->getMessages( 8 , $selected_user);
        }


        return json_encode( $messages );
    }

    public function getMessages( $limit, $selected_user ) {
        $query = "SELECT * FROM 
                 (  SELECT users.image, users.name, messages.* 
                    FROM messages 
                    INNER JOIN users on users.id = messages.sender_user_id 
                    WHERE (messages.sender_user_id = " . Auth::id() . " or messages.sender_user_id = " . $selected_user . ") 
                    AND (messages.receiver_user_id = " . Auth::id() . " or messages.receiver_user_id = " . $selected_user . ")
                    order by messages.created_at desc
                    limit " . $limit . "
                 ) sub
                    ORDER BY created_at ASC;";

        $messages = DB::select($query);
        return $messages;
    }

    public function check_for_new_messages(){
        $query = "SELECT sender_user_id, count(sender_user_id) as count_new_sms 
                  from messages 
                  where receiver_user_id = " . Auth::id() . " 
                  and ReadType = 'NotRead' 
                  GROUP by sender_user_id";
        $new_messages = DB::select($query);

        return json_encode( $new_messages );
    }

    public function save_msg_read(){
        $selected_user = Input::get('sel');
        $query = "update messages set ReadType = 'Read' where receiver_user_id = " .  Auth::id() . " and sender_user_id = " . $selected_user. " ";
        return DB::select($query);

    }
}
