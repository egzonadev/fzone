<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityInvitedUser extends Model
{
    protected $table = "activity_invited_users";

    public static function Add($activity_id, $user_id ){
        
        $new_activity_invitee = new ActivityInvitedUser();

        $new_activity_invitee->activity_id = $activity_id;
        $new_activity_invitee->invited_user_id = $user_id;
        
        $new_activity_invitee->save();
    }
}
