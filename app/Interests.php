<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interests extends Model
{
    protected $table = 'interest';

    protected $fillable = [
        'interest','image'
    ];

    public function addInterest($request, $user_id){
        $interests =  Interests::all();
        $user_interests = new UserInterests();

        foreach($interests as $interest){
            if (isset($request['category_' . $interest->id]) ?  $request['category_' . $interest->id] :'0' ) {
                $user_interests->addUserInterest((int)$user_id, (int)$interest->id);
            }
        }
    }

}
