<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use DateTime;

class BusinessVisits extends Model
{
    //
    protected $table = 'business_visits';

    public function add_visit($business_id) {
        
        $ip_address = Request::ip();
        $current_date = date('Y-m-d');
        
        if (! $this->has_visited_today( $business_id, $ip_address, $current_date  ) ) {
            $new_visit = new BusinessVisits();

            $new_visit->business_id = $business_id;
            $new_visit->ip = $ip_address;

            $new_visit->save();
        }
    }

    protected function has_visited_today($business_id, $ip, $current_date){
        $visit = BusinessVisits::where('business_id','=', $business_id)
                                ->where('ip','=', $ip)
                                ->whereDate('created_at', '=', $current_date)
                                ->first();
        if ($visit != null) { return true; } else { return false;}
    }

    public static function VisitsCount($business_id){
        return BusinessVisits::where('business_id','=', $business_id)->count();
    }

    public static function GetWeeklyVisitsCount($business_id) {
        $dateTime = new DateTime();
        $dateTime->setISODate(date("Y"), date("W")); //
        $current_week_start_date = $dateTime->format('Y-m-d');
        $dateTime->modify('+6 days');
        $current_week_end_date = $dateTime->format('Y-m-d');
        $weekly_visits_count = BusinessVisits::where('business_id','=', $business_id)
                                ->whereDate('created_at', '>=', $current_week_start_date)
                                ->whereDate('created_at', '<=', $current_week_end_date)
                                ->count();   
        return $weekly_visits_count;
    }
}
