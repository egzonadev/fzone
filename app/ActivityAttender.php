<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ActivityAttender extends Model
{
    //
    protected $table = "activity_attenders";

    public function attend($activity_id){
        $interested = new ActivityAttender();
        $interested->user_id = Auth::id();
        $interested->activity_id = $activity_id;
        $interested->AttendingType= 'Attending';
        $interested->save();
    }

    public function do_not_attend($activity_id){
        ActivityAttender::where('activity_id', $activity_id)
          ->where('user_id', Auth::id())->delete();
    }
 
    public static function isAttendingActivity($activity_id){ 
        $is_interested = ActivityAttender::where('activity_id', $activity_id)
                                            ->where('user_id', Auth::id())
                                            ->where('AttendingType','Attending')
                                            ->first();
        if( $is_interested != null ){
            return true;
        } else {
            return false;
        } 
    }
 
}
