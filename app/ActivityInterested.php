<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\DB;

class ActivityInterested extends Model
{
    //
    protected $table = "activity_interesteds";

    public function is_interested($activity_id){
        $interested = new ActivityInterested();
        $interested->user_id = Auth::id();
        $interested->activity_id = $activity_id;
        $interested->InterestType= 'Interested';
        $interested->save();
    }

    public function is_not_interested($activity_id){
        ActivityInterested::where('activity_id', $activity_id)
          ->where('user_id', Auth::id())->delete();
    }

    public static function isFollowingActivity($activity_id){ 
        $is_interested = ActivityInterested::where('activity_id', $activity_id)
                                            ->where('user_id', Auth::id())
                                            ->where('InterestType','Interested')
                                            ->first();
        if( $is_interested != null ){
            return true;
        } else {
            return false;
        } 
    }

    public static function GetIntersted($business_id){
        $interested = DB::table('activity_interesteds')
                        ->join('activities', 'activities.id', '=', 'activity_interesteds.activity_id')
                        ->join('users', 'users.id', '=', 'activities.user_id')
                        ->select('*')
                        ->where('activities.business_id','=', $business_id)
                        ->simplePaginate(4);
        return $interested;
    }
    
}
