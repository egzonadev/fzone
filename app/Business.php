<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use App\BusinessFeature; 
use App\BusinessReviews;
use App\BusinessVisits;

class Business extends Model
{
    protected $table = 'businesses';

    public function get_All_User_Business() {
 
        $businesses = DB::table('businesses')->select('id', 'name')->where('user_id',Auth::id())->orderBy('created_at','desc')->get();
     
        return $businesses; 
    }

    public function getSelectedBusiness( $business_id ) { 
            return DB::table('businesses')
                        ->select('id', 'name')
                        ->where('id', $business_id)
                        ->get(); 
    }

    public function getBusinessUserBusinesses(){
        return DB::table('businesses')
                    ->select('id', 'name')
                    ->where('user_id', Auth::id())
                    ->get();
    }

    public function get_First_User_Business(){
        return Business::where( 'user_id', Auth::id()  )->take(1)->get(); 
    }

    public function add_edit_business($request,  $id, $btype){
  
         // Handle Logo and Cover Image Upload and get the names to save in db 
        $business = new Business();

        $cover_fileNameToStore = $this->get_uploaded_file_name($request, 'cover', 'input-cover_image');
        $logo_fileNameToStore =  $this->get_uploaded_file_name($request, 'logo', 'input-logo_image');

        $logged_user_id = Auth::id();

        $business->user_id = $logged_user_id;
        $business->name = $request->input('name');
        $business->business_type = $btype;
        $business->category = $request->input('type');
        $business->phone = $request->input('phone');
        $business->email = $request->input('email');
        $business->opening_hours = $request->input('opening_hours');
        $business->closing_hours = $request->input('closing_hours');
        $business->street = $request->input('street');
        $business->house_no = $request->input('house_no');
        $business->street = $request->input('street');
        $business->post_code = $request->input('post_code');
        $business->city = $request->input('city');
        $business->country = $request->input('country');

        $business->sdate = $request->input('sdate');
        $business->edate = $request->input('edate');
        $business->showdate = $request->input('showdate');
        $business->bio = $request->input('bio');
        $business->web1 = $request->input('web1');
        $business->web2 = $request->input('web2');
        $business->video = $request->input('video');

        if( $request->hasFile('input-cover_image') ){
            $business->path_cover_image = $cover_fileNameToStore;
        }
        if( $request->hasFile('input-logo_image') ){
            $business->path_logo_image = $logo_fileNameToStore;
        }
        $business->save();

        // IF WE ARE EDITING BUSINESS AND WE REMOVED ALL THE FILES ... 
        //BusinessFeature::add_feature_images($request, $last_business_id, $id);
    }

    public function edit_business($request,  $id){

        // Handle Logo and Cover Image Upload and get the names to save in db
        $business = Business::find($id);
        $cover_fileNameToStore = $this->get_uploaded_file_name($request, 'cover', 'input-cover_image');
        $logo_fileNameToStore =  $this->get_uploaded_file_name($request, 'logo', 'input-logo_image');

        $logged_user_id = Auth::user()->id;

        $business->user_id = $logged_user_id;
        $business->name = $request->input('name');
        $business->category = $request->input('type');
        $business->phone = $request->input('phone');
        $business->email = $request->input('email');
        $business->opening_hours = $request->input('opening_hours');
        $business->closing_hours = $request->input('closing_hours');
        $business->street = $request->input('street');
        $business->house_no = $request->input('house_no');
        $business->street = $request->input('street');
        $business->post_code = $request->input('post_code');
        $business->city = $request->input('city');
        $business->country = $request->input('country');

        $business->sdate = $request->input('sdate');
        $business->edate = $request->input('edate');
        $business->showdate = $request->input('showdate');
        $business->bio = $request->input('bio');
        $business->web1 = $request->input('web1');
        $business->web2 = $request->input('web2');
        $business->video = $request->input('video');

        if( $request->hasFile('input-cover_image') ){
            $business->path_cover_image = $cover_fileNameToStore;
        }
        if( $request->hasFile('input-logo_image') ){
            $business->path_logo_image = $logo_fileNameToStore;
        }
        $business->save();

        $last_business_id = $business->id;

        // IF WE ARE EDITING BUSINESS AND WE REMOVED ALL THE FILES ...
        BusinessFeature::add_feature_images($request, $last_business_id, $id);
    }
    /**
     * 21 April 18 
     * this function gets the extention of the file and the file name and separates it
     * it uploads in the db and returns the name uploaded to be saved in the db 
     * parameters
     * $request from the form
     * $folder_to_upload : is the name of the folder where you want to upload
     * $input_name : is the name of the input ..
     */
    public function get_uploaded_file_name($request, $folder_to_upload, $input_name){
        // Handle Cover Image Upload 
        if($request->hasFile($input_name)){ 
            // File name to store
            $fileNameToStore = $request->file($input_name)->hashName();
            // Upload Image 
            $path = $request->file($input_name)->storeAs('/public/business/' . $folder_to_upload, $fileNameToStore);
        } else {
            $fileNameToStore = "noCoverImage.jpg";
        }
        return $fileNameToStore;
    }

    public static function getVisitsGraphData( $business_id , $year) {
 
        return BusinessVisits::where('business_id','=', $business_id)
                                                        ->whereYear('created_at','=', $year)
                                                        ->count(); 
    }

    public static function getReviewsGraphData( $business_id , $year) {
       return BusinessReviews::where('business_id','=', $business_id)
                                                        ->whereYear('created_at','=', $year)
                                                        ->count(); 
    }
    
}
 