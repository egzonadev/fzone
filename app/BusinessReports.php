<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BusinessReports extends Model
{
    protected $table = "business_reports";

    public function userReporting(){

        return $this->hasOne('App\User','id','reporter' );
    }
    public function business(){

        return $this->hasOne('App\Business','id','business_id' );
    }

    public static function reported($id){
        $reported = BusinessReports::where('reporter', Auth::user()->id)
            ->where('business_id', $id)
            ->first();
        if( $reported != null ){
            return true;
        } else {
            return false;
        }
    }
}
