<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BusinessContact extends Model
{
    //
    protected $table = "business_contacts";

    public function ContactBusiness($request, $business_id){
        
        $business_contact = new BusinessContact();
        
        $business_contact->business_id = $business_id;

        if(Auth::check()){
            $business_contact->name = Auth::user()->name;
            $business_contact->email = Auth::user()->email;
        } else {
            $business_contact->name = $request->input('name');
            $business_contact->email = $request->input('email');
        } 
 
        $business_contact->message = $request->input('message');
        $business_contact->save();
    }
}
