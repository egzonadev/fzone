<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'usertype','name','username', 'email', 'password','image','gender','age', 'phone', 'street','pcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user(){

        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function userinterest(){

        return $this->hasMany('App\UserInterests', 'user_id', 'id');
    }

  /*  public function interests($user_id, $interest_id ){

        $interest = new Interests();
        $interest->activity_id = $activity_id;
        $interest->category_id = $category_id;
        $interest->save();
    }*/
}
