<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "categories";

    public static function getAllCategories(){
        $category = Category::all();
        return $category;
    }

    public function activity()
    {
        return $this->belongsToMany('App\Activity','activity_categories');
    }

}
