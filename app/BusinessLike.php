<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BusinessLike extends Model
{
    protected $table = "business_likes";

    public function like($business_id){
        $interested = new BusinessLike();
        $interested->user_id = Auth::id();
        $interested->business_id = $business_id;
        $interested->LikeType= 'Likes';
        $interested->save();
    }

    public function dislike($business_id){
        BusinessLike::where('business_id', $business_id)
          ->where('user_id', Auth::id())->delete();
    }

    public static function likesBusiness($business_id){ 
        $is_interested = BusinessLike::where('business_id', $business_id)
                                        ->where('user_id', Auth::id())
                                        ->where('LikeType','Likes')
                                        ->first();
        if( $is_interested != null ){
            return true;
        } else {
            return false;
        } 
    } 
    
}
